﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Net.Mail;
using System.Web.Mvc;
using System.IO;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using openXmlDoc = DocumentFormat.OpenXml.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using HRMS.DataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Reflection;
using System.Globalization;
using System.Xml.Linq;
using System.Xml;
using System.Linq.Expressions;
using System.Net;
using HRMS.DataAccess.GeneralDB;
using System.Text.RegularExpressions;
using System.ComponentModel;
using DocumentFormat.OpenXml.Office2013.Excel;
using Stimulsoft.Report;
//using NotesFor.HtmlToOpenXml;

namespace HRMS.Web.CommonHelper
{
    public static class MasterHelper
    {
        public static MvcHtmlString TempView(this System.Web.Mvc.HtmlHelper htmlHelper)
        {
            return System.Web.Mvc.Html.PartialExtensions.Partial(htmlHelper, VirtualPathUtility.ToAbsolute("~/Views/DevelopmentPlan/_NonTeaching.cshtml"));
        }
        public static MvcHtmlString PartialTeachingView(this System.Web.Mvc.HtmlHelper htmlHelper, object model)
        {
            return System.Web.Mvc.Html.PartialExtensions.Partial(htmlHelper, VirtualPathUtility.ToAbsolute("~/Areas/PDRP/Views/AppraisalTeaching/_YearlyKPIs.cshtml"), model);
        }
        public static MvcHtmlString PartialNonTeachingView(this System.Web.Mvc.HtmlHelper htmlHelper, object model)
        {
            return System.Web.Mvc.Html.PartialExtensions.Partial(htmlHelper, VirtualPathUtility.ToAbsolute("~/Views/DevelopmentPlan/_PDRPNonTeaching.cshtml"), model);
        }
        public static MvcHtmlString PartialPDRPTeachingView(this System.Web.Mvc.HtmlHelper htmlHelper, object model)
        {
            return System.Web.Mvc.Html.PartialExtensions.Partial(htmlHelper, VirtualPathUtility.ToAbsolute("~/Views/DevelopmentPlan/_PDRPTeaching.cshtml"), model);
        }
        public static MvcHtmlString PartialPDRPNonTeachingView(this System.Web.Mvc.HtmlHelper htmlHelper, object model)
        {
            return System.Web.Mvc.Html.PartialExtensions.Partial(htmlHelper, VirtualPathUtility.ToAbsolute("~/Views/DevelopmentPlan/_PDRPNonTeaching.cshtml"), model);
        }
    }
    public class CommonHelper
    {
        public const string LogoutMsg = "You have successfully log out.";
        public const string InvalidCredentialMsg = "User name and password mismatch!.";
        public const string NoPermissionsMsg = "Sorry! permissions not assign to you, please contact administration.";
        public static int countGr = 0;
        public static int NoOfDecimalPlaces = 0;
        public static string AmountFormat = "0.00";
        public static int PdfHeaderCount = 0;
        public static string tempMessage = "";       
        public static string GetAmountFormat()
        {
            int DigitAfterDecimal = 0;

            try
            {
                NoOfDecimalPlaces = new PayrollDB().GetDigitAfterDecimal();
                DigitAfterDecimal = NoOfDecimalPlaces;
                if (DigitAfterDecimal == 0)
                {
                    AmountFormat = "0";
                }
                else if (DigitAfterDecimal == 1)
                {
                    AmountFormat = "0.0";
                }
                else if (DigitAfterDecimal == 2)
                {
                    AmountFormat = "0.00";
                }
                else if (DigitAfterDecimal == 3)
                {
                    AmountFormat = "0.000";
                }
            }
            catch (Exception ex)
            {
                AmountFormat = "0.00";
            }
            return AmountFormat;
        }

        public static string GetAmountFormateForLateAndEarly()
        {
            int DigitAfterDecimal = 0;

            try
            {
                LateDeductionSetting lateDeductionSetting = new LateDeductionDB().GetLateDeductionSetting();
                DigitAfterDecimal = lateDeductionSetting.DigitAfterDecimalForLate;
                if (DigitAfterDecimal == 0)
                {
                    AmountFormat = "0";
                }
                else if (DigitAfterDecimal == 1)
                {
                    AmountFormat = "0.0";
                }
                else if (DigitAfterDecimal == 2)
                {
                    AmountFormat = "0.00";
                }
                else if (DigitAfterDecimal == 3)
                {
                    AmountFormat = "0.000";
                }
            }
            catch (Exception ex)
            {
                AmountFormat = "0";
            }
            return AmountFormat;
        }

        ///// <summary>
        ///// 3DES encryption using key
        ///// </summary>
        ///// <returns></returns>
        //public static string EncryptStringUsing3DES(string Message)
        //{
        //    if (Message != null)
        //    {
        //        string Key = ConfigurationManager.AppSettings["SecurityKey"];
        //        byte[] Results;
        //        System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

        //        // hash the Key using SHA256
        //        // we use the SHA256 hash generator as the result is a 192 bit array
        //        // which is a valid length for the TripleDES encoder we use below
        //        SHA256CryptoServiceProvider HashProvider = new SHA256CryptoServiceProvider();
        //        byte[] temp = HashProvider.ComputeHash(UTF8.GetBytes(Key));
        //        byte[] TDESKey = new byte[24];
        //        Array.Copy(temp, TDESKey, 24);
        //        //MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
        //        //byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Key));

        //        //Create a new TripleDESCryptoServiceProvider object
        //        TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

        //        //Setup the encoder
        //        TDESAlgorithm.Key = TDESKey;
        //        TDESAlgorithm.Mode = CipherMode.ECB;
        //        TDESAlgorithm.Padding = PaddingMode.PKCS7;

        //        //Convert the input string to a byte[]
        //        byte[] DataToEncrypt = UTF8.GetBytes(Message);

        //        // Attempt to encrypt the string
        //        try
        //        {
        //            ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
        //            Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
        //        }
        //        finally
        //        {
        //            // Clear the TripleDes and Hashprovider services of any sensitive information
        //            TDESAlgorithm.Clear();
        //            HashProvider.Clear();
        //        }

        //        // Return the encrypted string as a base64 encoded string
        //        return Convert.ToBase64String(Results);
        //    }
        //    else
        //        return null;
        //}

        public static string decryptPassword(string password)
        {
            byte[] HashKey = SecureIt.GetHashKey(Properties.Settings.Default["salt1"].ToString());
            string Decrypted = SecureIt.Decrypt(HashKey, password);
            return Decrypted;
        }

        //public static string EncryptPassword(string password)
        //{
        //    byte[] HashKey = SecureIt.GetHashKey(Properties.Settings.Default["salt1"].ToString());
        //    string encrypted = SecureIt.Encrypt(HashKey, password);
        //    return encrypted;
        //}

        /// <summary>
        /// Sends the Mail
        /// </summary>
        /// <param name="sendto"></param>
        /// <param name="from"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="bodyHTML"></param>
        /// <returns></returns>
        ///        

        public OperationDetails SendMail(string ReceiverEmailAddress, string Subject, string Body)
        {
            return SendMail(ReceiverEmailAddress, Subject, Body, null);
        }

        public OperationDetails SendMail(string ReceiverEmailAddress, string Subject, string Body, string CC)
        {
            OperationDetails op = new OperationDetails();
            CommonDB objCommonDB = new CommonDB();
            SMTPConfig objSMTP = new SMTPConfig();
            objSMTP = objCommonDB.GetSMTPConfig();

            try
            {
                if (objSMTP.SMTPServer != "")
                {
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(objSMTP.FromEmailAddress, objSMTP.FromDisplayName); //From Email Id
                    mailMessage.Subject = Subject; //Subject of Email
                    mailMessage.Body = Body;
                    mailMessage.IsBodyHtml = true;
                    MailAddress MailID = new MailAddress(ReceiverEmailAddress);
                    mailMessage.To.Add(MailID);
                    mailMessage.ReplyToList.Add(new MailAddress(objSMTP.ReplyEmail));
                    if (!string.IsNullOrEmpty(CC))
                        mailMessage.CC.Add(CC);
                    SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
                    smtp.Host = objSMTP.SMTPServer; //host of emailaddress for example smtp.gmail.com etc  //network and security related credentials
                    smtp.EnableSsl = objSMTP.SecureConnection;
                    NetworkCredential NetworkCred = new NetworkCredential();
                    NetworkCred.UserName = objSMTP.Username;
                    NetworkCred.Password = objSMTP.Password;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = objSMTP.PortNumber;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Timeout = 20000;
                    smtp.Send(mailMessage); //sending Email                           
                    op.Message = "Email sent successfully";
                    op.Success = true;
                    op.CssClass = "success";
                }
                else
                {
                    op.Message = "SMTP configuration is not available.";
                    op.Success = false;
                    op.CssClass = "error";
                }
            }
            catch (Exception ex)
            {
                op.Message = "SMTP Configuration:" + ex.Message;
                op.Success = false;
                op.CssClass = "error";
            }
            return op;
        }

        public static string RenderViewToString(ControllerContext context, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            var viewData = new ViewDataDictionary(model);

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static byte[] CreateExcelsheet(DataSet ds, bool isFormReport = false)
        {
            using (MemoryStream mem = new MemoryStream())
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(mem, SpreadsheetDocumentType.Workbook))
                {
                    // create the workbook
                    spreadSheet.AddWorkbookPart();
                    spreadSheet.WorkbookPart.Workbook = new Workbook();     // create the worksheet
                    spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet = new Worksheet();
                    
                    // create sheet data
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.AppendChild(new SheetData());

                    //// save worksheet
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.Save();

                    //// create the worksheet to workbook relation
                    spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());

                    foreach (System.Data.DataTable table in ds.Tables)
                    {
                        var sheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();                      
                        var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                        sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);
                      
                        DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                        string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(sheetPart);

                        uint sheetId = 1;
                        if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                        {
                            sheetId = sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                        sheets.Append(sheet);
                     
                        if (!isFormReport)
                        {
                            SchoolInformation sc = new SchoolInformation();
                            sc = new SchoolInformationDB().GetBasicSchoolInformation();
                            string Schoolname = sc.SchoolName_1;
                            if (!string.IsNullOrEmpty(sc.CampusName_1))
                            {
                                Schoolname += "-" + sc.CampusName_1;
                            }

                            //// School Basic Info
                            DocumentFormat.OpenXml.Spreadsheet.Row BasicInformationRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            DocumentFormat.OpenXml.Spreadsheet.Cell SchoolInfoCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            SchoolInfoCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            SchoolInfoCell.StyleIndex = 1;
                            SchoolInfoCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("School Name: " + Schoolname);
                            BasicInformationRow.AppendChild(SchoolInfoCell);
                            //DocumentFormat.OpenXml.Spreadsheet.Row BasicInfo = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            sheetData.AppendChild(BasicInformationRow);
                            ///////////


                            //// CurrentDate
                            DocumentFormat.OpenXml.Spreadsheet.Row TodaysDate = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            DocumentFormat.OpenXml.Spreadsheet.Cell TodaysDateCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            TodaysDateCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            TodaysDateCell.StyleIndex = 1;
                            TodaysDateCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Exported Date: " + DateTime.Now.ToString("dd MMM yyyy"));
                            TodaysDate.AppendChild(TodaysDateCell);
                            //DocumentFormat.OpenXml.Spreadsheet.Row BasicInfo = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            sheetData.AppendChild(TodaysDate);
                            ///////////
                            ////Blank Row
                            sheetData.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Row());
                        }
                        
                        DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        /// Styling 
                        WorkbookStylesPart stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                        //stylesPart.Stylesheet = CreateStylesheet();
                        stylesPart.Stylesheet = GenerateStyleSheetForCustomizeExcelSheet();
                        stylesPart.Stylesheet.Save();

                        if(isFormReport)
                        {
                            // Create custom widths for columns
                            UInt32Value i = 1;
                            Columns lstColumns = sheetPart.Worksheet.GetFirstChild<Columns>();
                            Boolean needToInsertColumns = false;
                            if (lstColumns == null)
                            {
                                lstColumns = new Columns();
                                needToInsertColumns = true;
                            }
                            foreach (System.Data.DataColumn column in table.Columns)
                            {
                                lstColumns.Append(new Column() { Min = i, Max = i, Width = 22.5, CustomWidth = true, BestFit = true });
                                i++;
                            }

                            // Only insert the columns if we had to create a new columns element
                            if (needToInsertColumns)
                                sheetPart.Worksheet.InsertAt(lstColumns, 0);

                            // Get the sheetData cells
                            sheetData = sheetPart.Worksheet.GetFirstChild<SheetData>();
                        }
                      
                        List<String> columns = new List<string>();
                        foreach (System.Data.DataColumn column in table.Columns)
                        {
                            columns.Add(column.ColumnName);

                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                            if (isFormReport)                              
                                cell.StyleIndex = 11;
                            else
                                cell.StyleIndex = 1;
                            headerRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(headerRow);

                        foreach (System.Data.DataRow dsrow in table.Rows)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            foreach (String col in columns)
                            {
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                CellValue cellValue = new CellValue();
                                if (table.Columns[col].Caption == "hyperlink")
                                {
                                    string[] values = dsrow[col].ToString().Split('|');
                                    CellFormula cellFormula = new CellFormula() { Space = SpaceProcessingModeValues.Preserve };
                                    cellFormula.Text = @"HYPERLINK(""" + values[0] + @""", """ + values[1] + @""")";
                                    cellValue.Text = values[1];
                                    cell.Append(cellFormula);
                                    cell.StyleIndex = 10U;

                                    Run run = new Run();
                                    RunProperties runProperties = new RunProperties();
                                    Underline underline = new Underline();
                                    Color color = new Color() { Rgb = "000000FFF" };

                                    runProperties.Append(underline);
                                    runProperties.Append(color);
                                    run.RunProperties = runProperties;
                                    cell.Append(run);
                                }
                                else
                                {
                                    cellValue.Text = dsrow[col].ToString();
                                }
                                   
                                cell.Append(cellValue);
                                newRow.AppendChild(cell);

                            }

                            sheetData.AppendChild(newRow);
                        }
                     
                    }

                    spreadSheet.WorkbookPart.Workbook.Save();
                    spreadSheet.Close();
                    return mem.ToArray();
                }
            }
        }
       
        public static string CreateAndWriteIntoExcelSheet(DataSet ds, string fileName)
        {
              string filePath = System.Web.HttpContext.Current.Server.MapPath("~/Downloads/" + fileName);
              SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook);             
               // create the workbook
               spreadSheet.AddWorkbookPart();
               spreadSheet.WorkbookPart.Workbook = new Workbook();     // create the worksheet
               spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
               spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet = new Worksheet();

               // create sheet data
               spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.AppendChild(new SheetData());

               //// save worksheet
               spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.Save();

               //// create the worksheet to workbook relation
               spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());

                  foreach (System.Data.DataTable table in ds.Tables)
                  {
                      var sheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                      var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                      sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                      DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                      string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(sheetPart);

                      uint sheetId = 1;
                      if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                      {
                          sheetId = sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                      }

                      DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                      sheets.Append(sheet);
                                         
                      ////Blank Row
                      sheetData.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Row());         

                      DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                      /// Styling 
                      WorkbookStylesPart stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                      stylesPart.Stylesheet = GenerateStyleSheetForCustomizeExcelSheet();
                      stylesPart.Stylesheet.Save();


                      List<String> columns = new List<string>();
                      foreach (System.Data.DataColumn column in table.Columns)
                      {
                          columns.Add(column.ColumnName);

                          DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                          cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                          cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                          cell.StyleIndex = 9;
                          headerRow.AppendChild(cell);
                      }


                      sheetData.AppendChild(headerRow);

                      foreach (System.Data.DataRow dsrow in table.Rows)
                      {
                          DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();                                                               
                          foreach (String col in columns)
                          {
                              DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();                       
                              cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                              cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                              cell.StyleIndex = 6;
                              newRow.AppendChild(cell);
                          }

                          sheetData.AppendChild(newRow);
                      }

                  }

            spreadSheet.WorkbookPart.Workbook.Save();
            spreadSheet.Close();
            return filePath;
        }

        public static Stylesheet GenerateStyleSheetForCustomizeExcelSheet()
        {
            int noOfDecimals = new PayrollDB().GetDigitAfterDecimal();
            return new Stylesheet(
                new Fonts(
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 0 – The default font.
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 1 – The bold font.
                        new Bold(),
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 2 – The Italic font with bold.
                        new Bold(),
                        new Italic(),
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 3 – The Times Roman font. with 16 size
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 4 – The Blue font.
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = "0000EE" },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(
                        new Bold(),                                                                                            // Index 5 – The Bold font with White color text
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 12 },
                        new Color() { Rgb = "ffffff" },
                        new FontName() { Val = "Verdana" })
                ),
                new Fills(
                    new Fill(                                                           // Index 0 – The default fill.
                        new PatternFill() { PatternType = PatternValues.None }),
                    new Fill(                                                           // Index 1 – The default fill of gray 125 (required)
                        new PatternFill() { PatternType = PatternValues.Gray125 }),
                    new Fill(                                                           // Index 2 – The yellow fill.
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                        )
                        { PatternType = PatternValues.Solid }),
                    new Fill(                                                           // Index 3 – The blue fill.
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "000000FFF" } }
                        )),
                     new Fill(              
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "00302F80" } }
                        )
                        { PatternType = PatternValues.Solid })                      // Index 4 - The Blue background color fill
                ),
                new Borders(
                    new Border(                                                         // Index 0 – The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 – Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Hair },
                        new RightBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Hair },
                        new TopBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Hair },
                        new BottomBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Hair },
                        new DiagonalBorder()),
                    new Border(                                                         // Index 2 – Applies a Top border to a cell                      
                        new TopBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Hair },                       
                        new DiagonalBorder())
                 
                ),
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 – The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 1 – Bold 
                    new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 – Italic
                    new CellFormat(
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                        ) { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true, ApplyAlignment = true },       // Index 3 – Times Roman
                    new CellFormat(){ FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 – Yellow Fill
                    new CellFormat(                                                                   // Index 5 – Alignment
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                    )
                    { FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true },
                    new CellFormat(
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center },
                        new NumberingFormat() { NumberFormatId = UInt32Value.FromUInt32(4), FormatCode = "#,##0.0000" })
                    { FontId = 0, FillId = 0, BorderId = 1, NumberFormatId = 2, ApplyBorder = true, ApplyFont = true, ApplyAlignment = true, ApplyNumberFormat = true },     // Index 6 – Border with default font
                    new CellFormat(new Alignment() { WrapText = true }) { FontId = 0, FillId = 0, BorderId = 0 }, // Index 7 – Border  
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 2, ApplyBorder = true, ApplyFont = true } ,    // Index 8 – top cell Border 
                    new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center, MergeCell = "A:B" }) { FontId = 1, FillId = 0, BorderId = 1, ApplyBorder = true, ApplyFont = true , ApplyAlignment = true },  // Index 9 – Border with bold         
                    new CellFormat() { FontId = 4, FillId = 3, BorderId = 0, ApplyFill = true },       // Index 10 – Blue Fill
                    new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }) { FontId = 5, FillId = 4, BorderId = 0, ApplyFill = true, ApplyAlignment = true }// Index 11 -- styles for forms report column header                 
                )
            ); // return
        }

        public static Stylesheet GenerateStyleSheet()
        {
            return new Stylesheet(
                new Fonts(
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 0 – The default font.
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 1 – The bold font.
                        new Bold(),
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 2 – The Italic font.
                        new Italic(),
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new DocumentFormat.OpenXml.Spreadsheet.Font(                                                               // Index 2 – The Times Roman font. with 16 size
                        new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Times New Roman" })
                ),
                new Fills(
                    new Fill(                                                           // Index 0 – The default fill.
                        new PatternFill() { PatternType = PatternValues.None }),
                    new Fill(                                                           // Index 1 – The default fill of gray 125 (required)
                        new PatternFill() { PatternType = PatternValues.Gray125 }),
                    new Fill(                                                           // Index 2 – The yellow fill.
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                        )
                        { PatternType = PatternValues.Solid })
                ),
                new Borders(
                    new Border(                                                         // Index 0 – The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 – Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new RightBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new TopBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new BottomBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                ),
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 – The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 1 – Bold 
                    new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 – Italic
                    new CellFormat() { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 3 – Times Roman
                    new CellFormat() { FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 – Yellow Fill
                    new CellFormat(                                                                   // Index 5 – Alignment
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                    )
                    { FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true },
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true },     // Index 6 – Border
                    new CellFormat(new Alignment() { WrapText = true }) { FontId = 0, FillId = 0, BorderId = 0 }// Index 7 – Border
                    
                )
            ); // return
        }

        public static Document GenratePDF(HttpResponseBase resp, DataSet ds, bool isLandscape)
        {
            Document pdfDocument;
            if (isLandscape)
            {
                pdfDocument = new Document(PageSize.A4.Rotate(), 0f, 0f, 170f, 20f);  //Landscape
            }
            else
            {
                pdfDocument = new Document(PageSize.A4, 0f, 0f, 150f, 20f);   //Postrait
            }


            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {
                pdfDocument.Open();

                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font MHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 20, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));


                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;

                // create two column table
                PdfPTable head = new PdfPTable(4);
                head.TotalWidth = page.Width;
                head.LockedWidth = true;
                if (isLandscape)
                {
                    float[] widths = new float[] { 180f, 160f, 300f, 180f };  //landscape
                    head.SetWidths(widths);
                }
                else
                {
                    float[] widths = new float[] { 60f, 135f, 250f, 150f };
                    head.SetWidths(widths);
                }



                //Blanck Cell
                PdfPCell c0 = new PdfPCell();
                c0.Border = PdfPCell.NO_BORDER;
                head.AddCell(c0);


                // add image; PdfPCell() overload sizes image to fit cell
                PdfPCell c = new PdfPCell(jpg, true);
                c.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //c.HorizontalAlignment = Element.ALIGN_LEFT;
                c.FixedHeight = cellHeight;

                c.Border = PdfPCell.NO_BORDER;
                c.PaddingLeft = 10f;
                c.PaddingTop = 10f;
                c.PaddingBottom = 20f;
                head.AddCell(c);
                // add the header text

                PdfPCell c1 = new PdfPCell(new Phrase(Schoolname, Heading)); //Landscape
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape

                c1.PaddingTop = 50F;
                c1.FixedHeight = cellHeight;
                head.AddCell(c1);

                PdfPCell c2 = new PdfPCell(new Phrase("Exported Date: " + DateTime.Now.ToString("dd MMM yyyy"), DateFont));
                c2.PaddingTop = 10f;
                c2.PaddingRight = 20f;
                c2.HorizontalAlignment = Element.ALIGN_RIGHT;
                c2.Border = PdfPCell.NO_BORDER;
                head.AddCell(c2);


                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );
                //
                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));


                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                //
                pdfDocument.SetMargins(0f, 0f, 20f, 20f);
                PdfPTable content = new PdfPTable(ds.Tables[0].Columns.Count);
                content.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                content.TotalWidth = page.Width;
                content.WidthPercentage = 95;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {


                    PdfPCell tableColumnCell = new PdfPCell(new Phrase(dc.ToString(), MHeading));
                    tableColumnCell.SetLeading(0.0f, 1.5f);
                    tableColumnCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    tableColumnCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    tableColumnCell.PaddingBottom = 10f;
                    tableColumnCell.PaddingTop = 1f;
                    content.AddCell(tableColumnCell);
                }
                foreach (DataRow DR in ds.Tables[0].Rows)
                {
                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        PdfPCell tableCell;
                        if (IsEnglish(DR[i].ToString()))
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                        else
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), arabicFont));

                        tableCell.SetLeading(0.0f, 1.5f);
                        tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tableCell.PaddingBottom = 10f;
                        tableCell.PaddingTop = 1f;
                        content.AddCell(tableCell);
                    }
                }
                pdfDocument.Add(content);
                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static Document GenrateEmployeeProfilePDF(HttpResponseBase resp, DataSet ds, int EmployeeId)
        {
            Document pdfDocument;
            pdfDocument = new Document(PageSize.A4.Rotate(), 30f, 20f, 100f, 30f);  //Landscape

            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {
                SchoolInfo.cellHeight = pdfDocument.TopMargin;
                SchoolInfo.page = pdfDocument.PageSize;
                SchoolInfo.isHeaderRepeated = false;
                SchoolInfo.pageCount = 0;
                wri.PageEvent = new itextEvents();
                pdfDocument.Open();
                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font MHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 20, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Heading1 = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));
                iTextSharp.text.Font MHeadingWithColor = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(173, 216, 230)));


                #region Header Part
                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.ReportLogo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;

                // create two column table
                PdfPTable head = new PdfPTable(3);
                head.TotalWidth = page.Width;
                head.PaddingTop = 10f;
                head.LockedWidth = true;

                float[] widths = new float[] { 180f, 460f, 180f };  //landscape
                head.SetWidths(widths);


                //Blanck Cell
                PdfPCell c0 = new PdfPCell(jpg, true);
                c0.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                c0.Border = PdfPCell.NO_BORDER;
                c0.PaddingTop = 10f;
                c0.FixedHeight = cellHeight;
                head.AddCell(c0);

                PdfPTable SchoolInfoTable = new PdfPTable(1);
                SchoolInfoTable.TotalWidth = page.Width;
                SchoolInfoTable.PaddingTop = 10f;
                SchoolInfoTable.LockedWidth = true;
                float[] widths1 = new float[] { 440f };  //landscape
                SchoolInfoTable.SetWidths(widths1);

                PdfPCell schoolName = new PdfPCell(new Phrase(Schoolname, Heading));
                schoolName.Border = PdfPCell.NO_BORDER;
                schoolName.HorizontalAlignment = Element.ALIGN_CENTER;
                SchoolInfoTable.AddCell(schoolName);


                PdfPCell PoBox = new PdfPCell(new Phrase("P.O.Box:" + sc.POBox + " " + sc.City + " " + sc.Country, MHeading));
                PoBox.Border = PdfPCell.NO_BORDER;
                PoBox.HorizontalAlignment = Element.ALIGN_CENTER;
                SchoolInfoTable.AddCell(PoBox);

                PdfPCell Phone = new PdfPCell(new Phrase("Tel:" + sc.Phone + "/Fax:" + sc.Fax, MHeading));
                Phone.Border = PdfPCell.NO_BORDER;
                Phone.HorizontalAlignment = Element.ALIGN_CENTER;
                SchoolInfoTable.AddCell(Phone);


                PdfPCell c1 = new PdfPCell(); //Landscape
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape

                c1.PaddingTop = 10F;
                c1.FixedHeight = cellHeight;
                c1.AddElement(SchoolInfoTable);
                head.AddCell(c1);


                PdfPCell c2 = new PdfPCell(new Phrase("", DateFont));
                c2.PaddingTop = 10f;
                c2.PaddingRight = 20f;
                c2.HorizontalAlignment = Element.ALIGN_RIGHT;
                c2.Border = PdfPCell.NO_BORDER;
                head.AddCell(c2);


                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );
                //

                #endregion

                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));

                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                //
                pdfDocument.SetMargins(30f, 20f, 20f, 30f);

                PdfPTable BlankTable = new PdfPTable(1);
                BlankTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                BlankTable.TotalWidth = page.Width;
                BlankTable.WidthPercentage = 95;

                PdfPCell Blankcell = new PdfPCell(new Phrase("", MHeading));
                Blankcell.FixedHeight = 20f;
                Blankcell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Blankcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                Blankcell.Border = PdfPCell.NO_BORDER;
                BlankTable.AddCell(Blankcell);
                pdfDocument.Add(BlankTable);

                PdfPTable ApplicantProfileLabel = new PdfPTable(1);
                ApplicantProfileLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                ApplicantProfileLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                //ApplicantProfileLabel.TotalWidth = page.Width;
                ApplicantProfileLabel.WidthPercentage = 98;

                PdfPCell ApplicantProfileCell = new PdfPCell(new Phrase("Applicant Profile", MHeading));
                ApplicantProfileCell.FixedHeight = 17f;
                ApplicantProfileCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                ApplicantProfileCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ApplicantProfileCell.Border = PdfPCell.NO_BORDER;
                ApplicantProfileCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                ApplicantProfileLabel.AddCell(ApplicantProfileCell);

                PdfPCell EmpAltId = new PdfPCell(new Phrase("For Employee " + ds.Tables[0].Rows[0][1], MHeading));
                EmpAltId.FixedHeight = 17f;
                EmpAltId.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmpAltId.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                EmpAltId.Border = PdfPCell.NO_BORDER;
                EmpAltId.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                ApplicantProfileLabel.AddCell(EmpAltId);
                pdfDocument.Add(ApplicantProfileLabel);

                PdfPTable pTable = new PdfPTable(2);
                float[] pTableWidths = { 700, 142 };
                pTable.SetWidths(pTableWidths);
                pTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                pTable.WidthPercentage = 98;
                int columnCount = 1;

                //Basic Information
                #region Basic Information
                if (ds.Tables.Count >= 1)
                {
                    PdfPTable empPersonalIdentityLabel = new PdfPTable(1);
                    empPersonalIdentityLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    empPersonalIdentityLabel.TotalWidth = page.Width;
                    empPersonalIdentityLabel.WidthPercentage = 95;
                    empPersonalIdentityLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    PdfPCell empPersonalIdentityCell = new PdfPCell(new Phrase("Basic Information", MHeading));
                    empPersonalIdentityCell.PaddingLeft = 0f;
                    empPersonalIdentityCell.FixedHeight = 30f;
                    empPersonalIdentityCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    empPersonalIdentityCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    empPersonalIdentityCell.Border = PdfPCell.NO_BORDER;
                    empPersonalIdentityLabel.AddCell(empPersonalIdentityCell);
                    pdfDocument.Add(empPersonalIdentityLabel);


                    PdfPTable PersonalIdentityTable = new PdfPTable(6);
                    PersonalIdentityTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    // PersonalIdentityTable.TotalWidth = 600f;
                    PersonalIdentityTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    // PersonalIdentityTable.WidthPercentage = 80;

                    foreach (DataRow DR in ds.Tables[0].Rows)
                    {
                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                        {
                            Paragraph para = new Paragraph(DR[i].ToString(), TableContent);
                            para.SetLeading(0, 1);

                            PdfPCell cell = new PdfPCell();
                            cell.MinimumHeight = 23;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.AddElement(para);
                            //table.addCell(cell);


                            if (i % 2 == 0)
                            {
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                            }

                            PersonalIdentityTable.AddCell(cell);
                            columnCount++;
                        }

                        if (columnCount % 6 == 0)
                        {
                            PersonalIdentityTable.CompleteRow();
                        }
                    }
                    PersonalIdentityTable.Complete = true;
                    pTable.AddCell(new PdfPCell(PersonalIdentityTable) { Border = PdfPCell.NO_BORDER });
                }
                iTextSharp.text.Image profilePic;
                EmployeeDB employeeDB = new EmployeeDB();
                var dataContain = employeeDB.GetUserImage(EmployeeId);
                if (dataContain.Count() > 0)
                    profilePic = iTextSharp.text.Image.GetInstance(dataContain);
                else
                {
                    profilePic = iTextSharp.text.Image.GetInstance(GetBlankImage());

                }

                PdfPCell Img = new PdfPCell(profilePic, true);
                Img.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                // Img.Border = PdfPCell.NO_BORDER;
                Img.PaddingTop = 5f;
                Img.PaddingBottom = 5f;
                Img.PaddingLeft = 5f;
                Img.PaddingRight = 5f;
                Img.FixedHeight = 20f;
                pTable.AddCell(Img);
                pdfDocument.Add(pTable);

                #endregion
                //

                //Assignment details
                #region Assignment details
                if (ds.Tables.Count >= 2)
                {
                    PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                    empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                    empPersonalIdentityDetailLabel.WidthPercentage = 95;
                    empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Assignment Detalls", MHeading));
                    empPersonalIdentityDetailCell.PaddingLeft = 0f;
                    empPersonalIdentityDetailCell.FixedHeight = 30f;
                    empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                    empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                    pdfDocument.Add(empPersonalIdentityDetailLabel);

                    PdfPTable AssignmentDetailTable = new PdfPTable(6);
                    AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    AssignmentDetailTable.TotalWidth = page.Width;
                    AssignmentDetailTable.WidthPercentage = 98;
                    AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    columnCount = 1;
                    foreach (DataRow DR in ds.Tables[1].Rows)
                    {
                        for (int i = 0; i < ds.Tables[1].Columns.Count; i++)
                        {

                            Paragraph para = new Paragraph(DR[i].ToString(), TableContent);
                            para.SetLeading(0, 1f);

                            PdfPCell cell = new PdfPCell();
                            cell.MinimumHeight = 23;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.AddElement(para);
                            cell.PaddingBottom = 2f;


                            if (i % 2 == 0)
                            {
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                            }
                            AssignmentDetailTable.AddCell(cell);

                        }
                        columnCount++;
                        if (columnCount % 6 == 0)
                        {
                            AssignmentDetailTable.CompleteRow();
                        }

                    }
                    pdfDocument.Add(AssignmentDetailTable);
                }
                #endregion
                //
                columnCount = 1;
                //Assignment details 2
                #region Assignment details 2
                if (ds.Tables.Count >= 3)
                {
                    if (ds.Tables[2].Rows.Count > 1)
                    {
                        DataRow toInsert = ds.Tables[2].NewRow();

                        // insert in the desired place
                        ds.Tables[2].Rows.InsertAt(toInsert, 0);
                    }
                    PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[2].Columns.Count);
                    AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    AssignmentDetailTable.TotalWidth = page.Width;
                    AssignmentDetailTable.WidthPercentage = 98;
                    AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    foreach (DataColumn dc in ds.Tables[2].Columns)
                    {

                        //Paragraph para = new Paragraph(DR[i].ToString(), TableContent);
                        //para.SetLeading(0, 1f);

                        //PdfPCell cell = new PdfPCell();
                        //cell.MinimumHeight = 23;
                        //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        //cell.AddElement(para);
                        //cell.PaddingBottom = 2f;


                        Paragraph p = new Paragraph(dc.ToString(), TableContent);
                        p.SetLeading(0, 1f);
                        p.Alignment = Element.ALIGN_CENTER;

                        PdfPCell tableColumnCell = new PdfPCell();
                        //tableColumnCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        //tableColumnCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tableColumnCell.MinimumHeight = 28;
                        tableColumnCell.AddElement(p);
                        tableColumnCell.PaddingBottom = 2f;
                        tableColumnCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        // content.AddCell(new Phrase(dc.ToString(), MHeading));
                        tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                        AssignmentDetailTable.AddCell(tableColumnCell);
                    }



                    columnCount = 1;
                    foreach (DataRow DR in ds.Tables[2].Rows)
                    {
                        for (int i = 0; i < ds.Tables[2].Columns.Count; i++)
                        {
                            PdfPCell tableCell;
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                            tableCell.SetLeading(0.0f, 1.5f);
                            tableCell.MinimumHeight = 23f;
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.PaddingBottom = 5f;
                            tableCell.PaddingTop = 1f;
                            AssignmentDetailTable.AddCell(tableCell);
                        }

                    }



                    pdfDocument.Add(AssignmentDetailTable);
                }
                #endregion

                #region Contact Details
                if (ds.Tables.Count >= 4)
                {
                    PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                    empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                    empPersonalIdentityDetailLabel.WidthPercentage = 95;
                    empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Contact Details", MHeading));
                    empPersonalIdentityDetailCell.PaddingLeft = 0f;
                    empPersonalIdentityDetailCell.FixedHeight = 30f;
                    empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                    empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                    pdfDocument.Add(empPersonalIdentityDetailLabel);



                    PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[3].Columns.Count);
                    AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    AssignmentDetailTable.TotalWidth = page.Width;
                    AssignmentDetailTable.WidthPercentage = 98;
                    AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    foreach (DataColumn dc in ds.Tables[3].Columns)
                    {
                        Paragraph p = new Paragraph(dc.ToString(), TableContent);
                        p.Alignment = Element.ALIGN_CENTER;

                        PdfPCell tableColumnCell = new PdfPCell();
                        //tableColumnCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        //tableColumnCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tableColumnCell.AddElement(p);
                        tableColumnCell.PaddingBottom = 10f;
                        tableColumnCell.MinimumHeight = 23f;
                        // content.AddCell(new Phrase(dc.ToString(), MHeading));
                        tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                        AssignmentDetailTable.AddCell(tableColumnCell);
                    }

                    AssignmentDetailTable.CompleteRow();

                    columnCount = 1;
                    foreach (DataRow DR in ds.Tables[3].Rows)
                    {
                        for (int i = 0; i < ds.Tables[3].Columns.Count; i++)
                        {
                            PdfPCell tableCell;
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                            tableCell.SetLeading(0.0f, 1.5f);
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.MinimumHeight = 23f;
                            tableCell.PaddingBottom = 5f;
                            tableCell.PaddingTop = 1f;
                            AssignmentDetailTable.AddCell(tableCell);

                        }
                    }

                    AssignmentDetailTable.CompleteRow();

                    pdfDocument.Add(AssignmentDetailTable);
                }

                #endregion

                #region Details of Family Members Grade(E & Above)
                if (ds.Tables.Count >= 5)
                {
                    if (ds.Tables[4].Rows.Count == 0)
                    {
                        DataRow toInsert = ds.Tables[4].NewRow();
                        ds.Tables[4].Rows.InsertAt(toInsert, 0);
                    }

                    PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                    empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                    empPersonalIdentityDetailLabel.WidthPercentage = 95;
                    empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Details of Family Members Grade(E & Above)", MHeading));
                    empPersonalIdentityDetailCell.PaddingLeft = 0f;
                    empPersonalIdentityDetailCell.FixedHeight = 30f;
                    empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                    empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                    pdfDocument.Add(empPersonalIdentityDetailLabel);

                    PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[4].Columns.Count);
                    AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    AssignmentDetailTable.TotalWidth = page.Width;
                    AssignmentDetailTable.WidthPercentage = 98;
                    AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    foreach (DataColumn dc in ds.Tables[4].Columns)
                    {
                        Paragraph p = new Paragraph(dc.ToString(), TableContent);
                        p.Alignment = Element.ALIGN_CENTER;

                        PdfPCell tableColumnCell = new PdfPCell();
                        tableColumnCell.AddElement(p);
                        tableColumnCell.MinimumHeight = 23f;
                        tableColumnCell.PaddingBottom = 10f;
                        tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                        AssignmentDetailTable.AddCell(tableColumnCell);
                    }

                    columnCount = 1;
                    foreach (DataRow DR in ds.Tables[4].Rows)
                    {
                        for (int i = 0; i < ds.Tables[4].Columns.Count; i++)
                        {
                            PdfPCell tableCell;
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                            tableCell.SetLeading(0.0f, 1.5f);
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.MinimumHeight = 23f;
                            tableCell.PaddingBottom = 5f;
                            tableCell.PaddingTop = 1f;
                            AssignmentDetailTable.AddCell(tableCell);

                        }
                    }
                    pdfDocument.Add(AssignmentDetailTable);
                }
                #endregion

                PermissionModel permissionModel = CommonHelper.CheckPermissionForUser("/Academic");

                if (permissionModel.IsMainNavigationPermission)
                {
                    #region Educational Qualification
                    if (ds.Tables.Count >= 6)
                    {
                        if (ds.Tables[5].Rows.Count == 0)
                        {
                            DataRow toInsert = ds.Tables[5].NewRow();
                            ds.Tables[5].Rows.InsertAt(toInsert, 0);
                        }
                        PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                        empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                        empPersonalIdentityDetailLabel.WidthPercentage = 95;
                        empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Educational Qualification", MHeading));
                        empPersonalIdentityDetailCell.PaddingLeft = 0f;
                        empPersonalIdentityDetailCell.FixedHeight = 30f;
                        empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                        empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                        pdfDocument.Add(empPersonalIdentityDetailLabel);

                        PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[5].Columns.Count);
                        AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        AssignmentDetailTable.TotalWidth = page.Width;
                        AssignmentDetailTable.WidthPercentage = 98;
                        AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        foreach (DataColumn dc in ds.Tables[5].Columns)
                        {
                            Paragraph p = new Paragraph(dc.ToString(), TableContent);
                            p.Alignment = Element.ALIGN_CENTER;

                            PdfPCell tableColumnCell = new PdfPCell();
                            tableColumnCell.AddElement(p);
                            tableColumnCell.MinimumHeight = 23f;
                            tableColumnCell.PaddingBottom = 10f;
                            tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                            AssignmentDetailTable.AddCell(tableColumnCell);
                        }



                        columnCount = 1;
                        foreach (DataRow DR in ds.Tables[5].Rows)
                        {
                            for (int i = 0; i < ds.Tables[5].Columns.Count; i++)
                            {
                                PdfPCell tableCell;
                                tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                                tableCell.SetLeading(0.0f, 1.5f);
                                tableCell.MinimumHeight = 23;
                                tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.PaddingBottom = 5f;
                                tableCell.PaddingTop = 1f;
                                AssignmentDetailTable.AddCell(tableCell);

                            }
                        }



                        pdfDocument.Add(AssignmentDetailTable);
                    }
                    #endregion

                    #region Profesional Qualification
                    if (ds.Tables.Count >= 7)
                    {
                        if (ds.Tables[6].Rows.Count == 0)
                        {
                            DataRow toInsert = ds.Tables[6].NewRow();
                            ds.Tables[6].Rows.InsertAt(toInsert, 0);
                        }
                        PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                        empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                        empPersonalIdentityDetailLabel.WidthPercentage = 95;
                        empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Profesional Qualification", MHeading));
                        empPersonalIdentityDetailCell.FixedHeight = 30f;
                        empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                        empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                        pdfDocument.Add(empPersonalIdentityDetailLabel);

                        PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[6].Columns.Count);
                        AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        AssignmentDetailTable.TotalWidth = page.Width;
                        AssignmentDetailTable.WidthPercentage = 98;
                        AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        foreach (DataColumn dc in ds.Tables[6].Columns)
                        {
                            Paragraph p = new Paragraph(dc.ToString(), TableContent);
                            p.Alignment = Element.ALIGN_CENTER;

                            PdfPCell tableColumnCell = new PdfPCell();
                            tableColumnCell.AddElement(p);
                            tableColumnCell.PaddingBottom = 10f;
                            tableColumnCell.MinimumHeight = 23;
                            tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                            AssignmentDetailTable.AddCell(tableColumnCell);
                        }

                        columnCount = 1;
                        foreach (DataRow DR in ds.Tables[6].Rows)
                        {
                            for (int i = 0; i < ds.Tables[6].Columns.Count; i++)
                            {
                                PdfPCell tableCell;
                                tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                                tableCell.SetLeading(0.0f, 1.5f);
                                tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.PaddingBottom = 5f;
                                tableCell.MinimumHeight = 23;
                                tableCell.PaddingTop = 1f;
                                AssignmentDetailTable.AddCell(tableCell);

                            }
                        }
                        pdfDocument.Add(AssignmentDetailTable);
                    }

                    #endregion

                    #region Professional Accreditation
                    if (ds.Tables.Count >= 8)
                    {
                        if (ds.Tables[7].Rows.Count == 0)
                        {
                            DataRow toInsert = ds.Tables[7].NewRow();
                            ds.Tables[7].Rows.InsertAt(toInsert, 0);
                        }

                        PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                        empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                        empPersonalIdentityDetailLabel.WidthPercentage = 95;
                        empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Professional Accreditation", MHeading));
                        empPersonalIdentityDetailCell.PaddingLeft = 0f;
                        empPersonalIdentityDetailCell.FixedHeight = 30f;
                        empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                        empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                        pdfDocument.Add(empPersonalIdentityDetailLabel);

                        PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[7].Columns.Count);
                        AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        AssignmentDetailTable.TotalWidth = page.Width;
                        AssignmentDetailTable.WidthPercentage = 98;
                        AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        foreach (DataColumn dc in ds.Tables[7].Columns)
                        {
                            Paragraph p = new Paragraph(dc.ToString(), TableContent);
                            p.Alignment = Element.ALIGN_CENTER;

                            PdfPCell tableColumnCell = new PdfPCell();
                            tableColumnCell.AddElement(p);
                            tableColumnCell.MinimumHeight = 23f;
                            tableColumnCell.PaddingBottom = 10f;
                            tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                            AssignmentDetailTable.AddCell(tableColumnCell);
                        }

                        columnCount = 1;
                        foreach (DataRow DR in ds.Tables[7].Rows)
                        {
                            for (int i = 0; i < ds.Tables[7].Columns.Count; i++)
                            {
                                PdfPCell tableCell;
                                tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                                tableCell.SetLeading(0.0f, 1.5f);
                                tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.MinimumHeight = 23;
                                tableCell.PaddingBottom = 5f;
                                tableCell.PaddingTop = 1f;
                                AssignmentDetailTable.AddCell(tableCell);

                            }
                        }

                        pdfDocument.Add(AssignmentDetailTable);
                    }
                    #endregion
                }

                #region Previous Employment History
                if (ds.Tables.Count >= 9)
                {
                    if (ds.Tables[8].Rows.Count == 0)
                    {
                        DataRow toInsert = ds.Tables[8].NewRow();
                        ds.Tables[8].Rows.InsertAt(toInsert, 0);
                    }

                    PdfPTable empPersonalIdentityDetailLabel = new PdfPTable(1);
                    empPersonalIdentityDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    empPersonalIdentityDetailLabel.TotalWidth = page.Width;
                    empPersonalIdentityDetailLabel.WidthPercentage = 95;
                    empPersonalIdentityDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    PdfPCell empPersonalIdentityDetailCell = new PdfPCell(new Phrase("Previous Employment History", MHeading));
                    empPersonalIdentityDetailCell.PaddingLeft = 0f;
                    empPersonalIdentityDetailCell.FixedHeight = 30f;
                    empPersonalIdentityDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    empPersonalIdentityDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    empPersonalIdentityDetailCell.Border = PdfPCell.NO_BORDER;
                    empPersonalIdentityDetailLabel.AddCell(empPersonalIdentityDetailCell);
                    pdfDocument.Add(empPersonalIdentityDetailLabel);

                    PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[8].Columns.Count);
                    AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    AssignmentDetailTable.TotalWidth = page.Width;
                    AssignmentDetailTable.WidthPercentage = 98;
                    AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    foreach (DataColumn dc in ds.Tables[8].Columns)
                    {
                        Paragraph p = new Paragraph(dc.ToString(), TableContent);
                        p.Alignment = Element.ALIGN_CENTER;

                        PdfPCell tableColumnCell = new PdfPCell();
                        tableColumnCell.AddElement(p);
                        tableColumnCell.MinimumHeight = 23;
                        tableColumnCell.PaddingBottom = 10f;
                        tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                        AssignmentDetailTable.AddCell(tableColumnCell);
                    }

                    columnCount = 1;
                    foreach (DataRow DR in ds.Tables[8].Rows)
                    {
                        for (int i = 0; i < ds.Tables[8].Columns.Count; i++)
                        {
                            PdfPCell tableCell;
                            tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                            tableCell.SetLeading(0.0f, 1.5f);
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.MinimumHeight = 23f;
                            tableCell.PaddingBottom = 5f;
                            tableCell.PaddingTop = 1f;
                            AssignmentDetailTable.AddCell(tableCell);

                        }
                    }
                    pdfDocument.Add(AssignmentDetailTable);
                }
                #endregion

                PdfPTable docVerficationTable = new PdfPTable(1);
                docVerficationTable.KeepTogether = true;
                docVerficationTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                for (int i = 0; i < 1; i++)
                {
                    docVerficationTable.AddCell(Blankcell);
                }

                PdfPCell pHrVerification = new PdfPCell(new Phrase("-------------------------", TableContent));
                pHrVerification.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                pHrVerification.Border = PdfPCell.NO_BORDER;
                docVerficationTable.AddCell(pHrVerification);

                pHrVerification = new PdfPCell(new Phrase("Human Resources Manager", TableContent));
                pHrVerification.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                pHrVerification.Border = PdfPCell.NO_BORDER;
                docVerficationTable.AddCell(pHrVerification);
                pdfDocument.Add(docVerficationTable);
                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static Document GenrateGraphicalReportPDF(HttpResponseBase resp, string ImagePath)
        {
            Document pdfDocument;
            pdfDocument = new Document(PageSize.A4, 10f, 10f, 30f, 20f);  //Landscape

            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {


                pdfDocument.Open();

                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font MHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(68, 114, 196)));
                iTextSharp.text.Font SubHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));
                iTextSharp.text.Font MHeadingWithColor = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(173, 216, 230)));

                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font HeadingAr = new iTextSharp.text.Font(basefont, 16, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(68, 114, 196));
                iTextSharp.text.Font SubHeadingAr = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));

                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();

                PdfPTable head = new PdfPTable(3);
                head.TotalWidth = 540;
                head.PaddingTop = 60f;
                head.LockedWidth = true;
                float[] widths = new float[] { 190f, 160f, 190f };  //landscape
                head.SetWidths(widths);

                PdfPCell p1 = new PdfPCell(new Phrase(sc.SchoolName_1, Heading));

                PdfPTable SchoolInfoTable = new PdfPTable(1);
                SchoolInfoTable.TotalWidth = 170f;
                SchoolInfoTable.PaddingTop = 10f;
                SchoolInfoTable.LockedWidth = true;
                float[] widths1 = new float[] { 160f };  //landscape
                SchoolInfoTable.SetWidths(widths1);


                PdfPCell schoolName = new PdfPCell(new Phrase(sc.SchoolName_1, Heading));
                schoolName.Border = PdfPCell.NO_BORDER;
                schoolName.HorizontalAlignment = Element.ALIGN_LEFT;
                SchoolInfoTable.AddCell(schoolName);

                PdfPCell campusName = new PdfPCell(new Phrase(sc.CampusName_1, SubHeading));
                campusName.Border = PdfPCell.NO_BORDER;
                campusName.HorizontalAlignment = Element.ALIGN_LEFT;
                SchoolInfoTable.AddCell(campusName);

                PdfPCell PoBox = new PdfPCell(new Phrase("P.O.Box:" + sc.POBox + " " + sc.City + " " + sc.Country, SubHeading));
                PoBox.Border = PdfPCell.NO_BORDER;
                PoBox.HorizontalAlignment = Element.ALIGN_LEFT;
                SchoolInfoTable.AddCell(PoBox);

                PdfPCell Phone = new PdfPCell(new Phrase("Tel:" + sc.Phone + "/Fax:" + sc.Fax, SubHeading));
                Phone.Border = PdfPCell.NO_BORDER;
                Phone.HorizontalAlignment = Element.ALIGN_LEFT;
                SchoolInfoTable.AddCell(Phone);
                SchoolInfoTable.HorizontalAlignment = Element.ALIGN_LEFT;
                SchoolInfoTable.DefaultCell.Border = Rectangle.NO_BORDER;
                PdfPCell EnglishHeading = new PdfPCell();
                EnglishHeading.Border = PdfPCell.NO_BORDER;
                EnglishHeading.AddElement(SchoolInfoTable);
                head.AddCell(EnglishHeading);

                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.ReportLogo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = pdfDocument.TopMargin;

                PdfPCell c0 = new PdfPCell(jpg, true);
                c0.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                c0.Border = PdfPCell.NO_BORDER;
                c0.PaddingTop = 10f;
                c0.FixedHeight = 100f;
                head.AddCell(c0);

                //PdfPCell p2 = new PdfPCell(new Phrase(sc.SchoolName_1, Heading));
                //head.AddCell(p2);

                PdfPTable SchoolInfoTableAr = new PdfPTable(1);
                SchoolInfoTableAr.TotalWidth = 170f;
                SchoolInfoTableAr.PaddingTop = 10f;
                SchoolInfoTableAr.LockedWidth = true;
                SchoolInfoTableAr.SetWidths(widths1);

                PdfPCell schoolNameAr = new PdfPCell(new Phrase(sc.SchoolName_2, HeadingAr));
                schoolNameAr.Border = PdfPCell.NO_BORDER;
                schoolNameAr.HorizontalAlignment = Element.ALIGN_LEFT;
                schoolNameAr.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                SchoolInfoTableAr.AddCell(schoolNameAr);

                PdfPCell campusNameAr = new PdfPCell(new Phrase(sc.CampusName_2, SubHeadingAr));
                campusNameAr.Border = PdfPCell.NO_BORDER;
                campusNameAr.HorizontalAlignment = Element.ALIGN_LEFT;
                campusNameAr.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                SchoolInfoTableAr.AddCell(campusNameAr);

                PdfPCell PoBoxAr = new PdfPCell(new Phrase("ص.ب.:" + sc.POBox + " " + sc.City_2 + " " + sc.Country_2, SubHeadingAr));
                PoBoxAr.Border = PdfPCell.NO_BORDER;
                PoBoxAr.HorizontalAlignment = Element.ALIGN_LEFT;
                PoBoxAr.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                SchoolInfoTableAr.AddCell(PoBoxAr);

                PdfPCell PhoneAr = new PdfPCell(new Phrase("Tel:" + sc.Phone + "/Fax:" + sc.Fax, SubHeadingAr));
                PhoneAr.Border = PdfPCell.NO_BORDER;
                PhoneAr.HorizontalAlignment = Element.ALIGN_LEFT;
                PhoneAr.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                SchoolInfoTableAr.AddCell(PhoneAr);

                SchoolInfoTableAr.HorizontalAlignment = Element.ALIGN_RIGHT;
                SchoolInfoTableAr.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell EnglishHeadingAr = new PdfPCell();
                EnglishHeadingAr.Border = PdfPCell.NO_BORDER;
                EnglishHeadingAr.AddElement(SchoolInfoTableAr);

                head.AddCell(EnglishHeadingAr);

                //PdfPCell p3 = new PdfPCell(new Phrase(sc.SchoolName_1, Heading));
                //  head.AddCell(p3);
                pdfDocument.Add(head);

                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                //
                pdfDocument.SetMargins(30f, 20f, 20f, 30f);

                PdfPTable BlankTable = new PdfPTable(1);
                BlankTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                BlankTable.TotalWidth = 540f;
                BlankTable.WidthPercentage = 95;

                PdfPCell Blankcell = new PdfPCell(new Phrase("", MHeading));
                Blankcell.FixedHeight = 20f;
                Blankcell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Blankcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                Blankcell.Border = PdfPCell.NO_BORDER;
                BlankTable.AddCell(Blankcell);
                pdfDocument.Add(BlankTable);

                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(ImagePath);
                gif.Alignment = PdfPCell.ALIGN_CENTER;
                gif.ScalePercent(60f);

                pdfDocument.Add(gif);

                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static Document GenratePayrollPDF(HttpResponseBase resp, DataSet ds, bool isLandscape)
        {
            DataTable dtNewTable = AddHeaderRows(ds.Tables[0]);
            Document pdfDocument;
            bool isHeader = false;
            if (isLandscape)
            {
                pdfDocument = new Document(PageSize.A4.Rotate(), 0f, 0f, 120f, 20f);  //Landscape
            }
            else
            {
                pdfDocument = new Document(PageSize.A4, 0f, 0f, 120f, 20f);   //Postrait
            }

            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {
                SchoolInfo.cellHeight = pdfDocument.TopMargin;
                SchoolInfo.page = pdfDocument.PageSize;
                SchoolInfo.isHeaderRepeated = false;
                SchoolInfo.pageCount = 0;
                string MainHeader = "";
                int count = 0;
                wri.PageEvent = new itextEvents();
                pdfDocument.Open();
                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font tableHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font Heading1 = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));

                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;
                PdfPTable masterTbl = new PdfPTable(1);
                #region header
                // create four column table
                PdfPTable head = GetPdfHeader(dtNewTable, page.Width, isLandscape, jpg, Heading, Heading1, Schoolname, 120);

                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );
                PdfContentByte cb = wri.DirectContent;
                cb.SetLineWidth(1.5f);
                cb.SetColorStroke(new CMYKColor(0, 0, 0, 100f));
                cb.MoveTo(20, 500);
                cb.LineTo(530, 500);
                cb.Stroke();
                cb.SetLineWidth(1.5f);
                cb.MoveTo(750, 500);
                cb.LineTo(822, 500);
                cb.Stroke();
                PdfPCell WPSReportName = new PdfPCell(new Phrase(CommonHelper.tempMessage, Heading));
                WPSReportName.Border = PdfPCell.NO_BORDER;
                WPSReportName.HorizontalAlignment = Element.ALIGN_CENTER;
                WPSReportName.VerticalAlignment = Element.ALIGN_MIDDLE;
                PdfPTable table = new PdfPTable(1);
                table.TotalWidth = 220f;
                table.AddCell(WPSReportName);
                table.WriteSelectedRows(0, -1, 520, 510, cb);
                #endregion

                #region PageContent

                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));


                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                //
                pdfDocument.SetMargins(0f, 0f, 20f, 20f);
                PdfPTable content = new PdfPTable(dtNewTable.Columns.Count - 2);
                content.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                content.TotalWidth = page.Width;
                content.WidthPercentage = 95;
                foreach (DataRow DR in dtNewTable.Rows)
                {
                    if (DR["active"].ToString().ToLower() == "active")
                        isHeader = true;
                    else
                        isHeader = false;

                    if (DR["active"].ToString() != "7")
                    {
                        for (int i = 0; i < dtNewTable.Columns.Count - 2; i++)
                        {
                            PdfPCell tableCell;
                            if (dtNewTable.Columns[i].ToString().ToLower().Contains("(ar)"))
                                tableCell = new PdfPCell(new Phrase(DR[i].ToString(), arabicFont));
                            else
                            {
                                tableCell = new PdfPCell(new Phrase(DR[i].ToString(), isHeader ? tableHeading : TableContent));
                            }
                            if (DR["active"].ToString().Contains("14"))
                            {

                                int GrossPos = dtNewTable.Columns["Gross"].Ordinal;
                                int NetPayblePos = dtNewTable.Columns["Net Payble"].Ordinal;

                                if (i > 4 && i <= GrossPos)
                                {
                                    if (i == GrossPos)
                                    {
                                        tableCell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                                    }
                                    else
                                    {
                                        tableCell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                    }

                                }
                                if (i > GrossPos && i < NetPayblePos)
                                {
                                    tableCell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                }
                            }
                            tableCell.SetLeading(0.0f, 1f);
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.PaddingBottom = 5f;
                            tableCell.PaddingTop = 4f;
                            tableCell.BorderWidth = 1f;
                            content.AddCell(tableCell);
                        }
                    }

                }
                pdfDocument.Add(content);
                #endregion
                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static Document GenratePayrollPDFWithRepeatedHeader(HttpResponseBase resp, DataSet ds, bool isLandscape)
        {
            SchoolInfo.schoolInfo = new SchoolInformationDB().GetBasicSchoolInformation();
            SchoolInfo.ds = ds;
            SchoolInfo.pageCount = 0;
            SchoolInfo.isHeaderRepeated = true;
            DataTable dtNewTable = AddHeaderRows(ds.Tables[0]);

            bool isHeader = false;
            Document pdfDocument;
            if (isLandscape)
            {
                pdfDocument = new Document(PageSize.A4.Rotate(), 0f, 0f, 100f, 20f);  //Landscape
            }
            else
            {
                pdfDocument = new Document(PageSize.A4, 0f, 0f, 100f, 20f);   //Postrait
            }
            iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

            //SchoolInfo.difference = headerHeight - 9f;
            pdfDocument.SetMargins(0f, 0f, 150f, 20f);
            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {
                SchoolInfo.cellHeight = pdfDocument.TopMargin;
                SchoolInfo.page = pdfDocument.PageSize;
                wri.PageEvent = new itextEvents();
                pdfDocument.Open();

                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font tableHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));


                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 20, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));

                #region PageContent

                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));


                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                //

                PdfPTable content = new PdfPTable(dtNewTable.Columns.Count - 2);
                content.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                //content.PaddingTop = 10f;
                content.TotalWidth = SchoolInfo.page.Width;
                content.WidthPercentage = 95;

                foreach (DataRow DR in dtNewTable.Rows)
                {
                    if (DR["active"].ToString().ToLower() == "active")
                        isHeader = true;
                    else
                        isHeader = false;
                    if (!isHeader)
                    {
                        if (DR["active"].ToString() != "7")
                        {

                            for (int i = 0; i < dtNewTable.Columns.Count - 2; i++)
                            {
                                PdfPCell tableCell;
                                if (dtNewTable.Columns[i].ToString().ToLower().Contains("(ar)"))
                                    tableCell = new PdfPCell(new Phrase(DR[i].ToString(), arabicFont));
                                else
                                    tableCell = new PdfPCell(new Phrase(DR[i].ToString(), isHeader ? tableHeading : TableContent));
                                if (DR["active"].ToString().Contains("14"))
                                {
                                    int GrossPos = dtNewTable.Columns["Gross"].Ordinal;
                                    int NetPayblePos = dtNewTable.Columns["Net Payble"].Ordinal;

                                    if (i > 4 && i <= GrossPos)
                                    {
                                        if (i == GrossPos)
                                        {
                                            tableCell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                                        }
                                        else
                                        {
                                            tableCell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                        }

                                    }
                                    if (i > GrossPos && i < NetPayblePos)
                                    {
                                        tableCell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                                    }
                                }
                                tableCell.SetLeading(0.0f, 1f);
                                tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                tableCell.PaddingBottom = 5f;
                                tableCell.PaddingTop = 4f;
                                content.AddCell(tableCell);

                            }
                        }
                    }
                }
                pdfDocument.Add(content);
                #endregion
                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static DataTable AddHeaderRows(DataTable dtOldData)
        {
            DataTable dtNewTable = dtOldData.Clone();

            for (int i = 0; i < dtOldData.Columns.Count; i++)
            {
                dtNewTable.Columns[i].DataType = typeof(string);
                dtNewTable.Columns[i].MaxLength = 100;

            }


            foreach (DataRow row in dtOldData.Rows)
            {
                dtNewTable.ImportRow(row);
            }
            try
            {
                int rowCount = dtOldData.Rows.Count;
                for (int r = 0; r < rowCount; r++)
                {
                    if (dtNewTable.Rows[r]["active"].ToString() == "2" && dtNewTable.Rows[r - 1]["active"].ToString() != "2")
                    {
                        DataRow headerRow = dtNewTable.NewRow();
                        for (int c = 0; c < dtOldData.Columns.Count; c++)
                        {
                            if (dtOldData.Columns[c].ColumnName.ToLower() != "header")
                                headerRow[c] = dtOldData.Columns[c].ColumnName;
                            else
                                headerRow[c] = "";
                        }
                        dtNewTable.Rows.InsertAt(headerRow, r);
                        r++;
                        rowCount++;
                    }
                }

            }
            catch (Exception ex)
            {
                //throw ex;
            }
            return dtNewTable;
        }

        public static Document GenrateDepartmentPDF(HttpResponseBase resp, DataSet ds, bool isLandscape, int CompId)
        {
            Document pdfDocument;
            ds.Tables[0].Columns.Add("Department Employee List");
            if (isLandscape)
            {
                pdfDocument = new Document(PageSize.A4.Rotate(), 0f, 0f, 170f, 20f);  //Landscape
            }
            else
            {
                pdfDocument = new Document(PageSize.A4, 0f, 0f, 150f, 20f);   //Postrait
            }


            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {
                pdfDocument.Open();

                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font MHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 20, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));


                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;

                // create two column table
                PdfPTable head = new PdfPTable(4);
                head.TotalWidth = page.Width;
                head.LockedWidth = true;
                if (isLandscape)
                {
                    float[] widths = new float[] { 180f, 160f, 300f, 180f };  //landscape
                    head.SetWidths(widths);
                }
                else
                {
                    float[] widths = new float[] { 60f, 135f, 250f, 150f };
                    head.SetWidths(widths);
                }



                //Blanck Cell
                PdfPCell c0 = new PdfPCell();
                c0.Border = PdfPCell.NO_BORDER;
                head.AddCell(c0);


                // add image; PdfPCell() overload sizes image to fit cell
                PdfPCell c = new PdfPCell(jpg, true);
                c.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //c.HorizontalAlignment = Element.ALIGN_LEFT;
                c.FixedHeight = cellHeight;

                c.Border = PdfPCell.NO_BORDER;
                c.PaddingLeft = 10f;
                c.PaddingTop = 10f;
                c.PaddingBottom = 20f;
                head.AddCell(c);
                // add the header text

                PdfPCell c1 = new PdfPCell(new Phrase(Schoolname, Heading)); //Landscape
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape

                c1.PaddingTop = 50F;
                c1.FixedHeight = cellHeight;
                head.AddCell(c1);

                PdfPCell c2 = new PdfPCell(new Phrase("Exported Date: " + DateTime.Now.ToString("dd MMM yyyy"), DateFont));
                c2.PaddingTop = 10f;
                c2.PaddingRight = 20f;
                c2.HorizontalAlignment = Element.ALIGN_RIGHT;
                c2.Border = PdfPCell.NO_BORDER;
                head.AddCell(c2);


                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );
                pdfDocument.SetMargins(0f, 0f, 20f, 20f);


                List<int> deptid = new List<int>();
                foreach (DataRow DR in ds.Tables[0].Rows)
                {
                    deptid.Add(Convert.ToInt32(DR[0].ToString()));
                }

                ds.Tables[0].Columns.RemoveAt(0);
                PdfPTable content = new PdfPTable(ds.Tables[0].Columns.Count);
                content.TotalWidth = page.Width;
                int[] columnWidths = { 55, 65, 65, 65, 65, 55, 65, 55, 55, 55, 50, 55, 135 };
                content.WidthPercentage = 95;
                content.SetWidths(columnWidths);
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    Paragraph p = new Paragraph(dc.ToString(), MHeading);
                    p.Alignment = Element.ALIGN_CENTER;

                    PdfPCell tableColumnCell = new PdfPCell();
                    //tableColumnCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    //tableColumnCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    tableColumnCell.AddElement(p);
                    tableColumnCell.PaddingBottom = 10f;
                    // content.AddCell(new Phrase(dc.ToString(), MHeading));
                    content.AddCell(tableColumnCell);
                }

                int rowsCount = 0;
                foreach (DataRow DR in ds.Tables[0].Rows)
                {

                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        PdfPCell tableCell = new PdfPCell();
                        if (i == ds.Tables[0].Columns.Count - 1)
                        {
                            int deptID = deptid[rowsCount];
                            rowsCount++;
                            DepartmentEmployeeDB objDepEmpDB = new DepartmentEmployeeDB();
                            List<EmployeeDetails> lstEmp = new List<EmployeeDetails>();
                            lstEmp = objDepEmpDB.GetEmployeeByDepartmentId(deptID, CompId);
                            Paragraph EmpP = new Paragraph();
                            foreach (var emp in lstEmp)
                            {
                                EmpP.Add(new Phrase(emp.FirstName_1 + " " + emp.SurName_1 + "\n", TableContent));
                            }
                            EmpP.Alignment = Element.ALIGN_CENTER;
                            tableCell.AddElement(EmpP);
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.PaddingBottom = 10f;
                        }
                        else
                        {
                            Paragraph p = new Paragraph(DR[i].ToString(), TableContent);
                            p.Alignment = Element.ALIGN_CENTER;
                            p.PaddingTop = 1;
                            tableCell.AddElement(p);
                            tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            tableCell.PaddingBottom = 10f;
                            // content.AddCell(new Phrase(DR[i].ToString(), TableContent));


                        }
                        content.AddCell(tableCell);

                    }
                }
                pdfDocument.Add(content);
                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static Document GenratepaySlipPDF(FileStream resp, Dictionary<string, string> keyValues, DataSet dsRemunerationTable, DataSet dsAdditionTable, DataSet dsDeductionTable, int templateID)
        {
            Document pdfDocument;
            pdfDocument = new Document(PageSize.A4, 0f, 0f, 240f, 20f);   //Postrait
            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp))
            {
                pdfDocument.Open();
                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);

                iTextSharp.text.Font ReportDataFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));


                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font OtherHeadingContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font PaySlipName = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 13, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font PaySlipInfo = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font OtherHeadingContentBold = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 13, iTextSharp.text.Font.BOLD, Black));

                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = 220; //pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;

                #region Header part
                // create two column table
                PdfPTable head = new PdfPTable(2);
                head.TotalWidth = page.Width;
                head.LockedWidth = true;

                float[] widths = new float[] { 140f, 400f };
                head.SetWidths(widths);


                // add image; PdfPCell() overload sizes image to fit cell
                PdfPCell c = new PdfPCell(jpg, true);
                c.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //c.HorizontalAlignment = Element.ALIGN_LEFT;
                c.FixedHeight = cellHeight;

                c.Border = PdfPCell.NO_BORDER;
                c.PaddingLeft = 50f;
                c.PaddingTop = 40f;
                c.PaddingBottom = 20f;
                head.AddCell(c);
                // add the header text

                PdfPTable Title = new PdfPTable(1);
                float[] Titlewidths = new float[] { 445f };
                Title.SetWidths(Titlewidths);


                PdfPCell SchoolName = new PdfPCell(new Phrase(Schoolname, Heading));
                SchoolName.Border = PdfPCell.NO_BORDER;
                SchoolName.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Blankcell = new PdfPCell(new Phrase("", Heading));
                Blankcell.Border = PdfPCell.NO_BORDER;
                Blankcell.HorizontalAlignment = Element.ALIGN_CENTER;
                Blankcell.FixedHeight = 10f;

                PdfPCell Address = new PdfPCell(new Phrase(keyValues["address1"] + " " + keyValues["city"] + " " + keyValues["country"] + " P.O.Box: " + keyValues["pobox"], OtherHeadingContent));
                Address.Border = PdfPCell.NO_BORDER;
                Address.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Phone = new PdfPCell(new Phrase("Phone: " + keyValues["phoneno"], OtherHeadingContent));
                Phone.Border = PdfPCell.NO_BORDER;
                Phone.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Fax = new PdfPCell(new Phrase("Fax: " + keyValues["faxno"], OtherHeadingContent));
                Fax.Border = PdfPCell.NO_BORDER;
                Fax.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Email = new PdfPCell(new Phrase("Email ID: " + keyValues["emailid"], OtherHeadingContent));
                Email.Border = PdfPCell.NO_BORDER;
                Email.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell WebSite = new PdfPCell(new Phrase("Website: " + keyValues["website"], OtherHeadingContent));
                WebSite.Border = PdfPCell.NO_BORDER;
                WebSite.HorizontalAlignment = Element.ALIGN_CENTER;

                PdfPCell blanckCell = new PdfPCell(new Phrase("", OtherHeadingContent));
                blanckCell.Border = PdfPCell.NO_BORDER;
                blanckCell.HorizontalAlignment = Element.ALIGN_CENTER;
                blanckCell.FixedHeight = 10f;

                PdfPCell paySlipCycle = new PdfPCell(new Phrase(keyValues["monthname"] + " " + keyValues["YearName"] + " PAY SLIP", PaySlipName));
                paySlipCycle.Border = PdfPCell.NO_BORDER;
                paySlipCycle.HorizontalAlignment = Element.ALIGN_CENTER;


                Title.AddCell(SchoolName);
                Title.AddCell(Blankcell);
                Title.AddCell(Address);
                Title.AddCell(Phone);
                Title.AddCell(Fax);
                Title.AddCell(Email);
                Title.AddCell(WebSite);
                Title.AddCell(blanckCell);
                Title.AddCell(paySlipCycle);
                PdfPCell c1 = new PdfPCell();
                c1.AddElement(Title);
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape
                c1.PaddingTop = 40F;
                c1.PaddingLeft = 0f;
                c1.PaddingRight = 100f;
                c1.FixedHeight = cellHeight;
                head.AddCell(c1);



                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );

                // pdfDocument.Add(head);
                #endregion

                #region 2nd Part
                PdfPTable PaymentInfoTable = new PdfPTable(1);
                float[] paymentinfoWidths = new float[] { 460f };
                PaymentInfoTable.TotalWidth = page.Width - 30;
                PaymentInfoTable.LockedWidth = true;
                PaymentInfoTable.SetWidths(paymentinfoWidths);

                PdfPCell dateofPayment = new PdfPCell(new Phrase("Date of Payment: " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(keyValues["paymentdate"]), PaySlipInfo));
                dateofPayment.Border = PdfPCell.NO_BORDER;
                dateofPayment.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                dateofPayment.PaddingTop = 0f;
                dateofPayment.PaddingRight = 30f;
                dateofPayment.PaddingLeft = 60f;
                dateofPayment.FixedHeight = 15f;

                PdfPCell payperiod = new PdfPCell(new Phrase("Pay Period: " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(keyValues["payfrom"]) + " to " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(keyValues["payto"]), PaySlipInfo));
                payperiod.Border = PdfPCell.NO_BORDER;
                payperiod.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                payperiod.PaddingTop = 0f;
                payperiod.PaddingRight = 30f;
                payperiod.PaddingLeft = 60f;
                payperiod.FixedHeight = 15f;

                PaymentInfoTable.AddCell(dateofPayment);
                PaymentInfoTable.AddCell(payperiod);
                PaymentInfoTable.WriteSelectedRows(
                0, -1,  // first/last row; -1 flags all write all rows
                0,      // left offset
                        // ** bottom** yPos of the table
               page.Height - cellHeight + PaymentInfoTable.TotalHeight,
               wri.DirectContent
             );
                // pdfDocument.Add(PaymentInfoTable);
                #endregion
                #region PageContent

                #region PersonalInfo
                pdfDocument.SetMargins(0f, 0f, 20f, 20f);
                PdfPTable PersonalInfoTable = new PdfPTable(4);
                PersonalInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                //PersonalInfoTable.SpacingBefore = 20f;
                PdfPCell personalInfoLabel = new PdfPCell(new Phrase("Personal Information", OtherHeadingContentBold));
                personalInfoLabel.Colspan = 4;
                personalInfoLabel.Border = PdfPCell.NO_BORDER;
                personalInfoLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                personalInfoLabel.PaddingTop = 0f;
                personalInfoLabel.PaddingLeft = 0f;
                personalInfoLabel.FixedHeight = 20f;
                PersonalInfoTable.AddCell(personalInfoLabel);

                //PdfPCell personalInfoBlankLabel = new PdfPCell(new Phrase("", OtherHeadingContentBold));
                //personalInfoBlankLabel.Colspan = 4;
                //personalInfoBlankLabel.Border = PdfPCell.NO_BORDER;
                //personalInfoBlankLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                //personalInfoBlankLabel.PaddingTop = 0f;
                //personalInfoBlankLabel.PaddingLeft = 0f;
                //personalInfoBlankLabel.FixedHeight = 7f;
                //PersonalInfoTable.AddCell(personalInfoBlankLabel);

                PdfPCell EmployeeIDLabel = new PdfPCell(new Phrase(" Employee ID", ReportDataFont));
                EmployeeIDLabel.BackgroundColor = new BaseColor(211, 211, 211);
                EmployeeIDLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeIDLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeIDLabel);

                PdfPCell EmployeeID = new PdfPCell(new Phrase(" " + keyValues["employeeid"], ReportDataFont));
                EmployeeID.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeID.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeID);

                PdfPCell EmployeeNameLabel = new PdfPCell(new Phrase(" Employee Name", ReportDataFont));
                EmployeeNameLabel.BackgroundColor = new BaseColor(211, 211, 211);
                EmployeeNameLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeNameLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeNameLabel);

                PdfPCell EmployeeName = new PdfPCell(new Phrase(" " + keyValues["employeename"], ReportDataFont));
                EmployeeName.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeName.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeName);

                PdfPCell DepartmentLabel = new PdfPCell(new Phrase(" Department", ReportDataFont));
                DepartmentLabel.BackgroundColor = new BaseColor(211, 211, 211);
                DepartmentLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DepartmentLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(DepartmentLabel);

                PdfPCell Department = new PdfPCell(new Phrase(" " + keyValues["department"], ReportDataFont));
                Department.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Department.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Department);

                PdfPCell PositionLabel = new PdfPCell(new Phrase(" Position", ReportDataFont));
                PositionLabel.BackgroundColor = new BaseColor(211, 211, 211);
                PositionLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                PositionLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(PositionLabel);

                PdfPCell Position = new PdfPCell(new Phrase(" " + keyValues["empposition"], ReportDataFont));
                Position.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Position.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Position);

                PdfPCell BankLabel = new PdfPCell(new Phrase(" Bank Name", ReportDataFont));
                BankLabel.BackgroundColor = new BaseColor(211, 211, 211);
                BankLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                BankLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(BankLabel);

                PdfPCell Bank = new PdfPCell(new Phrase(" " + keyValues["bankname"], ReportDataFont));
                Bank.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Bank.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Bank);

                PdfPCell AccountNoLabel = new PdfPCell(new Phrase(" Account Number", ReportDataFont));
                AccountNoLabel.BackgroundColor = new BaseColor(211, 211, 211);
                AccountNoLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AccountNoLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(AccountNoLabel);

                PdfPCell AccountNo = new PdfPCell(new Phrase(" " + keyValues["accountnumber"], ReportDataFont));
                AccountNo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AccountNo.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(AccountNo);

                PdfPCell ServiceSinceLabel = new PdfPCell(new Phrase(" In service since", ReportDataFont));
                ServiceSinceLabel.BackgroundColor = new BaseColor(211, 211, 211);
                ServiceSinceLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                ServiceSinceLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(ServiceSinceLabel);

                PdfPCell ServiceSince = new PdfPCell(new Phrase(" " + keyValues["inservicesince"], ReportDataFont));
                ServiceSince.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                ServiceSince.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(ServiceSince);

                PdfPCell RemainingofLabel = new PdfPCell(new Phrase(" Remaining off days", ReportDataFont));
                RemainingofLabel.BackgroundColor = new BaseColor(211, 211, 211);
                RemainingofLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemainingofLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(RemainingofLabel);

                PdfPCell Remainingof = new PdfPCell(new Phrase(" " + keyValues["remainingoffdays"], ReportDataFont));
                Remainingof.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Remainingof.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Remainingof);


                pdfDocument.Add(PersonalInfoTable);
                #endregion

                #region SalaryInfo
                PdfPTable salaryInfoTable = new PdfPTable(3);
                salaryInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;


                PdfPCell blanckLabel = new PdfPCell(new Phrase("", OtherHeadingContentBold));
                blanckLabel.Colspan = 3;
                blanckLabel.Border = PdfPCell.NO_BORDER;
                blanckLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                blanckLabel.PaddingTop = 0f;
                blanckLabel.FixedHeight = 20f;
                salaryInfoTable.AddCell(blanckLabel);

                PdfPCell salaryInfoLabel = new PdfPCell(new Phrase("Salary Information", OtherHeadingContentBold));
                salaryInfoLabel.Colspan = 3;
                salaryInfoLabel.Border = PdfPCell.NO_BORDER;
                salaryInfoLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                salaryInfoLabel.PaddingTop = 0f;
                salaryInfoLabel.PaddingLeft = 0f;
                salaryInfoLabel.FixedHeight = 20f;
                salaryInfoTable.AddCell(salaryInfoLabel);

                //PdfPCell salaryInfoBlankLabel = new PdfPCell(new Phrase("", OtherHeadingContentBold));
                //salaryInfoBlankLabel.Colspan = 3;
                //salaryInfoBlankLabel.Border = PdfPCell.NO_BORDER;
                //salaryInfoBlankLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                //salaryInfoBlankLabel.PaddingTop = 0f;
                //salaryInfoBlankLabel.FixedHeight = 7f;
                //salaryInfoTable.AddCell(salaryInfoBlankLabel);

                PdfPCell RemunerationLabel = new PdfPCell(new Phrase("Remuneration", ReportDataFont));
                RemunerationLabel.BackgroundColor = new BaseColor(211, 211, 211);
                RemunerationLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemunerationLabel.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                RemunerationLabel.PaddingBottom = 5f;
                salaryInfoTable.AddCell(RemunerationLabel);

                PdfPCell AdditionLabel = new PdfPCell(new Phrase("Addition", ReportDataFont));
                AdditionLabel.BackgroundColor = new BaseColor(211, 211, 211);
                AdditionLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AdditionLabel.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                AdditionLabel.PaddingBottom = 5f;
                salaryInfoTable.AddCell(AdditionLabel);

                PdfPCell DeductionLabelLabel = new PdfPCell(new Phrase("Deduction", ReportDataFont));
                DeductionLabelLabel.BackgroundColor = new BaseColor(211, 211, 211);
                DeductionLabelLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DeductionLabelLabel.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                DeductionLabelLabel.PaddingBottom = 5f;
                salaryInfoTable.AddCell(DeductionLabelLabel);

                PdfPTable RemunerationTabel = new PdfPTable(2);
                if (dsRemunerationTable.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsRemunerationTable.Tables[0].Rows)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell(new Phrase(" " + dr["itemdescr"].ToString(), ReportDataFont));
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase(" " + dr["amount"].ToString(), ReportDataFont));
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        RemunerationTabel.AddCell(descrCell);
                        RemunerationTabel.AddCell(amountCell);
                    }
                    if (dsRemunerationTable.Tables[0].Rows.Count < 3)
                    {
                        int rowsCount = dsRemunerationTable.Tables[0].Rows.Count;

                        for (int i = 0; i <= 3 - rowsCount; i++)
                        {
                            PdfPCell descrCell;
                            descrCell = new PdfPCell();
                            descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            descrCell.PaddingBottom = 5f;
                            descrCell.FixedHeight = 15f;
                            PdfPCell amountCell;
                            amountCell = new PdfPCell(new Phrase());
                            amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            amountCell.PaddingBottom = 5f;
                            amountCell.FixedHeight = 15f;
                            RemunerationTabel.AddCell(descrCell);
                            RemunerationTabel.AddCell(amountCell);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell();
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        descrCell.FixedHeight = 15f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase());
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        amountCell.FixedHeight = 15f;
                        RemunerationTabel.AddCell(descrCell);
                        RemunerationTabel.AddCell(amountCell);
                    }
                }


                PdfPCell RemunerationDetails = new PdfPCell();
                //RemunerationDetails.Colspan = 3;
                RemunerationDetails.AddElement(RemunerationTabel);
                RemunerationDetails.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemunerationDetails.PaddingBottom = 5f;
                salaryInfoTable.AddCell(RemunerationDetails);


                PdfPTable AdditionTable = new PdfPTable(2);
                if (dsAdditionTable.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsAdditionTable.Tables[0].Rows)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell(new Phrase(" " + dr["itemdescr"].ToString(), ReportDataFont));
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase(" " + dr["amount"].ToString(), ReportDataFont));
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        AdditionTable.AddCell(descrCell);
                        AdditionTable.AddCell(amountCell);
                    }
                    if (dsAdditionTable.Tables[0].Rows.Count < 3)
                    {
                        int rowsCount = dsAdditionTable.Tables[0].Rows.Count;

                        for (int i = 0; i <= 3 - rowsCount; i++)
                        {
                            PdfPCell descrCell;
                            descrCell = new PdfPCell();
                            descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            descrCell.PaddingBottom = 5f;
                            descrCell.FixedHeight = 15f;
                            PdfPCell amountCell;
                            amountCell = new PdfPCell(new Phrase());
                            amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            amountCell.PaddingBottom = 5f;
                            amountCell.FixedHeight = 15f;
                            AdditionTable.AddCell(descrCell);
                            AdditionTable.AddCell(amountCell);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell();
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        descrCell.FixedHeight = 15f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase());
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        amountCell.FixedHeight = 15f;
                        AdditionTable.AddCell(descrCell);
                        AdditionTable.AddCell(amountCell);
                    }
                }

                PdfPCell AdditionDetails = new PdfPCell();
                AdditionDetails.AddElement(AdditionTable);
                AdditionDetails.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AdditionDetails.PaddingBottom = 5f;
                salaryInfoTable.AddCell(AdditionDetails);

                PdfPTable DeductionTable = new PdfPTable(2);
                if (dsDeductionTable.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsDeductionTable.Tables[0].Rows)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell(new Phrase(" " + dr["itemdescr"].ToString(), ReportDataFont));
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase(" " + dr["amount"].ToString(), ReportDataFont));
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        DeductionTable.AddCell(descrCell);
                        DeductionTable.AddCell(amountCell);
                    }

                    if (dsDeductionTable.Tables[0].Rows.Count < 3)
                    {
                        int rowsCount = dsDeductionTable.Tables[0].Rows.Count;

                        for (int i = 0; i <= 3 - rowsCount; i++)
                        {
                            PdfPCell descrCell;
                            descrCell = new PdfPCell();
                            descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            descrCell.PaddingBottom = 5f;
                            descrCell.FixedHeight = 15f;
                            PdfPCell amountCell;
                            amountCell = new PdfPCell(new Phrase());
                            amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            amountCell.PaddingBottom = 5f;
                            amountCell.FixedHeight = 15f;
                            DeductionTable.AddCell(descrCell);
                            DeductionTable.AddCell(amountCell);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell();
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        descrCell.FixedHeight = 15f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase());
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        amountCell.FixedHeight = 15f;
                        DeductionTable.AddCell(descrCell);
                        DeductionTable.AddCell(amountCell);
                    }
                }

                PdfPCell DeductionDetails = new PdfPCell();
                DeductionDetails.AddElement(DeductionTable);
                DeductionDetails.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DeductionDetails.PaddingBottom = 5f;
                salaryInfoTable.AddCell(DeductionDetails);

                PdfPCell RemunerationTotal = new PdfPCell(new Phrase("Total: " + keyValues["remtotal"], ReportDataFont));
                RemunerationTotal.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemunerationTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                RemunerationTotal.PaddingBottom = 5f;
                salaryInfoTable.AddCell(RemunerationTotal);

                PdfPCell AdditionTotal = new PdfPCell(new Phrase("Total: " + keyValues["addtotal"], ReportDataFont));
                AdditionTotal.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AdditionTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                AdditionTotal.PaddingBottom = 5f;
                salaryInfoTable.AddCell(AdditionTotal);

                PdfPCell DeductionTotal = new PdfPCell(new Phrase("Total: " + keyValues["dedtotal"], ReportDataFont));
                DeductionTotal.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DeductionTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                DeductionTotal.PaddingBottom = 5f;
                salaryInfoTable.AddCell(DeductionTotal);

                PdfPCell CurrentNetSalary = new PdfPCell(new Phrase("Current Net Salary: " + keyValues["netsalary"], ReportDataFont));
                CurrentNetSalary.Colspan = 3;
                CurrentNetSalary.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                CurrentNetSalary.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                CurrentNetSalary.PaddingBottom = 5f;
                salaryInfoTable.AddCell(CurrentNetSalary);


                pdfDocument.Add(salaryInfoTable);


                #endregion

                if (templateID == 1)
                {
                    #region Attendance
                    PdfPTable AttendanceInfoTable = new PdfPTable(2);
                    AttendanceInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;

                    PdfPCell blanckLabel1 = new PdfPCell(new Phrase("", OtherHeadingContentBold));
                    blanckLabel1.Colspan = 2;
                    blanckLabel1.Border = PdfPCell.NO_BORDER;
                    blanckLabel1.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    blanckLabel1.PaddingTop = 0f;
                    blanckLabel1.FixedHeight = 20f;
                    AttendanceInfoTable.AddCell(blanckLabel1);

                    PdfPCell attendanceLabel = new PdfPCell(new Phrase("Attendance during the period", OtherHeadingContentBold));
                    attendanceLabel.Colspan = 2;
                    attendanceLabel.Border = PdfPCell.NO_BORDER;
                    attendanceLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    attendanceLabel.PaddingTop = 0f;
                    attendanceLabel.PaddingLeft = 0f;
                    attendanceLabel.FixedHeight = 20f;
                    AttendanceInfoTable.AddCell(attendanceLabel);

                    //PdfPCell attendanceBlankLabel = new PdfPCell(new Phrase(" ", OtherHeadingContentBold));
                    //attendanceBlankLabel.Colspan = 2;
                    //attendanceBlankLabel.Border = PdfPCell.NO_BORDER;
                    //attendanceBlankLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    //attendanceBlankLabel.PaddingTop = 0f;
                    //attendanceBlankLabel.FixedHeight = 8f;
                    //AttendanceInfoTable.AddCell(attendanceBlankLabel);

                    PdfPCell PresentLabel = new PdfPCell(new Phrase(" Present", ReportDataFont));
                    PresentLabel.BackgroundColor = new BaseColor(211, 211, 211);
                    PresentLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    PresentLabel.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(PresentLabel);

                    PdfPCell PresentDays = new PdfPCell(new Phrase(" " + keyValues["present"], ReportDataFont));
                    PresentDays.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    PresentDays.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(PresentDays);

                    PdfPCell AbsentLabel = new PdfPCell(new Phrase(" Absent", ReportDataFont));
                    AbsentLabel.BackgroundColor = new BaseColor(211, 211, 211);
                    AbsentLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    AbsentLabel.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(AbsentLabel);

                    PdfPCell AbsentDays = new PdfPCell(new Phrase(" " + keyValues["absent"], ReportDataFont));
                    AbsentDays.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    AbsentDays.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(AbsentDays);

                    PdfPCell LateinLabel = new PdfPCell(new Phrase(" Late in and early out", ReportDataFont));
                    LateinLabel.BackgroundColor = new BaseColor(211, 211, 211);
                    LateinLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    LateinLabel.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(LateinLabel);

                    PdfPCell lateinandearlyout = new PdfPCell(new Phrase(" " + keyValues["lateinandearlyout"], ReportDataFont));
                    lateinandearlyout.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    lateinandearlyout.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(lateinandearlyout);

                    PdfPCell OffdaysLabel = new PdfPCell(new Phrase(" Off days", ReportDataFont));
                    OffdaysLabel.BackgroundColor = new BaseColor(211, 211, 211);
                    OffdaysLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    OffdaysLabel.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(OffdaysLabel);

                    PdfPCell offdays = new PdfPCell(new Phrase(" " + keyValues["offdays"], ReportDataFont));
                    offdays.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    offdays.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(offdays);

                    PdfPCell ExtradaysLabel = new PdfPCell(new Phrase(" Extra days", ReportDataFont));
                    ExtradaysLabel.BackgroundColor = new BaseColor(211, 211, 211);
                    ExtradaysLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    ExtradaysLabel.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(ExtradaysLabel);

                    PdfPCell extradays = new PdfPCell(new Phrase(" " + keyValues["extradays"], ReportDataFont));
                    extradays.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    extradays.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(extradays);


                    PdfPCell holidaysLabel = new PdfPCell(new Phrase(" Holidays", ReportDataFont));
                    holidaysLabel.BackgroundColor = new BaseColor(211, 211, 211);
                    holidaysLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    holidaysLabel.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(holidaysLabel);

                    PdfPCell holidays = new PdfPCell(new Phrase(" " + keyValues["holidays"], ReportDataFont));
                    holidays.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    holidays.PaddingBottom = 5f;
                    AttendanceInfoTable.AddCell(holidays);

                    pdfDocument.Add(AttendanceInfoTable);
                    #endregion
                }

                #endregion




                pdfDocument.Close();

            }
            return pdfDocument;

        }

        public static Document GenratepaySlipPDFTemplateThree(FileStream resp, Dictionary<string, string> keyValues, DataSet dsRemunerationTable, DataSet dsAdditionTable, DataSet dsDeductionTable, int templateID, bool isSundriesInclude, List<HR_PaySundriesModel> PaySundryList, string AmountFormat)
        {
            Document pdfDocument;
            pdfDocument = new Document(PageSize.A4, 0f, 0f, 200f, 20f);   //Postrait
            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp))
            {
                pdfDocument.Open();
                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);

                iTextSharp.text.Font ReportDataFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font OtherHeadingContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font PaySlipName = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 13, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font PaySlipInfo = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, Black));
                iTextSharp.text.Font PaySlipInfo1 = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD | iTextSharp.text.Font.UNDERLINE, Black));
                iTextSharp.text.Font OtherHeadingContentBold = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 13, iTextSharp.text.Font.BOLD, Black));


                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = 220; //pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;

                #region Header part
                // create two column table
                PdfPTable head = new PdfPTable(2);
                head.TotalWidth = page.Width;
                head.LockedWidth = true;

                float[] widths = new float[] { 140f, 400f };
                head.SetWidths(widths);


                // add image; PdfPCell() overload sizes image to fit cell
                PdfPCell c = new PdfPCell(jpg, true);
                c.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //c.HorizontalAlignment = Element.ALIGN_LEFT;
                c.FixedHeight = cellHeight;

                c.Border = PdfPCell.NO_BORDER;
                c.PaddingLeft = 50f;
                c.PaddingTop = 40f;
                c.PaddingBottom = 20f;
                head.AddCell(c);
                // add the header text

                PdfPTable Title = new PdfPTable(1);
                float[] Titlewidths = new float[] { 445f };
                Title.SetWidths(Titlewidths);


                PdfPCell SchoolName = new PdfPCell(new Phrase(Schoolname, Heading));
                SchoolName.Border = PdfPCell.NO_BORDER;
                SchoolName.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Blankcell = new PdfPCell(new Phrase("", Heading));
                Blankcell.Border = PdfPCell.NO_BORDER;
                Blankcell.HorizontalAlignment = Element.ALIGN_CENTER;
                Blankcell.FixedHeight = 10f;

                PdfPCell Address = new PdfPCell(new Phrase(keyValues["address1"] + " " + keyValues["city"] + " " + keyValues["country"] + " P.O.Box: " + keyValues["pobox"], OtherHeadingContent));
                Address.Border = PdfPCell.NO_BORDER;
                Address.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Phone = new PdfPCell(new Phrase("Phone: " + keyValues["phoneno"], OtherHeadingContent));
                Phone.Border = PdfPCell.NO_BORDER;
                Phone.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Fax = new PdfPCell(new Phrase("Fax: " + keyValues["faxno"], OtherHeadingContent));
                Fax.Border = PdfPCell.NO_BORDER;
                Fax.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell Email = new PdfPCell(new Phrase("Email ID: " + keyValues["emailid"], OtherHeadingContent));
                Email.Border = PdfPCell.NO_BORDER;
                Email.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell WebSite = new PdfPCell(new Phrase("Website: " + keyValues["website"], OtherHeadingContent));
                WebSite.Border = PdfPCell.NO_BORDER;
                WebSite.HorizontalAlignment = Element.ALIGN_CENTER;

                PdfPCell blanckCell = new PdfPCell(new Phrase("", OtherHeadingContent));
                blanckCell.Border = PdfPCell.NO_BORDER;
                blanckCell.HorizontalAlignment = Element.ALIGN_CENTER;
                blanckCell.FixedHeight = 10f;

                PdfPCell paySlipCycle = new PdfPCell(new Phrase(keyValues["monthname"] + " " + keyValues["YearName"] + " PAY SLIP", PaySlipName));
                paySlipCycle.Border = PdfPCell.NO_BORDER;
                paySlipCycle.HorizontalAlignment = Element.ALIGN_CENTER;


                Title.AddCell(SchoolName);
                Title.AddCell(Blankcell);
                Title.AddCell(Address);
                Title.AddCell(Phone);
                Title.AddCell(Fax);
                Title.AddCell(Email);
                Title.AddCell(WebSite);
                Title.AddCell(blanckCell);
                Title.AddCell(paySlipCycle);
                PdfPCell c1 = new PdfPCell();
                c1.AddElement(Title);
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape
                c1.PaddingTop = 40F;
                c1.PaddingLeft = 0f;
                c1.PaddingRight = 100f;
                c1.FixedHeight = cellHeight;
                head.AddCell(c1);



                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );

                // pdfDocument.Add(head);
                #endregion
                //Employee Name 
                pdfDocument.SetMargins(0f, 0f, 20f, 20f);
                PdfPTable EmployeeNameTable = new PdfPTable(1);
                float[] EmployeeNameWidths = new float[] { 460f };
                EmployeeNameTable.TotalWidth = page.Width - 30;
                EmployeeNameTable.LockedWidth = true;
                EmployeeNameTable.SetWidths(EmployeeNameWidths);



                PdfPCell NameOfEmployee = new PdfPCell(new Phrase("Employee Name: " + keyValues["employeename"], PaySlipInfo1));
                NameOfEmployee.Border = PdfPCell.NO_BORDER;
                NameOfEmployee.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                //NameOfEmployee.PaddingTop = 0f;               
                NameOfEmployee.PaddingLeft = 45f;
                NameOfEmployee.PaddingBottom = 15f;
                //NameOfEmployee.FixedHeight = 15f;
                EmployeeNameTable.AddCell(NameOfEmployee);

                PdfPCell NameOfEmployee1 = new PdfPCell(new Phrase("", PaySlipInfo));
                NameOfEmployee1.Border = PdfPCell.NO_BORDER;
                EmployeeNameTable.AddCell(NameOfEmployee1);


                pdfDocument.Add(EmployeeNameTable);

                #region 2nd Part
                PdfPTable PaymentInfoTable = new PdfPTable(1);
                float[] paymentinfoWidths = new float[] { 460f };
                PaymentInfoTable.TotalWidth = page.Width - 30;
                PaymentInfoTable.LockedWidth = true;
                PaymentInfoTable.SetWidths(paymentinfoWidths);
                // PaymentInfoTable.PaddingTop = 20f;

                PdfPCell dateofPayment = new PdfPCell(new Phrase("Date of Payment: " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(keyValues["paymentdate"]), PaySlipInfo));
                dateofPayment.Border = PdfPCell.NO_BORDER;
                dateofPayment.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                dateofPayment.PaddingTop = 0f;
                dateofPayment.PaddingRight = 30f;
                dateofPayment.PaddingLeft = 45f;
                dateofPayment.FixedHeight = 15f;

                PdfPCell payperiod = new PdfPCell(new Phrase("Pay Period: " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(keyValues["payfrom"]) + " to " + DataAccess.GeneralDB.CommonDB.GetFormattedDate_DDMMYYYY(keyValues["payto"]), PaySlipInfo));
                payperiod.Border = PdfPCell.NO_BORDER;
                payperiod.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                payperiod.PaddingTop = 0f;
                payperiod.PaddingLeft = 45f;
                payperiod.FixedHeight = 15f;


                PaymentInfoTable.AddCell(dateofPayment);
                PaymentInfoTable.AddCell(payperiod);

                pdfDocument.Add(PaymentInfoTable);
                #endregion
                #region PageContent

                #region PersonalInfo

                PdfPTable PersonalInfoTable = new PdfPTable(4);
                PersonalInfoTable.SpacingBefore = 5f;
                PersonalInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                //PersonalInfoTable.SpacingBefore = 20f;
                PdfPCell personalInfoLabel = new PdfPCell(new Phrase("Personal Information", OtherHeadingContentBold));
                personalInfoLabel.Colspan = 4;
                personalInfoLabel.Border = PdfPCell.NO_BORDER;
                personalInfoLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                personalInfoLabel.PaddingTop = 0f;
                personalInfoLabel.PaddingLeft = 0f;
                personalInfoLabel.FixedHeight = 20f;
                PersonalInfoTable.AddCell(personalInfoLabel);

                PdfPCell EmployeeIDLabel = new PdfPCell(new Phrase(" Employee ID", ReportDataFont));
                EmployeeIDLabel.BackgroundColor = new BaseColor(211, 211, 211);
                EmployeeIDLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeIDLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeIDLabel);

                PdfPCell EmployeeID = new PdfPCell(new Phrase(" " + keyValues["employeeid"], ReportDataFont));
                EmployeeID.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeID.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeID);

                PdfPCell PositionLabel = new PdfPCell(new Phrase(" Position", ReportDataFont));
                PositionLabel.BackgroundColor = new BaseColor(211, 211, 211);
                PositionLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                PositionLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(PositionLabel);

                PdfPCell Position = new PdfPCell(new Phrase(" " + keyValues["empposition"], ReportDataFont));
                Position.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Position.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Position);



                PdfPCell DepartmentLabel = new PdfPCell(new Phrase(" Department", ReportDataFont));
                DepartmentLabel.BackgroundColor = new BaseColor(211, 211, 211);
                DepartmentLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DepartmentLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(DepartmentLabel);

                PdfPCell Department = new PdfPCell(new Phrase(" " + keyValues["department"], ReportDataFont));
                Department.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Department.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Department);


                PdfPCell AccountNoLabel = new PdfPCell(new Phrase(" Account Number", ReportDataFont));
                AccountNoLabel.BackgroundColor = new BaseColor(211, 211, 211);
                AccountNoLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AccountNoLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(AccountNoLabel);

                PdfPCell AccountNo = new PdfPCell(new Phrase(" " + keyValues["accountnumber"].ToString(), ReportDataFont));
                AccountNo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AccountNo.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(AccountNo);



                PdfPCell BankLabel = new PdfPCell(new Phrase(" Bank Name", ReportDataFont));
                BankLabel.BackgroundColor = new BaseColor(211, 211, 211);
                BankLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                BankLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(BankLabel);

                PdfPCell Bank = new PdfPCell(new Phrase(" " + keyValues["bankname"], ReportDataFont));
                Bank.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Bank.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Bank);

                PdfPCell RemainingofLabel = new PdfPCell(new Phrase(" Remaining off days", ReportDataFont));
                RemainingofLabel.BackgroundColor = new BaseColor(211, 211, 211);
                RemainingofLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemainingofLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(RemainingofLabel);

                PdfPCell Remainingof = new PdfPCell(new Phrase(" " + keyValues["remainingoffdays"], ReportDataFont));
                Remainingof.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Remainingof.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(Remainingof);


                PdfPCell ServiceSinceLabel = new PdfPCell(new Phrase(" In service since", ReportDataFont));
                ServiceSinceLabel.BackgroundColor = new BaseColor(211, 211, 211);
                ServiceSinceLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                ServiceSinceLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(ServiceSinceLabel);

                PdfPCell ServiceSince = new PdfPCell(new Phrase(" " + keyValues["inservicesince"], ReportDataFont));
                ServiceSince.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                ServiceSince.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(ServiceSince);

                PdfPCell EmployeeNameLabel = new PdfPCell(new Phrase("", ReportDataFont));
                EmployeeNameLabel.BackgroundColor = new BaseColor(211, 211, 211);
                EmployeeNameLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeNameLabel.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeNameLabel);

                PdfPCell EmployeeName = new PdfPCell(new Phrase("", ReportDataFont));
                EmployeeName.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                EmployeeName.PaddingBottom = 5f;
                PersonalInfoTable.AddCell(EmployeeName);



                pdfDocument.Add(PersonalInfoTable);
                #endregion

                #region SalaryInfo
                PdfPTable salaryInfoTable = new PdfPTable(3);
                salaryInfoTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;


                PdfPCell blanckLabel = new PdfPCell(new Phrase("", OtherHeadingContentBold));
                blanckLabel.Colspan = 3;
                blanckLabel.Border = PdfPCell.NO_BORDER;
                blanckLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                blanckLabel.PaddingTop = 0f;
                blanckLabel.FixedHeight = 20f;
                salaryInfoTable.AddCell(blanckLabel);

                PdfPCell salaryInfoLabel = new PdfPCell(new Phrase("Salary Information", OtherHeadingContentBold));
                salaryInfoLabel.Colspan = 3;
                salaryInfoLabel.Border = PdfPCell.NO_BORDER;
                salaryInfoLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                salaryInfoLabel.PaddingTop = 0f;
                salaryInfoLabel.PaddingLeft = 0f;
                salaryInfoLabel.FixedHeight = 20f;
                salaryInfoTable.AddCell(salaryInfoLabel);

                PdfPCell RemunerationLabel = new PdfPCell(new Phrase("Remuneration", ReportDataFont));
                RemunerationLabel.BackgroundColor = new BaseColor(211, 211, 211);
                RemunerationLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemunerationLabel.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                RemunerationLabel.PaddingBottom = 5f;
                salaryInfoTable.AddCell(RemunerationLabel);

                PdfPCell AdditionLabel = new PdfPCell(new Phrase("Addition", ReportDataFont));
                AdditionLabel.BackgroundColor = new BaseColor(211, 211, 211);
                AdditionLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AdditionLabel.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                AdditionLabel.PaddingBottom = 5f;
                salaryInfoTable.AddCell(AdditionLabel);

                PdfPCell DeductionLabelLabel = new PdfPCell(new Phrase("Deduction", ReportDataFont));
                DeductionLabelLabel.BackgroundColor = new BaseColor(211, 211, 211);
                DeductionLabelLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DeductionLabelLabel.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                DeductionLabelLabel.PaddingBottom = 5f;
                salaryInfoTable.AddCell(DeductionLabelLabel);

                PdfPTable RemunerationTabel = new PdfPTable(2);
                if (dsRemunerationTable.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsRemunerationTable.Tables[0].Rows)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell(new Phrase(dr["itemdescr"].ToString(), ReportDataFont));
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase(dr["amount"].ToString(), ReportDataFont));
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        RemunerationTabel.AddCell(descrCell);
                        RemunerationTabel.AddCell(amountCell);
                    }
                    if (dsRemunerationTable.Tables[0].Rows.Count < 3)
                    {
                        int rowsCount = dsRemunerationTable.Tables[0].Rows.Count;

                        for (int i = 0; i <= 3 - rowsCount; i++)
                        {
                            PdfPCell descrCell;
                            descrCell = new PdfPCell();
                            descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            descrCell.PaddingBottom = 5f;
                            descrCell.FixedHeight = 15f;
                            PdfPCell amountCell;
                            amountCell = new PdfPCell(new Phrase());
                            amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            amountCell.PaddingBottom = 5f;
                            amountCell.FixedHeight = 15f;
                            RemunerationTabel.AddCell(descrCell);
                            RemunerationTabel.AddCell(amountCell);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell();
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        descrCell.FixedHeight = 15f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase());
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        amountCell.FixedHeight = 15f;
                        RemunerationTabel.AddCell(descrCell);
                        RemunerationTabel.AddCell(amountCell);
                    }
                }


                PdfPCell RemunerationDetails = new PdfPCell();
                //RemunerationDetails.Colspan = 3;
                RemunerationDetails.AddElement(RemunerationTabel);
                RemunerationDetails.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemunerationDetails.PaddingBottom = 5f;
                salaryInfoTable.AddCell(RemunerationDetails);


                PdfPTable AdditionTable = new PdfPTable(2);
                if (dsAdditionTable.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsAdditionTable.Tables[0].Rows)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell(new Phrase(" " + dr["itemdescr"].ToString(), ReportDataFont));
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase(" " + dr["amount"].ToString(), ReportDataFont));
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        AdditionTable.AddCell(descrCell);
                        AdditionTable.AddCell(amountCell);
                    }
                    if (dsAdditionTable.Tables[0].Rows.Count < 3)
                    {
                        int rowsCount = dsAdditionTable.Tables[0].Rows.Count;

                        for (int i = 0; i <= 3 - rowsCount; i++)
                        {
                            PdfPCell descrCell;
                            descrCell = new PdfPCell();
                            descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            descrCell.PaddingBottom = 5f;
                            descrCell.FixedHeight = 15f;
                            PdfPCell amountCell;
                            amountCell = new PdfPCell(new Phrase());
                            amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            amountCell.PaddingBottom = 5f;
                            amountCell.FixedHeight = 15f;
                            AdditionTable.AddCell(descrCell);
                            AdditionTable.AddCell(amountCell);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell();
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        descrCell.FixedHeight = 15f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase());
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        amountCell.FixedHeight = 15f;
                        AdditionTable.AddCell(descrCell);
                        AdditionTable.AddCell(amountCell);
                    }
                }

                PdfPCell AdditionDetails = new PdfPCell();
                AdditionDetails.AddElement(AdditionTable);
                AdditionDetails.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AdditionDetails.PaddingBottom = 5f;
                salaryInfoTable.AddCell(AdditionDetails);

                PdfPTable DeductionTable = new PdfPTable(2);
                if (dsDeductionTable.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsDeductionTable.Tables[0].Rows)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell(new Phrase(" " + dr["itemdescr"].ToString(), ReportDataFont));
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase(" " + dr["amount"].ToString(), ReportDataFont));
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        DeductionTable.AddCell(descrCell);
                        DeductionTable.AddCell(amountCell);
                    }

                    if (dsDeductionTable.Tables[0].Rows.Count < 3)
                    {
                        int rowsCount = dsDeductionTable.Tables[0].Rows.Count;

                        for (int i = 0; i <= 3 - rowsCount; i++)
                        {
                            PdfPCell descrCell;
                            descrCell = new PdfPCell();
                            descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            descrCell.PaddingBottom = 5f;
                            descrCell.FixedHeight = 15f;
                            PdfPCell amountCell;
                            amountCell = new PdfPCell(new Phrase());
                            amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            amountCell.PaddingBottom = 5f;
                            amountCell.FixedHeight = 15f;
                            DeductionTable.AddCell(descrCell);
                            DeductionTable.AddCell(amountCell);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        PdfPCell descrCell;
                        descrCell = new PdfPCell();
                        descrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        descrCell.PaddingBottom = 5f;
                        descrCell.FixedHeight = 15f;
                        PdfPCell amountCell;
                        amountCell = new PdfPCell(new Phrase());
                        amountCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        amountCell.PaddingBottom = 5f;
                        amountCell.FixedHeight = 15f;
                        DeductionTable.AddCell(descrCell);
                        DeductionTable.AddCell(amountCell);
                    }
                }

                PdfPCell DeductionDetails = new PdfPCell();
                DeductionDetails.AddElement(DeductionTable);
                DeductionDetails.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DeductionDetails.PaddingBottom = 5f;
                salaryInfoTable.AddCell(DeductionDetails);

                PdfPCell RemunerationTotal = new PdfPCell(new Phrase("Total: " + keyValues["remtotal"], ReportDataFont));
                RemunerationTotal.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RemunerationTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                RemunerationTotal.PaddingBottom = 5f;
                salaryInfoTable.AddCell(RemunerationTotal);

                PdfPCell AdditionTotal = new PdfPCell(new Phrase("Total: " + keyValues["addtotal"], ReportDataFont));
                AdditionTotal.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                AdditionTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                AdditionTotal.PaddingBottom = 5f;
                salaryInfoTable.AddCell(AdditionTotal);

                PdfPCell DeductionTotal = new PdfPCell(new Phrase("Total: " + keyValues["dedtotal"], ReportDataFont));
                DeductionTotal.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                DeductionTotal.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                DeductionTotal.PaddingBottom = 5f;
                salaryInfoTable.AddCell(DeductionTotal);

                PdfPCell CurrentNetSalary = new PdfPCell(new Phrase(" Current Net Salary: " + keyValues["netsalary"], ReportDataFont));
                CurrentNetSalary.Colspan = 3;
                CurrentNetSalary.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                CurrentNetSalary.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                CurrentNetSalary.PaddingBottom = 5f;
                salaryInfoTable.AddCell(CurrentNetSalary);


                pdfDocument.Add(salaryInfoTable);

                #endregion

                #endregion

                #region SundryInformation

                if (isSundriesInclude)
                {
                    decimal totalAmt = 0;
                    DataTable Dt = new DataTable();
                    Dt.Columns.Add("Pay Sundries Date");
                    Dt.Columns.Add("Sundry");
                    Dt.Columns.Add("Batch");
                    Dt.Columns.Add("Academic Year");
                    Dt.Columns.Add("Amount");
                    foreach (var item in PaySundryList)
                    {
                        Dt.Rows.Add(item.PaySundriesDate, item.SundryName, item.BankName, item.AcademicYear, item.Amount.ToString(AmountFormat));
                        totalAmt = totalAmt + item.Amount;
                    }
                    PdfPTable paySundryTable = new PdfPTable(5);
                    paySundryTable.SpacingBefore = 5f;
                    PdfPCell SundryInfoLabel = new PdfPCell(new Phrase("Sundries Information", OtherHeadingContentBold));
                    SundryInfoLabel.Colspan = 5;
                    SundryInfoLabel.Border = PdfPCell.NO_BORDER;
                    SundryInfoLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    SundryInfoLabel.PaddingTop = 0f;
                    SundryInfoLabel.PaddingLeft = 0f;
                    SundryInfoLabel.FixedHeight = 20f;
                    paySundryTable.AddCell(SundryInfoLabel);
                    paySundryTable = GetPdfTable(Dt, paySundryTable, ReportDataFont, ReportDataFont, PdfPCell.LEFT_BORDER, PdfPCell.LEFT_BORDER, 5f, new BaseColor(211, 211, 211));
                    PdfPCell totalSundryLabel = new PdfPCell(new Phrase("Net Sundries:" + totalAmt.ToString(AmountFormat), ReportDataFont));
                    totalSundryLabel.Colspan = 5;
                    totalSundryLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    totalSundryLabel.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    totalSundryLabel.PaddingBottom = 5f;
                    paySundryTable.AddCell(totalSundryLabel);
                    pdfDocument.Add(paySundryTable);
                }

                #endregion



                pdfDocument.Close();

            }
            return pdfDocument;

        }

        public static PdfPTable GetPdfTable(DataTable Dt, PdfPTable table, iTextSharp.text.Font headingFont, iTextSharp.text.Font dataFont, int HorizantalAlignmentHeader, int HorizantalVerticalAlignmentData, float PaddingBottom, BaseColor BaseColor)
        {
            foreach (DataColumn Dc in Dt.Columns)
            {
                PdfPCell Heading = new PdfPCell(new Phrase(Dc.ColumnName, headingFont));
                Heading.BackgroundColor = BaseColor;
                Heading.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Heading.VerticalAlignment = HorizantalAlignmentHeader;
                Heading.PaddingBottom = PaddingBottom;
                table.AddCell(Heading);
            }
            foreach (DataRow dr in Dt.Rows)
            {
                foreach (DataColumn Dc in Dt.Columns)
                {
                    PdfPCell DataCell = new PdfPCell(new Phrase(dr[Dc.ColumnName].ToString(), dataFont));
                    DataCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    DataCell.VerticalAlignment = HorizantalVerticalAlignmentData;
                    DataCell.PaddingBottom = PaddingBottom;
                    table.AddCell(DataCell);
                }
            }
            return table;
        }

        public static byte[] CreateDepartmentExcelsheet(DataSet ds, int CompId)
        {
            ds.Tables[0].Columns.Add("Department Employee List");
            using (MemoryStream mem = new MemoryStream())
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(mem, SpreadsheetDocumentType.Workbook))
                {
                    // create the workbook
                    spreadSheet.AddWorkbookPart();
                    spreadSheet.WorkbookPart.Workbook = new Workbook();     // create the worksheet
                    spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet = new Worksheet();

                    // create sheet data
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.AppendChild(new SheetData());

                    //// save worksheet
                    spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet.Save();

                    //// create the worksheet to workbook relation
                    spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());

                    foreach (System.Data.DataTable table in ds.Tables)
                    {

                        var sheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                        var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                        sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                        DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                        string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(sheetPart);

                        uint sheetId = 1;
                        if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                        {
                            sheetId = sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                        sheets.Append(sheet);

                        SchoolInformation sc = new SchoolInformation();
                        sc = new SchoolInformationDB().GetBasicSchoolInformation();
                        string Schoolname = sc.SchoolName_1;
                        if (!string.IsNullOrEmpty(sc.CampusName_1))
                        {
                            Schoolname += "-" + sc.CampusName_1;
                        }

                        //// School Basic Info
                        DocumentFormat.OpenXml.Spreadsheet.Row BasicInformationRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        DocumentFormat.OpenXml.Spreadsheet.Cell SchoolInfoCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        SchoolInfoCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        SchoolInfoCell.StyleIndex = 1;
                        SchoolInfoCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("School Name: " + Schoolname);
                        BasicInformationRow.AppendChild(SchoolInfoCell);
                        //DocumentFormat.OpenXml.Spreadsheet.Row BasicInfo = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        sheetData.AppendChild(BasicInformationRow);
                        ///////////


                        //// CurrentDate
                        DocumentFormat.OpenXml.Spreadsheet.Row TodaysDate = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        DocumentFormat.OpenXml.Spreadsheet.Cell TodaysDateCell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        TodaysDateCell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        TodaysDateCell.StyleIndex = 1;
                        TodaysDateCell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Exported Date: " + DateTime.Now.ToString("dd MMM yyyy"));
                        TodaysDate.AppendChild(TodaysDateCell);
                        //DocumentFormat.OpenXml.Spreadsheet.Row BasicInfo = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        sheetData.AppendChild(TodaysDate);
                        ///////////
                        ////Blank Row
                        sheetData.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Row());

                        DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        /// Styling 
                        WorkbookStylesPart stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                        //stylesPart.Stylesheet = CreateStylesheet();
                        stylesPart.Stylesheet = GenerateStyleSheet();
                        stylesPart.Stylesheet.Save();
                        List<int> deptid = new List<int>();
                        foreach (DataRow DR in ds.Tables[0].Rows)
                        {
                            deptid.Add(Convert.ToInt32(DR[0].ToString()));
                        }
                        ds.Tables[0].Columns.RemoveAt(0);


                        List<String> columns = new List<string>();
                        foreach (System.Data.DataColumn column in table.Columns)
                        {
                            columns.Add(column.ColumnName);

                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                            cell.StyleIndex = 1;
                            headerRow.AppendChild(cell);
                        }


                        sheetData.AppendChild(headerRow);
                        int rowsCount = 0;
                        foreach (System.Data.DataRow dsrow in table.Rows)
                        {

                            DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            foreach (String col in columns)
                            {
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (col == "Department Employee List")
                                {
                                    int deptID = deptid[rowsCount];

                                    DepartmentEmployeeDB objDepEmpDB = new DepartmentEmployeeDB();
                                    List<EmployeeDetails> lstEmp = new List<EmployeeDetails>();
                                    lstEmp = objDepEmpDB.GetEmployeeByDepartmentId(deptID, CompId);
                                    rowsCount++;
                                    string EmpNames = "";
                                    foreach (var item in lstEmp)
                                    {
                                        EmpNames = EmpNames + item.FirstName_1 + " " + item.SurName_1 + ",";
                                    }
                                    if (EmpNames.Contains(","))
                                    {
                                        EmpNames = EmpNames.Remove(EmpNames.LastIndexOf(","));
                                    }
                                    cell.StyleIndex = 7;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(EmpNames);
                                }
                                else
                                {
                                    //cell.StyleIndex = 7;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                                }


                                newRow.AppendChild(cell);
                            }

                            sheetData.AppendChild(newRow);
                        }

                    }

                    spreadSheet.WorkbookPart.Workbook.Save();
                    spreadSheet.Close();
                    return mem.ToArray();
                }
            }
        }

        public static byte[] CreateWordDoc(DataTable dt, int[] widthArray)
        {
            try
            {
                int count = 0;
                var stream = new MemoryStream();
                string docName = HttpContext.Current.Server.MapPath("~/Uploads/DownloadTemplate");
                using (MemoryStream mem = new MemoryStream())
                {
                    using (WordprocessingDocument myDoc = WordprocessingDocument.Create(mem, WordprocessingDocumentType.Document))
                    {
                        // Add a new main document part. 

                        MainDocumentPart mainPart = myDoc.AddMainDocumentPart();
                        //Create DOM tree for simple document. 
                        mainPart.Document = new openXmlDoc.Document();
                        openXmlDoc.Body body = new openXmlDoc.Body();
                        openXmlDoc.Table table = new openXmlDoc.Table();

                        table.AppendChild<openXmlDoc.TableProperties>(GetTableProperties(12, 12, 12, 12, 12, 12));

                        openXmlDoc.SectionProperties sectionProperties = new DocumentFormat.OpenXml.Wordprocessing.SectionProperties();
                        openXmlDoc.PageMargin pageMargin = new openXmlDoc.PageMargin() { Top = 1000, Right = 1000, Left = 1200, Bottom = 1000 };
                        openXmlDoc.PageSize pageSize = new openXmlDoc.PageSize() { Width = (UInt32Value)15840U, Orient = openXmlDoc.PageOrientationValues.Landscape };
                        sectionProperties.AppendChild(pageMargin);
                        sectionProperties.AppendChild(pageSize);
                        body.AppendChild(sectionProperties);
                        List<string> columnArray = new List<string>();
                        openXmlDoc.TableRow trHeader = new openXmlDoc.TableRow();
                        foreach (var item in dt.Columns) // Loop over the items.
                        {
                            columnArray.Add(item.ToString());
                            openXmlDoc.TableCell tc = new openXmlDoc.TableCell();
                            openXmlDoc.RunProperties properties = new openXmlDoc.RunProperties();
                            openXmlDoc.Bold bold = new openXmlDoc.Bold();
                            openXmlDoc.FontSize fontSize = new openXmlDoc.FontSize { Val = "20" };
                            openXmlDoc.RunFonts runFonts = new openXmlDoc.RunFonts() { Ascii = "Arial" };
                            properties.AppendChild(bold);
                            properties.AppendChild(fontSize);
                            properties.AppendChild(runFonts);
                            tc.AppendChild<openXmlDoc.TableCellProperties>(GetTableCellProperties(6, 6, 6, 6, 6, 6, widthArray[count++].ToString()));
                            openXmlDoc.Paragraph paraInner = new openXmlDoc.Paragraph();
                            openXmlDoc.Run innerRun3 = new openXmlDoc.Run();

                            openXmlDoc.TableCellRightMargin cellRightMargin = new openXmlDoc.TableCellRightMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
                            openXmlDoc.TableCellLeftMargin cellLeftMargin = new openXmlDoc.TableCellLeftMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
                            innerRun3.AppendChild(cellRightMargin);
                            innerRun3.AppendChild(cellLeftMargin);

                            openXmlDoc.Text innerText3 = new openXmlDoc.Text(item.ToString());
                            innerRun3.AppendChild(properties);
                            paraInner.AppendChild(GetParagraphProp("Center"));
                            innerRun3.AppendChild(innerText3);
                            paraInner.AppendChild(innerRun3);
                            tc.AppendChild(paraInner);
                            trHeader.Append(tc);
                        }
                        table.AppendChild(trHeader);
                        openXmlDoc.TableRow tr;
                        count = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            tr = new openXmlDoc.TableRow();
                            count = 0;
                            foreach (var item in columnArray) // Loop over the items.
                            {
                                openXmlDoc.TableCell tc = new openXmlDoc.TableCell();
                                openXmlDoc.RunProperties properties = new openXmlDoc.RunProperties();
                                openXmlDoc.FontSize fontSize = new openXmlDoc.FontSize { Val = "20" };
                                openXmlDoc.RunFonts runFonts = new openXmlDoc.RunFonts() { Ascii = "Arial" };
                                properties.AppendChild(fontSize);
                                properties.AppendChild(runFonts);
                                tc.AppendChild<openXmlDoc.TableCellProperties>(GetTableCellProperties(6, 6, 6, 6, 6, 6, widthArray[count++].ToString()));
                                openXmlDoc.Paragraph paraInner = new openXmlDoc.Paragraph();
                                openXmlDoc.Run innerRun3 = new openXmlDoc.Run();
                                openXmlDoc.Text innerText3 = new openXmlDoc.Text(dr[item].ToString());
                                innerRun3.AppendChild(properties);
                                paraInner.AppendChild(GetParagraphProp("Center"));
                                innerRun3.AppendChild(innerText3);
                                paraInner.AppendChild(innerRun3);
                                tc.AppendChild(paraInner);
                                tr.Append(tc);
                            }
                            tr.AppendChild<openXmlDoc.TableRowProperties>(GetTableRowProperties(12, 12, 12, 12, 12, 12));
                            table.AppendChild(tr);
                        }

                        body.AppendChild(GetHeader(mainPart));
                        body.AppendChild(GetParaGraph(""));
                        body.AppendChild(table);
                        mainPart.Document.Append(body);
                        // Save changes to the main document part. 
                        mainPart.Document.Save();

                        myDoc.MainDocumentPart.Document.Save();
                        myDoc.Close();

                        return mem.ToArray();
                    }
                }

               ;
            }
            catch (Exception e)
            {
                var content = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                return content;
            }
        }

        public static openXmlDoc.Table GetHeader(MainDocumentPart mainPart)
        {
            openXmlDoc.Table tableHeader = new openXmlDoc.Table();
            tableHeader.AppendChild<openXmlDoc.TableProperties>(GetTableProperties(0, 0, 0, 0, 0, 0));
            openXmlDoc.TableRow trHeader = new openXmlDoc.TableRow();
            openXmlDoc.TableCell tc0 = new openXmlDoc.TableCell();
            openXmlDoc.TableCell tc1 = new openXmlDoc.TableCell();
            openXmlDoc.TableCell tc2 = new openXmlDoc.TableCell();
            openXmlDoc.TableCell tc3 = new openXmlDoc.TableCell();
            tc0.AppendChild<openXmlDoc.TableCellProperties>(GetTableCellProperties(0, 0, 0, 0, 0, 0, "25", "FFFFFF", "top"));
            tc1.AppendChild<openXmlDoc.TableCellProperties>(GetTableCellProperties(0, 0, 0, 0, 0, 0, "20", "FFFFFF", "top"));
            tc2.AppendChild<openXmlDoc.TableCellProperties>(GetTableCellProperties(0, 0, 0, 0, 0, 0, "30", "FFFFFF"));
            tc3.AppendChild<openXmlDoc.TableCellProperties>(GetTableCellProperties(0, 0, 0, 0, 0, 0, "25", "FFFFFF", "top", "top"));
            openXmlDoc.RunProperties properties = new openXmlDoc.RunProperties();
            openXmlDoc.Bold bold = new openXmlDoc.Bold();
            openXmlDoc.FontSize fontSize = new openXmlDoc.FontSize { Val = "32" };
            openXmlDoc.RunFonts runFonts = new openXmlDoc.RunFonts() { Ascii = "Arial" };
            properties.AppendChild(bold);
            properties.AppendChild(fontSize);
            properties.AppendChild(runFonts);
            openXmlDoc.Paragraph para0 = new openXmlDoc.Paragraph();
            openXmlDoc.Paragraph para1 = new openXmlDoc.Paragraph();

            SchoolInformation sc = new SchoolInformation();
            sc = new SchoolInformationDB().GetBasicSchoolInformation();

            openXmlDoc.Run run0 = new openXmlDoc.Run();
            openXmlDoc.Run run1 = new openXmlDoc.Run();
            openXmlDoc.Text innerText = new openXmlDoc.Text(sc.SchoolName_1);
            run0.AppendChild(properties);
            para0.AppendChild(GetParagraphProp("Left"));
            run0.AppendChild(innerText);
            para0.AppendChild(run0);
            tc0.AppendChild(GetParaGraph(""));
            tc2.AppendChild(para0);
            //run1.AppendChild(AddImageToRun("Logo.jpg", 500, 500, "{28A0092B-C50C-407E-A947-70E740481C1C}", Server.MapPath("~/Content/Image/logo.jpg")));
            para1.AppendChild(run1);
            tc3.AppendChild(GetParaGraph("Exported Date: " + DateTime.Now.ToString("dd MMM yyyy"), "right"));

            //ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
            string logoPath = HttpContext.Current.Server.MapPath(sc.Logo);
            //using (FileStream stream = new FileStream(logoPath, FileMode.Open))
            //{
            //    imagePart.FeedData(stream);
            //}

            //tc1.AppendChild(AddImageToCell(mainPart.GetIdOfPart(imagePart)));
            //tc1.AppendChild(GetParaGraph(""));

            //2019-02-18 Changed width and height values as the logo image was appearing very big 
            //AddImage(tc1, mainPart, logoPath, "rId1", 2743200, 2743200);
            AddImage(tc1, mainPart, logoPath, "rId1", 822960, 822960);

            trHeader.Append(tc0);
            trHeader.Append(tc1);
            trHeader.Append(tc2);
            trHeader.Append(tc3);
            tableHeader.AppendChild(trHeader);
            return tableHeader;

        }

        public static openXmlDoc.Paragraph GetParaGraph(string displayText = "", string alignMOde = "center")
        {
            openXmlDoc.RunProperties properties = new openXmlDoc.RunProperties();
            openXmlDoc.FontSize fontSize = new openXmlDoc.FontSize { Val = "22" };
            openXmlDoc.RunFonts runFonts = new openXmlDoc.RunFonts() { Ascii = "Arial" };
            properties.AppendChild(fontSize);
            properties.AppendChild(runFonts);
            openXmlDoc.Paragraph para = new openXmlDoc.Paragraph();
            openXmlDoc.ParagraphProperties paraProPerties = new openXmlDoc.ParagraphProperties();
            openXmlDoc.SpacingBetweenLines spacing = new openXmlDoc.SpacingBetweenLines() { Line = "240", LineRule = openXmlDoc.LineSpacingRuleValues.Auto, Before = "50", After = "50", AfterLines = 50, BeforeLines = 50 };
            openXmlDoc.Justification justification = new openXmlDoc.Justification()
            { Val = (alignMOde.ToLower() == "center" ? openXmlDoc.JustificationValues.Center : (alignMOde.ToLower() == "left" ? openXmlDoc.JustificationValues.Left : openXmlDoc.JustificationValues.Right)) };
            openXmlDoc.TableCellRightMargin cellRightMargin = new openXmlDoc.TableCellRightMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
            openXmlDoc.TableCellLeftMargin cellLeftMargin = new openXmlDoc.TableCellLeftMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
            openXmlDoc.TopMargin topMargin = new openXmlDoc.TopMargin() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = "200" };
            openXmlDoc.BottomMargin bottomMargin = new openXmlDoc.BottomMargin() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = "200" };

            openXmlDoc.TableCellVerticalAlignment verticalalignMent = new openXmlDoc.TableCellVerticalAlignment()
            {
                Val = openXmlDoc.TableVerticalAlignmentValues.Center
            };

            paraProPerties.AppendChild(cellRightMargin);
            paraProPerties.AppendChild(cellLeftMargin);
            paraProPerties.AppendChild(justification);
            paraProPerties.AppendChild(verticalalignMent);
            paraProPerties.AppendChild(topMargin);
            paraProPerties.AppendChild(spacing);
            paraProPerties.AppendChild(bottomMargin);
            para.AppendChild(paraProPerties);
            openXmlDoc.Run run = new openXmlDoc.Run();
            run.AppendChild(properties);
            openXmlDoc.Text text = new openXmlDoc.Text(displayText);
            run.AppendChild(text);
            para.AppendChild(run);
            return para;
        }

        public static openXmlDoc.TableProperties GetTableProperties(int topValue, int rightvalue, int bottomValue, int leftvalue, int horizontalValue, int verticalValue, string width = "13630", string tableJustification = "")
        {
            openXmlDoc.TableProperties props = new openXmlDoc.TableProperties(
            new openXmlDoc.TableWidth() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = width },
             new openXmlDoc.TableJustification() { Val = tableJustification == "center" ? openXmlDoc.TableRowAlignmentValues.Center: tableJustification == "left"? openXmlDoc.TableRowAlignmentValues.Left: tableJustification == "right"? openXmlDoc.TableRowAlignmentValues.Right:0 },
            new openXmlDoc.TableBorders(
            new openXmlDoc.TopBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.None),
                Color = "FFFFFF"
                //Size = Convert.ToUInt32(topValue)
            },
            new openXmlDoc.BottomBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.None),
                Color = "FFFFFF"
                //Size = Convert.ToUInt32(bottomValue)
            },
            new openXmlDoc.LeftBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.None),
                Color = "FFFFFF"
                //Size = Convert.ToUInt32(leftvalue)
            },
            new openXmlDoc.RightBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.None),
                Color = "FFFFFF"
                //Size = Convert.ToUInt32(rightvalue)
            }           
            ));


            return props;
        }

        public static openXmlDoc.TableRowProperties GetTableRowProperties(int topValue, int rightvalue, int bottomValue, int leftvalue, int horizontalValue, int verticalValue)
        {
            openXmlDoc.TableRowProperties props = new openXmlDoc.TableRowProperties(
            new openXmlDoc.TableBorders(
            new openXmlDoc.TopBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                Color = "000000",
                Size = Convert.ToUInt32(topValue)
            },
            new openXmlDoc.BottomBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                Color = "000000",
                Size = Convert.ToUInt32(bottomValue)
            },
            new openXmlDoc.LeftBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                Color = "000000",
                Size = Convert.ToUInt32(leftvalue)
            },
            new openXmlDoc.RightBorder
            {
                Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                Color = "000000",
                Size = Convert.ToUInt32(rightvalue)
            }
            //,
            //new openXmlDoc.InsideHorizontalBorder
            //{
            //    Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.BasicThinLines),
            //    Color = "000000",
            //    Size = Convert.ToUInt32(horizontalValue)
            //},
            //new openXmlDoc.InsideVerticalBorder
            //{
            //    Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.BasicThinLines),
            //    Color = "000000",
            //    Size = Convert.ToUInt32(verticalValue)
            //}
            ));

            return props;
        }

        public static openXmlDoc.ParagraphProperties GetParagraphProp(string mode, string verticalAlign = "center")
        {
            openXmlDoc.ParagraphProperties paraProPerties = new openXmlDoc.ParagraphProperties();
            openXmlDoc.SpacingBetweenLines spacing = new openXmlDoc.SpacingBetweenLines() { Line = "240", LineRule = openXmlDoc.LineSpacingRuleValues.Auto, Before = "50", After = "50", AfterLines = 50, BeforeLines = 50 };
            openXmlDoc.Justification justification = new openXmlDoc.Justification()
            {
                Val = ((mode.ToLower() == "center" ? openXmlDoc.JustificationValues.Center : (mode.ToLower() == "right" ? openXmlDoc.JustificationValues.Right : openXmlDoc.JustificationValues.Left)))
                //Val = openXmlDoc.JustificationValues.Center
            };
            openXmlDoc.TableCellRightMargin cellRightMargin = new openXmlDoc.TableCellRightMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
            openXmlDoc.TableCellLeftMargin cellLeftMargin = new openXmlDoc.TableCellLeftMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
            openXmlDoc.TopMargin topMargin = new openXmlDoc.TopMargin() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = "200" };
            openXmlDoc.BottomMargin bottomMargin = new openXmlDoc.BottomMargin() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = "200" };

            openXmlDoc.TableCellVerticalAlignment verticalalignMent = new openXmlDoc.TableCellVerticalAlignment()
            {
                Val = ((verticalAlign.ToLower() == "center" ? openXmlDoc.TableVerticalAlignmentValues.Center : (verticalAlign.ToLower() == "top" ? openXmlDoc.TableVerticalAlignmentValues.Top : openXmlDoc.TableVerticalAlignmentValues.Bottom)))
            };

            paraProPerties.AppendChild(cellRightMargin);
            paraProPerties.AppendChild(cellLeftMargin);
            paraProPerties.AppendChild(justification);
            paraProPerties.AppendChild(verticalalignMent);
            paraProPerties.AppendChild(topMargin);
            paraProPerties.AppendChild(spacing);
            paraProPerties.AppendChild(bottomMargin);
            return paraProPerties;
        }

        public static openXmlDoc.TableCellProperties GetTableCellProperties(int topValue, int rightvalue, int bottomValue, int leftvalue, int horizontalValue, int verticalValue, string width, string color = "000000", string verticalAlign = "center", string textAlignment = "Center")
        {
            openXmlDoc.TableCellProperties props = new openXmlDoc.TableCellProperties();
            openXmlDoc.TableCellWidth cellWidth = new openXmlDoc.TableCellWidth() { Type = openXmlDoc.TableWidthUnitValues.Pct, Width = width };
            openXmlDoc.TableCellRightMargin cellRightMargin = new openXmlDoc.TableCellRightMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
            openXmlDoc.TableCellLeftMargin cellLeftMargin = new openXmlDoc.TableCellLeftMargin() { Type = openXmlDoc.TableWidthValues.Dxa, Width = 200 };
            //openXmlDoc.TableCell tablecellSpece = new openXmlDoc.TableCellSpacing() { Width = "200" };
            openXmlDoc.TopMargin topMargin = new openXmlDoc.TopMargin() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = "200" };
            openXmlDoc.BottomMargin bottomMargin = new openXmlDoc.BottomMargin() { Type = openXmlDoc.TableWidthUnitValues.Dxa, Width = "200" };
            openXmlDoc.TableCellVerticalAlignment verticalalignMent = new openXmlDoc.TableCellVerticalAlignment()
            {
                Val = (verticalAlign.ToLower() == "center") ? openXmlDoc.TableVerticalAlignmentValues.Center : (verticalAlign.ToLower() == "top" ? openXmlDoc.TableVerticalAlignmentValues.Top : openXmlDoc.TableVerticalAlignmentValues.Bottom),
            };
            openXmlDoc.TextAlignment txtAliignment = new openXmlDoc.TextAlignment()
            {
                Val = (textAlignment.ToLower() == "center") ? openXmlDoc.VerticalTextAlignmentValues.Center : (textAlignment.ToLower() == "top" ? openXmlDoc.VerticalTextAlignmentValues.Top : openXmlDoc.VerticalTextAlignmentValues.Bottom),
            };
            openXmlDoc.Justification justification = new openXmlDoc.Justification()
            {
                Val = openXmlDoc.JustificationValues.Center,
            };


            openXmlDoc.HorizontalAlignmentValues horizantalAlignMent = new openXmlDoc.HorizontalAlignmentValues();


            openXmlDoc.TableCellBorders cellBorders = new openXmlDoc.TableCellBorders(
                                                    new openXmlDoc.TopBorder
                                                    {
                                                        Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                                                        Color = color,
                                                        Size = Convert.ToUInt32(topValue)
                                                    },
                                                    new openXmlDoc.BottomBorder
                                                    {
                                                        Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                                                        Color = color,
                                                        Size = Convert.ToUInt32(bottomValue)
                                                    },
                                                    new openXmlDoc.LeftBorder
                                                    {
                                                        Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                                                        Color = color,
                                                        Size = Convert.ToUInt32(leftvalue)
                                                    },
                                                    new openXmlDoc.RightBorder
                                                    {
                                                        Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                                                        Color = color,
                                                        Size = Convert.ToUInt32(rightvalue)
                                                    }
                                                    ,
                                                    new openXmlDoc.InsideHorizontalBorder
                                                    {
                                                        Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                                                        Size = Convert.ToUInt32(horizontalValue)
                                                    },
                                                    new openXmlDoc.InsideVerticalBorder
                                                    {
                                                        Val = new EnumValue<openXmlDoc.BorderValues>(openXmlDoc.BorderValues.Single),
                                                        Size = Convert.ToUInt32(verticalValue)
                                                    }
                                                    );
            props.AppendChild(cellWidth);
            props.AppendChild(cellRightMargin);
            props.AppendChild(cellLeftMargin);
            //props.AppendChild(tablecellSpece);
            props.AppendChild(topMargin);
            props.AppendChild(bottomMargin);

            props.AppendChild(cellBorders);
            props.AppendChild(verticalalignMent);
            props.AppendChild(txtAliignment);
            props.AppendChild(justification);
            //new openXmlDoc.TableCellLeftMargin { Width = 200 },

            //new openXmlDoc.TableCellMargin
            //{
            //    BottomMargin = new openXmlDoc.BottomMargin { Width = "0" },
            //    TopMargin = new openXmlDoc.TopMargin { Width = "0" },
            //    //RightMargin = new DocumentFormat.OpenXml.Wordprocessing.RightMargin {Width="200" },
            //    LeftMargin = new DocumentFormat.OpenXml.Wordprocessing.LeftMargin { Width = "200" }
            //},

            return props;
        }

        public static string GetCurrentTimeString()
        {
            return (DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString()).ToString();
        }

        public static void AddImage(openXmlDoc.TableCell tableCell, DocumentFormat.OpenXml.Packaging.MainDocumentPart mainpart, string filename, string SigId, Int64 width, Int64 height)
        {

            byte[] imageFileBytes;
            System.Drawing.Bitmap image = null;
            string GraphicDataUri = "http://schemas.openxmlformats.org/drawingml/2006/picture";
            // Open a stream on the image file and read it's contents.
            using (System.IO.FileStream fsImageFile = System.IO.File.OpenRead(filename))
            {
                imageFileBytes = new byte[fsImageFile.Length];
                fsImageFile.Read(imageFileBytes, 0, imageFileBytes.Length);
                image = new System.Drawing.Bitmap(fsImageFile);
            }
            //long imageWidthEMU = (long)((image.Width / image.HorizontalResolution) * 914400L);
            //long imageHeightEMU = (long)((image.Height / image.VerticalResolution) * 914400L);
            long imageWidthEMU = (long)((image.Width / image.HorizontalResolution) * width);
            long imageHeightEMU = (long)((image.Height / image.VerticalResolution) * height);

            // add this is not already there
            try
            {
                if (mainpart.GetPartById(SigId) == null)
                {
                    var imagePart = mainpart.AddNewPart<DocumentFormat.OpenXml.Packaging.ImagePart>("image/jpeg", SigId);

                    using (System.IO.BinaryWriter writer = new System.IO.BinaryWriter(imagePart.GetStream()))
                    {
                        writer.Write(imageFileBytes);
                        writer.Flush();
                    }
                }
            }
            catch // thrown if not found
            {
                var imagePart = mainpart.AddNewPart<DocumentFormat.OpenXml.Packaging.ImagePart>("image/jpeg", SigId);

                using (System.IO.BinaryWriter writer = new System.IO.BinaryWriter(imagePart.GetStream()))
                {
                    writer.Write(imageFileBytes);
                    writer.Flush();
                }

            }
            openXmlDoc.Paragraph para =
            new openXmlDoc.Paragraph(
              new openXmlDoc.Run(
                new openXmlDoc.Drawing(
                  new DW.Inline(

                    new DW.Extent()
                    {
                        Cx = imageWidthEMU,
                        Cy = imageHeightEMU
                    },

                    new DW.EffectExtent()
                    {
                        LeftEdge = 19050L,
                        TopEdge = 0L,
                        RightEdge = 9525L,
                        BottomEdge = 0L
                    },

                    new DW.DocProperties()
                    {
                        Id = (DocumentFormat.OpenXml.UInt32Value)1U,
                        Name = "Inline Text Wrapping Picture",
                        Description = GraphicDataUri
                    },

                    new DW.NonVisualGraphicFrameDrawingProperties(
                      new A.GraphicFrameLocks() { NoChangeAspect = true }),

                    new A.Graphic(
                      new A.GraphicData(
                        new PIC.Picture(

                          new PIC.NonVisualPictureProperties(
                            new PIC.NonVisualDrawingProperties()
                            {
                                Id = (DocumentFormat.OpenXml.UInt32Value)0U,
                                Name = filename
                            },
                            new PIC.NonVisualPictureDrawingProperties()),

                          new PIC.BlipFill(
                            new A.Blip() { Embed = SigId },
                            new A.Stretch(
                              new A.FillRectangle())),

                          new PIC.ShapeProperties(
                            new A.Transform2D(
                              new A.Offset() { X = 0L, Y = 0L },
                              new A.Extents()
                              {
                                  Cx = imageWidthEMU,
                                  Cy = imageHeightEMU
                              }),

                            new A.PresetGeometry(
                              new A.AdjustValueList()
                            )
                            { Preset = A.ShapeTypeValues.Rectangle }))
                      )
                      { Uri = GraphicDataUri })
                  )
                  {
                      DistanceFromTop = (DocumentFormat.OpenXml.UInt32Value)0U,
                      DistanceFromBottom = (DocumentFormat.OpenXml.UInt32Value)0U,
                      DistanceFromLeft = (DocumentFormat.OpenXml.UInt32Value)0U,
                      DistanceFromRight = (DocumentFormat.OpenXml.UInt32Value)0U
                  }))

            );
            tableCell.Append(para);
        }

        public static bool checkUrlPermission(string url, string Mode)
        {
            bool isContain = false;
            HRMS.Entities.ViewModel.UserContextViewModel userContext = new HRMS.Entities.ViewModel.UserContextViewModel();
            userContext = (HRMS.Entities.ViewModel.UserContextViewModel)HttpContext.Current.Session["userContext"];
            DBHelper objDBHelper = new DBHelper();
            List<HRMS.Entities.General.MenuNavigationModel> objMenuNavigationModelList = new List<HRMS.Entities.General.MenuNavigationModel>();
            objMenuNavigationModelList = objDBHelper.GetMenuNavigationList().Where(x => userContext.Permisssion.Contains(x.MainNavigationSubCategoryId) == true).Select(x => x).ToList();
            List<string> innerPageList = new List<string>();
            innerPageList.Add("/Employee");
            foreach (var item in objMenuNavigationModelList)
            {
                if (Mode == "Url")
                    if (item.URL.ToLower().Trim() == url.ToLower().Trim())
                        isContain = true;

                if (Mode == "Module")
                    if (url.ToLower().Trim().Contains(item.URL.ToLower().Trim()))
                        isContain = true;

            }
            foreach (var item in innerPageList)
            {
                foreach (var subNavigation in objMenuNavigationModelList)
                {
                    if (subNavigation.URL.ToLower() == item.ToLower() && url.ToLower().Trim().Contains(item.ToLower()))
                    {
                        isContain = true;
                    }
                }
            }
            return isContain;
        }

        public static PermissionModel CheckPermissionForUser(string Url)
        {
            DBHelper objDBHelper = new DBHelper();
            PermissionModel permissionModel = new PermissionModel();
            HRMS.Entities.ViewModel.UserContextViewModel userContext = (HRMS.Entities.ViewModel.UserContextViewModel)HttpContext.Current.Session["userContext"];
            List<HRMS.Entities.General.MenuNavigationModel> objMenuNavigationModelList = objDBHelper.GetMenuNavigationList().Where(x => userContext.Permisssion.Contains(x.MainNavigationSubCategoryId) == true).Select(x => x).ToList();
            List<int> permissionList = new List<int>();
            List<int> viewOnlyList = new List<int>();
            List<int> addOnlyList = new List<int>();
            List<int> updateOnlyList = new List<int>();
            List<int> deleteOnlyList = new List<int>();
            UserGroupDB objUserGroupDB = new UserGroupDB();

            foreach (var item in objMenuNavigationModelList)
            {
                if (item.URL.ToLower().Equals(Url.ToLower()))
                {
                    permissionModel.ModuleName = item.MainNavigationSubCategoryName;
                    permissionModel.IsMainNavigationPermission = true;
                }
            }

            if (!string.IsNullOrEmpty(userContext.ViewPagesSubCategoryIDs.Trim()))
            {
                viewOnlyList = userContext.ViewPagesSubCategoryIDs.Split(',').Where(x=>!string.IsNullOrEmpty(x)).Select(x => Convert.ToInt32(x)).ToList();
            }
            if (!string.IsNullOrEmpty(userContext.AddPagesSubCategoryIDs.Trim()))
            {
                addOnlyList = userContext.AddPagesSubCategoryIDs.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => Convert.ToInt32(x)).ToList();
            }
            if (!string.IsNullOrEmpty(userContext.UpdatePagesSubCategoryIDs.Trim()))
            {
                updateOnlyList = userContext.UpdatePagesSubCategoryIDs.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => Convert.ToInt32(x)).ToList();
            }
            if (!string.IsNullOrEmpty(userContext.DeletePagesSubCategoryIDs.Trim()))
            {
                deleteOnlyList = userContext.DeletePagesSubCategoryIDs.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => Convert.ToInt32(x)).ToList();
            }
            foreach (var iViewPage in viewOnlyList)
            {
                foreach (var iNavigation in objMenuNavigationModelList.Where(x => x.MainNavigationSubCategoryId == iViewPage))
                {
                    if (iNavigation.MainNavigationSubCategoryHTMLLink.ToLower().Contains(Url.ToLower())|| iNavigation.MainNavigationSubCategoryName.ToLower().Trim()==Url.ToLower().Trim())
                    {
                        permissionModel.IsViewOnlyPermission = true;
                    }
                }
            }
            foreach (var iAddPage in addOnlyList)
            {
                foreach (var iNavigation in objMenuNavigationModelList.Where(x => x.MainNavigationSubCategoryId == iAddPage))
                {
                    if (iNavigation.MainNavigationSubCategoryHTMLLink.ToLower().Contains(Url.ToLower())|| iNavigation.URL.ToLower().Trim()==Url.ToLower().Trim())
                    {
                        permissionModel.IsAddOnlyPermission = true;
                    }
                }
            }
            foreach (var iUpdatePage in updateOnlyList)
            {
                foreach (var iNavigation in objMenuNavigationModelList.Where(x => x.MainNavigationSubCategoryId == iUpdatePage))
                {
                    if (iNavigation.MainNavigationSubCategoryHTMLLink.ToLower().Contains(Url.ToLower()) || iNavigation.URL.ToLower().Trim() == Url.ToLower().Trim())
                    {
                        permissionModel.IsUpdateOnlyPermission = true;
                    }
                }
            }
            foreach (var isdeletePage in deleteOnlyList)
            {
                foreach (var iNavigation in objMenuNavigationModelList.Where(x => x.MainNavigationSubCategoryId == isdeletePage))
                {
                    if (iNavigation.MainNavigationSubCategoryHTMLLink.ToLower().Contains(Url.ToLower()) || iNavigation.URL.ToLower().Trim() == Url.ToLower().Trim())
                    {
                        permissionModel.IsDeleteOnlyPermission = true;
                    }
                }
            }

            return permissionModel;
        }

        public static byte[] GetBlankImage()
        {
            string path = HttpContext.Current.Server.MapPath("~/Content/images/Person-icon-grey.jpg");
            return System.IO.File.ReadAllBytes(path);
        }

        public static bool IsValidImage(byte[] bytes)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(bytes))
                    System.Drawing.Image.FromStream(ms);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        public static PdfPTable GetPdfHeader(DataTable dtNewTable, float pageWidth, bool isLandscape, iTextSharp.text.Image jpg,
           iTextSharp.text.Font Heading, iTextSharp.text.Font Heading1, string Schoolname, float columnHeight)
        {
            string MainHeader = "";
            int count = 0;
            PdfPTable head = new PdfPTable(4);
            head.HeaderRows = 1;
            head.TotalWidth = pageWidth; // page.Width;
            head.LockedWidth = true;
            if (isLandscape)
            {
                float[] widths = new float[] { 20f, 50f, 303f, 217f };  //landscape
                head.SetWidths(widths);
            }
            else
            {
                float[] widths = new float[] { 20f, 50f, 303f, 217f };
                head.SetWidths(widths);
            }

            //Blanck Cell
            PdfPCell c0 = new PdfPCell();
            c0.Border = PdfPCell.NO_BORDER;
            head.AddCell(c0);

            // add image; PdfPCell() overload sizes image to fit cell
            PdfPCell c = new PdfPCell(jpg, true);
            c.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            c.FixedHeight = columnHeight;
            c.Border = PdfPCell.NO_BORDER;
            c.PaddingLeft = 0f;
            c.PaddingTop = 10f;
            c.PaddingBottom = 20f;
            head.AddCell(c);
            // add the header text
            float topPadding = 60f;
            PdfPCell c1 = new PdfPCell(); //Landscape

            foreach (DataRow DR in dtNewTable.Rows)
            {
                if (DR["TopHeader"].ToString() == "1")
                {
                    string HeaderPart = "";
                    for (int k = 0; k < dtNewTable.Columns.Count - 2; k++)
                    {
                        HeaderPart += DR[k].ToString();
                    }
                    HeaderPart = HeaderPart.Trim();
                    if (HeaderPart != "")
                    {
                        topPadding = topPadding - 10f;
                        MainHeader = MainHeader + HeaderPart;
                        Paragraph p = new Paragraph(HeaderPart, count == 0 ? Heading : Heading1);
                        p.Alignment = Element.ALIGN_RIGHT;
                        c1.AddElement(p);
                    }
                }
                count = 1;
            }

            c1.Border = PdfPCell.NO_BORDER;
            c1.FixedHeight = columnHeight;
            c1.PaddingLeft = 10f;
            //  c1.SetLeading(0.0f, 2f);
            c1.VerticalAlignment = Element.ALIGN_TOP;
            c1.HorizontalAlignment = 1;
            c1.UseAscender = true;
            c1.PaddingTop = 10;
            c1.HorizontalAlignment = Element.ALIGN_RIGHT;
            if (MainHeader.Trim() == "")
            {
                c1 = new PdfPCell(new Phrase(Schoolname, Heading)); //Landscape
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape
                c1.PaddingTop = 50F;
                c1.FixedHeight = 100;
                // head.AddCell(c1);
            }
            head.AddCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("", Heading1));
            c2.PaddingTop = 10f;
            c2.PaddingRight = 20f;
            c2.HorizontalAlignment = Element.ALIGN_RIGHT;
            c2.Border = PdfPCell.NO_BORDER;
            head.AddCell(c2);

            return head;
        }

        //public static float CalculateHeaderHeight(DataTable dt, iTextSharp.text.Font Heading1)
        //{
        //    float height = 9;
        //    List<string> columnList = dt.Columns.Cast<DataColumn>().Where(x => x.ColumnName != "active" && x.ColumnName != "TopHeader").Select(x => x.ColumnName).ToList();
        //    PdfPTable tableHeader = new PdfPTable(columnList.Count);
        //    PdfPCell tableCell;
        //    foreach (string iColumn in columnList)
        //    {
        //        tableCell = new PdfPCell(new Phrase(iColumn, Heading1));
        //        tableCell.SetLeading(0.0f, 1f);

        //        tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
        //        tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
        //        tableCell.PaddingBottom = 5f;
        //        tableCell.PaddingTop = 4f;               
        //        tableHeader.AddCell(tableCell);                
        //    }

        //    return height;
        //}

        public static void DeleteFile(string directoryName)
        {
            try
            {
                string DirectoryName = System.Web.HttpContext.Current.Server.MapPath(directoryName);
                DirectoryInfo dir = new DirectoryInfo(DirectoryName);
                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.IsReadOnly = false;
                    fi.Delete();
                }

                foreach (DirectoryInfo di in dir.GetDirectories())
                {
                    ClearFolder(di.FullName);
                    di.Delete();
                }
            }
            catch { }
        }

        public static bool DeleteSingleFile(string fileName)
        {

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);
            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.IsReadOnly = false;
                fi.Delete();
            }
            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                ClearFolder(di.FullName);
                di.Delete();
            }
        }

        public static void CheckAndCreatDirectory(string pathName)
        {
            try
            {
                string DirectoryName = HttpContext.Current.Server.MapPath(pathName);
                if (!Directory.Exists(DirectoryName))
                {
                    Directory.CreateDirectory(DirectoryName);
                }

            }
            catch { }
        }

        public List<LoggerDetail> GetLoggerDetailsList()
        {
            List<LoggerDetail> LoggerList = new List<LoggerDetail>();
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath("~/CommonHelper/XMLFiles/LoggerDetail.xml"));

            //IEnumerable<XElement> selectors = from elements in doc.Elements("LoggerDetail").Elements("ActionUrl")
            //                                  select elements;
            XName companyName = XName.Get("ModuleName");
            ActionUrl actionUrl;
            LoggerDetail loggerDetails;
            foreach (XmlNode node in doc.SelectNodes("Logger")) //Root
            {
                foreach (XmlNode innerNode in node.ChildNodes)
                {
                    loggerDetails = new LoggerDetail();
                    loggerDetails.ModuleName = innerNode.ChildNodes[0].InnerText;
                    loggerDetails.ModuleRefrence = innerNode.ChildNodes[8].InnerText;
                    loggerDetails.ModuleUrl = innerNode.ChildNodes[1].InnerText;
                    loggerDetails.NameOfProperty = innerNode.ChildNodes[2].InnerText;
                    loggerDetails.NoOfParams = innerNode.ChildNodes[7].InnerText != "" ? Convert.ToInt32(innerNode.ChildNodes[7].InnerText) : 1;
                    loggerDetails.DBFileName = innerNode.ChildNodes[5].InnerText;
                    loggerDetails.DBFunctionName = innerNode.ChildNodes[6].InnerText;
                    loggerDetails.ActionUrlList = new List<ActionUrl>();
                    loggerDetails.DefaultParameter = innerNode.ChildNodes[10].InnerText;
                    loggerDetails.FilterParameter = innerNode.ChildNodes[11].InnerText;
                    if (innerNode.ChildNodes[9].ChildNodes.Count > 0)
                    {
                        foreach (XmlNode subNode in innerNode.ChildNodes[9].ChildNodes)
                        {
                            actionUrl = new ActionUrl();
                            actionUrl.ActionName = subNode.ChildNodes[0].InnerText;
                            actionUrl.ModuleName = subNode.ChildNodes[1].InnerText;
                            actionUrl.DirectSave = subNode.ChildNodes[2].InnerText != "" ? Convert.ToBoolean(subNode.ChildNodes[2].InnerText) : false;
                            actionUrl.DirectUpdate = subNode.ChildNodes[3].InnerText != "" ? Convert.ToBoolean(subNode.ChildNodes[3].InnerText) : false;
                            actionUrl.IsDelete = subNode.ChildNodes[4].InnerText != "" ? Convert.ToBoolean(subNode.ChildNodes[4].InnerText) : false;
                            actionUrl.NameOfProperty = subNode.ChildNodes[5].InnerText;
                            actionUrl.DBFileName = subNode.ChildNodes[7].InnerText;
                            actionUrl.DBFunctionName = subNode.ChildNodes[8].InnerText;
                            actionUrl.NoOfParams = subNode.ChildNodes[9].InnerText != "" ? Convert.ToInt32(subNode.ChildNodes[9].InnerText) : 1;
                            actionUrl.FilterParameter = subNode.ChildNodes[10].InnerText;
                            loggerDetails.ActionUrlList.Add(actionUrl);
                        }
                    }
                    LoggerList.Add(loggerDetails);
                }
            }
            return LoggerList;
        }
        public List<LoggerDetail> GetLoggerDetailsList(string xmlLogfile)
        {
            string xmlPath = string.IsNullOrEmpty(xmlLogfile) ? "~/CommonHelper/XMLFiles/LoggerDetail.xml" : "~/CommonHelper/XMLFiles/"+ xmlLogfile;
            //string xmlPath = "~/CommonHelper/XMLFiles/"+ xmlLogfile;
            List<LoggerDetail> LoggerList = new List<LoggerDetail>();
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(xmlPath));

            //IEnumerable<XElement> selectors = from elements in doc.Elements("LoggerDetail").Elements("ActionUrl")
            //                                  select elements;
            XName companyName = XName.Get("ModuleName");
            ActionUrl actionUrl;
            LoggerDetail loggerDetails;
            foreach (XmlNode node in doc.SelectNodes("Logger")) //Root
            {
                foreach (XmlNode innerNode in node.ChildNodes)
                {
                    loggerDetails = new LoggerDetail();
                    loggerDetails.ModuleName = innerNode.ChildNodes[0].InnerText;
                    loggerDetails.ModuleRefrence = innerNode.ChildNodes[8].InnerText;
                    loggerDetails.ModuleUrl = innerNode.ChildNodes[1].InnerText;
                    loggerDetails.NameOfProperty = innerNode.ChildNodes[2].InnerText;
                    loggerDetails.NoOfParams = innerNode.ChildNodes[7].InnerText != "" ? Convert.ToInt32(innerNode.ChildNodes[7].InnerText) : 1;
                    loggerDetails.DBFileName = innerNode.ChildNodes[5].InnerText;
                    loggerDetails.DBFunctionName = innerNode.ChildNodes[6].InnerText;
                    loggerDetails.ActionUrlList = new List<ActionUrl>();
                    loggerDetails.DefaultParameter = innerNode.ChildNodes[10].InnerText;
                    loggerDetails.FilterParameter = innerNode.ChildNodes[11].InnerText;
                    if (innerNode.ChildNodes[9].ChildNodes.Count > 0)
                    {
                        foreach (XmlNode subNode in innerNode.ChildNodes[9].ChildNodes)
                        {
                            actionUrl = new ActionUrl();
                            actionUrl.ActionName = subNode.ChildNodes[0].InnerText;
                            actionUrl.ModuleName = subNode.ChildNodes[1].InnerText;
                            actionUrl.DirectSave = subNode.ChildNodes[2].InnerText != "" ? Convert.ToBoolean(subNode.ChildNodes[2].InnerText) : false;
                            actionUrl.DirectUpdate = subNode.ChildNodes[3].InnerText != "" ? Convert.ToBoolean(subNode.ChildNodes[3].InnerText) : false;
                            actionUrl.IsDelete = subNode.ChildNodes[4].InnerText != "" ? Convert.ToBoolean(subNode.ChildNodes[4].InnerText) : false;
                            actionUrl.NameOfProperty = subNode.ChildNodes[5].InnerText;
                            actionUrl.DBFileName = subNode.ChildNodes[7].InnerText;
                            actionUrl.DBFunctionName = subNode.ChildNodes[8].InnerText;
                            actionUrl.NoOfParams = subNode.ChildNodes[9].InnerText != "" ? Convert.ToInt32(subNode.ChildNodes[9].InnerText) : 1;
                            actionUrl.FilterParameter = subNode.ChildNodes[10].InnerText;
                            loggerDetails.ActionUrlList.Add(actionUrl);
                        }
                    }
                    LoggerList.Add(loggerDetails);
                }
            }
            return LoggerList;
        }
        public static int GetEmployeeId(int Id)
        {
            int employeeId = 0;
            if (Id > 0)
            {
                HttpContext.Current.Session["EmployeeListID"] = Id;
                return Id;
            }
            else
            {
                if (HttpContext.Current.Session["EmployeeListID"] != null)
                {
                    if (int.TryParse(HttpContext.Current.Session["EmployeeListID"].ToString(), out employeeId))
                    {
                        return employeeId;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }

        public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
        {
            MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
            return expressionBody.Member.Name;
        }

        public static string GetHRMSVersion()
        {
            CommonDB objCommonDB = new CommonDB();
            string HRMSVersion = "";
            HRMSVersion = objCommonDB.GetHRMSVersion();
            HttpContext.Current.Session["VesionDate"] = HRMSVersion;
            return HRMSVersion;
        }

        public static Document GenrateDeductionDetailPDF(HttpResponseBase resp, DataSet ds)
        {
            Document pdfDocument;
            pdfDocument = new Document(PageSize.A4, 30f, 20f, 100f, 30f);  //Landscape

            using (PdfWriter wri = PdfWriter.GetInstance(pdfDocument, resp.OutputStream))
            {
                SchoolInfo.cellHeight = pdfDocument.TopMargin;
                SchoolInfo.page = pdfDocument.PageSize;
                SchoolInfo.isHeaderRepeated = false;
                SchoolInfo.pageCount = 0;

                wri.PageEvent = new itextEvents(false);

                pdfDocument.Open();

                FontFactory.RegisterDirectories();
                BaseFont helvMHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseColor CDarkGrey = new BaseColor(128, 128, 128);
                BaseColor Black = new BaseColor(0, 0, 0);
                iTextSharp.text.Font MHeading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
                iTextSharp.text.Font TableContent = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));

                BaseFont helvHead = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 20, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Heading1 = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD, Black));
                iTextSharp.text.Font Resulttext = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, CDarkGrey));
                iTextSharp.text.Font MHeadingWithColor = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(173, 216, 230)));


                #region Header Part
                //School Info
                SchoolInformation sc = new SchoolInformation();
                sc = new SchoolInformationDB().GetBasicSchoolInformation();
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.ReportLogo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);
                float cellHeight = pdfDocument.TopMargin;
                // PDF document size      
                Rectangle page = pdfDocument.PageSize;

                // create two column table
                PdfPTable head = new PdfPTable(3);
                head.TotalWidth = page.Width;
                head.PaddingTop = 10f;
                head.LockedWidth = true;

                float[] widths = new float[] { 180f, 460f, 180f };  //landscape
                head.SetWidths(widths);


                //Blanck Cell
                PdfPCell c0 = new PdfPCell(jpg, true);
                c0.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                c0.Border = PdfPCell.NO_BORDER;
                c0.PaddingTop = 10f;
                c0.FixedHeight = cellHeight;
                head.AddCell(c0);

                PdfPTable SchoolInfoTable = new PdfPTable(1);
                SchoolInfoTable.TotalWidth = page.Width;
                SchoolInfoTable.PaddingTop = 10f;
                SchoolInfoTable.LockedWidth = true;
                float[] widths1 = new float[] { 440f };  //landscape
                SchoolInfoTable.SetWidths(widths1);

                PdfPCell schoolName = new PdfPCell(new Phrase(Schoolname, Heading));
                schoolName.Border = PdfPCell.NO_BORDER;
                schoolName.HorizontalAlignment = Element.ALIGN_CENTER;
                SchoolInfoTable.AddCell(schoolName);


                PdfPCell PoBox = new PdfPCell(new Phrase("P.O.Box:" + sc.POBox + " " + sc.City + " " + sc.Country, MHeading));
                PoBox.Border = PdfPCell.NO_BORDER;
                PoBox.HorizontalAlignment = Element.ALIGN_CENTER;
                SchoolInfoTable.AddCell(PoBox);

                PdfPCell Phone = new PdfPCell(new Phrase("Tel:" + sc.Phone + "/Fax:" + sc.Fax, MHeading));
                Phone.Border = PdfPCell.NO_BORDER;
                Phone.HorizontalAlignment = Element.ALIGN_CENTER;
                SchoolInfoTable.AddCell(Phone);


                PdfPCell c1 = new PdfPCell(); //Landscape
                c1.Border = PdfPCell.NO_BORDER;
                c1.HorizontalAlignment = Element.ALIGN_CENTER; //landscape

                c1.PaddingTop = 10F;
                c1.FixedHeight = cellHeight;
                c1.AddElement(SchoolInfoTable);
                head.AddCell(c1);


                PdfPCell c2 = new PdfPCell(new Phrase("", DateFont));
                c2.PaddingTop = 10f;
                c2.PaddingRight = 20f;
                c2.HorizontalAlignment = Element.ALIGN_RIGHT;
                c2.Border = PdfPCell.NO_BORDER;
                head.AddCell(c2);


                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  page.Height - cellHeight + head.TotalHeight,
                  wri.DirectContent
                );
                //

                #endregion

                string fontpath = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\Arial.ttf";
                BaseFont basefont = BaseFont.CreateFont(fontpath, BaseFont.IDENTITY_H, true);
                iTextSharp.text.Font arabicFont = new iTextSharp.text.Font(basefont, 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));

                var el = new Chunk();
                iTextSharp.text.Font f2 = new iTextSharp.text.Font(basefont, el.Font.Size,
                                                el.Font.Style, el.Font.Color);
                el.Font = f2;
                pdfDocument.SetMargins(30f, 20f, 20f, 30f);

                PdfPTable BlankTable = new PdfPTable(1);
                BlankTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                BlankTable.TotalWidth = page.Width;
                BlankTable.WidthPercentage = 95;

                PdfPCell Blankcell = new PdfPCell(new Phrase("", MHeading));
                Blankcell.FixedHeight = 20f;
                Blankcell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                Blankcell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                Blankcell.Border = PdfPCell.NO_BORDER;
                BlankTable.AddCell(Blankcell);
                pdfDocument.Add(BlankTable);

                PdfPTable ApplicantProfileLabel = new PdfPTable(1);
                ApplicantProfileLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                ApplicantProfileLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                //ApplicantProfileLabel.TotalWidth = page.Width;
                ApplicantProfileLabel.WidthPercentage = 98;

                PdfPCell ApplicantProfileCell = new PdfPCell(new Phrase("Deduction Details", MHeading));
                ApplicantProfileCell.FixedHeight = 19f;
                ApplicantProfileCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                ApplicantProfileCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                ApplicantProfileCell.Border = PdfPCell.NO_BORDER;
                ApplicantProfileCell.PaddingBottom = 3f;
                ApplicantProfileCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                ApplicantProfileLabel.AddCell(ApplicantProfileCell);
                pdfDocument.Add(ApplicantProfileLabel);

                //Basic Information
                PdfPTable pTable = new PdfPTable(1);
                float[] pTableWidths = { 842 };
                pTable.SetWidths(pTableWidths);
                pTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                pTable.WidthPercentage = 98;
                int columnCount = 1;
                #region Personal Information
                if (ds.Tables.Count >= 1)
                {
                    PdfPTable empPersonalIdentityLabel = new PdfPTable(1);
                    empPersonalIdentityLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    empPersonalIdentityLabel.TotalWidth = page.Width;
                    empPersonalIdentityLabel.WidthPercentage = 95;
                    empPersonalIdentityLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                    PdfPCell empPersonalIdentityCell = new PdfPCell(new Phrase("Personal Information", MHeading));
                    empPersonalIdentityCell.PaddingLeft = 0f;
                    empPersonalIdentityCell.FixedHeight = 30f;
                    empPersonalIdentityCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    empPersonalIdentityCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    empPersonalIdentityCell.Border = PdfPCell.NO_BORDER;
                    empPersonalIdentityLabel.AddCell(empPersonalIdentityCell);
                    pdfDocument.Add(empPersonalIdentityLabel);

                    PdfPTable PersonalIdentityTable = new PdfPTable(4);
                    PersonalIdentityTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                    // PersonalIdentityTable.TotalWidth = 600f;
                    PersonalIdentityTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    // PersonalIdentityTable.WidthPercentage = 80;

                    foreach (DataRow DR in ds.Tables[0].Rows)
                    {
                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                        {
                            Paragraph para = new Paragraph(DR[i].ToString(), TableContent);
                            para.SetLeading(0, 1);

                            PdfPCell cell = new PdfPCell();
                            cell.MinimumHeight = 23;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell.PaddingBottom = 5;
                            cell.AddElement(para);
                            if (i % 2 == 0)
                            {
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                            }
                            PersonalIdentityTable.AddCell(cell);
                            columnCount++;
                        }
                        if (columnCount % 4 == 0)
                        {
                            PersonalIdentityTable.CompleteRow();
                        }
                    }
                    PersonalIdentityTable.Complete = true;
                    pTable.AddCell(new PdfPCell(PersonalIdentityTable) { Border = PdfPCell.NO_BORDER });
                    pdfDocument.Add(pTable);
                }
                #endregion
                //
                //Loan/Fees Basic Information                             
                PdfPTable bTable = new PdfPTable(1);
                float[] bTableWidths = { 842 };
                bTable.SetWidths(bTableWidths);
                bTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                bTable.WidthPercentage = 98;
                columnCount = 1;
                #region Basic Information
                if (ds.Tables.Count >= 2)
                {
                    int totalRecordBInfo = 5;
                    int countOfBInfo = ds.Tables[1].Rows.Count / totalRecordBInfo;
                    for (int m = 0; m < countOfBInfo; m++)
                    {
                        columnCount = 1;
                        PdfPTable empLoanFeesIdentityLabel = new PdfPTable(1);
                        empLoanFeesIdentityLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        empLoanFeesIdentityLabel.TotalWidth = page.Width;
                        empLoanFeesIdentityLabel.WidthPercentage = 98.4f;
                        empLoanFeesIdentityLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        empLoanFeesIdentityLabel.DefaultCell.Border = Rectangle.NO_BORDER;

                        PdfPCell empLoanFeesIdentityCell = new PdfPCell(new Phrase("Deduction Basic Information", MHeading));
                        empLoanFeesIdentityCell.PaddingLeft = 0f;
                        empLoanFeesIdentityCell.FixedHeight = 30f;
                        empLoanFeesIdentityCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                        empLoanFeesIdentityCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        empLoanFeesIdentityCell.Border = PdfPCell.NO_BORDER;
                        empLoanFeesIdentityLabel.AddCell(empLoanFeesIdentityCell);

                        PdfPTable AssignmentDetailTable = new PdfPTable(ds.Tables[1].Columns.Count - 1);
                        AssignmentDetailTable.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                        AssignmentDetailTable.TotalWidth = page.Width;
                        AssignmentDetailTable.WidthPercentage = 98;
                        AssignmentDetailTable.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                        for (int j = 0; j < totalRecordBInfo; j++)
                        {
                            DataRow DR = ds.Tables[1].Rows[j];
                            for (int i = 0; i < ds.Tables[1].Columns.Count - 1; i++)
                            {
                                PdfPCell tableCell;
                                tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                                tableCell.SetLeading(0.0f, 1.5f);
                                tableCell.MinimumHeight = 23;
                                tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                                tableCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                tableCell.PaddingBottom = 5f;
                                tableCell.PaddingTop = 1f;
                                if (i % 2 == 0)
                                {
                                    tableCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                                }
                                AssignmentDetailTable.AddCell(tableCell);

                            }
                        }
                        AssignmentDetailTable.SpacingBefore = 0f;
                        empLoanFeesIdentityLabel.DefaultCell.PaddingLeft = 0f;
                        empLoanFeesIdentityLabel.AddCell(AssignmentDetailTable);
                        empLoanFeesIdentityLabel.KeepTogether = true;
                        pdfDocument.Add(empLoanFeesIdentityLabel);

                        int PayDeductionID = 0;
                        for (int p = 0; p < totalRecordBInfo; p++)
                        {
                            DataRow DR = ds.Tables[1].Rows[0];
                            PayDeductionID = Convert.ToInt32(DR[2].ToString());
                            ds.Tables[1].Rows.Remove(DR);
                        }
                        #region Payment Details
                        if (ds.Tables.Count >= 3)
                        {
                            if (ds.Tables[2].Rows.Count == 0)
                            {
                                DataRow toInsert = ds.Tables[2].NewRow();
                                ds.Tables[2].Rows.InsertAt(toInsert, 0);
                            }
                            PdfPTable empPaymentDetailLabel = new PdfPTable(1);
                            empPaymentDetailLabel.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                            empPaymentDetailLabel.TotalWidth = page.Width;
                            empPaymentDetailLabel.WidthPercentage = 98;
                            empPaymentDetailLabel.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            empPaymentDetailLabel.DefaultCell.Border = Rectangle.NO_BORDER;

                            PdfPCell empPaymentDetailCell = new PdfPCell(new Phrase("Installments Payment Detail", MHeading));
                            empPaymentDetailCell.PaddingLeft = 0f;
                            empPaymentDetailCell.FixedHeight = 30f;
                            empPaymentDetailCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                            empPaymentDetailCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            empPaymentDetailCell.Border = PdfPCell.NO_BORDER;
                            empPaymentDetailLabel.AddCell(empPaymentDetailCell);
                            pdfDocument.Add(empPaymentDetailLabel);

                            PdfPTable AssignmentDetailTable1 = new PdfPTable(ds.Tables[2].Columns.Count - 1);
                            AssignmentDetailTable1.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                            AssignmentDetailTable1.TotalWidth = page.Width;
                            AssignmentDetailTable1.WidthPercentage = 98;
                            AssignmentDetailTable1.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                            foreach (DataColumn dc in ds.Tables[2].Columns)
                            {
                                if (dc.ColumnName.ToString() != ds.Tables[2].Columns[ds.Tables[2].Columns.Count - 1].ColumnName.ToString())
                                {
                                    Paragraph p = new Paragraph(dc.ToString(), TableContent);
                                    p.Alignment = Element.ALIGN_CENTER;

                                    PdfPCell tableColumnCell = new PdfPCell();
                                    tableColumnCell.MinimumHeight = 23;
                                    tableColumnCell.AddElement(p);
                                    tableColumnCell.PaddingBottom = 10f;
                                    tableColumnCell.BackgroundColor = new iTextSharp.text.BaseColor(173, 216, 230);
                                    AssignmentDetailTable1.AddCell(tableColumnCell);
                                }
                            }
                            columnCount = 1;
                            int chkPayDeductionID = 0;
                            int recordCntPerDeduction = 0;
                            foreach (DataRow DR in ds.Tables[2].Rows)
                            {
                                chkPayDeductionID = Convert.ToInt32(DR[ds.Tables[2].Columns.Count - 1].ToString());
                                if (PayDeductionID == chkPayDeductionID)
                                {
                                    for (int i = 0; i < ds.Tables[2].Columns.Count - 1; i++)
                                    {
                                        PdfPCell tableCell;
                                        tableCell = new PdfPCell(new Phrase(DR[i].ToString(), TableContent));
                                        tableCell.SetLeading(0.0f, 1.5f);
                                        tableCell.MinimumHeight = 23;
                                        tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                        tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                        tableCell.PaddingBottom = 5f;
                                        tableCell.PaddingTop = 1f;
                                        AssignmentDetailTable1.AddCell(tableCell);
                                    }
                                    recordCntPerDeduction++;
                                }
                            }
                            if (recordCntPerDeduction == 0)
                            {
                                for (int i = 0; i < ds.Tables[2].Columns.Count - 1; i++)
                                {
                                    PdfPCell tableCell;
                                    tableCell = new PdfPCell(new Phrase("", TableContent));
                                    tableCell.SetLeading(0.0f, 1.5f);
                                    tableCell.MinimumHeight = 23;
                                    tableCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                    tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    tableCell.PaddingBottom = 5f;
                                    tableCell.PaddingTop = 1f;
                                    AssignmentDetailTable1.AddCell(tableCell);

                                }
                                recordCntPerDeduction = 0;
                            }
                            pdfDocument.Add(AssignmentDetailTable1);
                        }
                        #endregion
                    }
                }
                #endregion
                //                              

                pdfDocument.Close();
            }
            return pdfDocument;
        }

        public static bool IsEnglish(string inputText)
        {
            Regex regex = new Regex(@"[A-Za-z0-9 .,-=+(){}\[\]\\]");
            MatchCollection matches = regex.Matches(inputText);
            if (matches.Count.Equals(inputText.Length))
                return true;
            else
                return false;
        }
        /// <summary>
        /// Convert Uploaded Files to byte array and returns
        /// </summary>
        /// <param name="file"></param>
        /// <returns>byte[]</returns>
        public static byte[] ConvertToByte(HttpPostedFileBase file)
        {
            byte[] byteDoc;
            using (var binaryReader = new BinaryReader(file.InputStream))
                byteDoc = binaryReader.ReadBytes(file.ContentLength);
            return byteDoc;

        }
        public static HrSettings GetHrSettings()
        {
            CommonDB objCommonDB = new CommonDB();
            return objCommonDB.GetHrSettings();
        }

    }

    public static class AssemblyExtension
    {
        public static string FindCurrentAssemblyVersion()
        {
            return Assembly.GetExecutingAssembly().GetLinkerTime();
        }

        public static string GetLinkerTime(this Assembly assembly)
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(assembly.Location);
            DateTime lastModified = fileInfo.LastWriteTime;
            CommonDB objCommonDB = new CommonDB();
            HttpContext.Current.Session["VesionDate"] = objCommonDB.GetHRMSVersion();
            return lastModified.ToString("ddMMyyyy");
        }

    }
  
    public static class ConvertListToDataTable
    {
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }

    enum UserRole
    {
        Admin = 1,
        Employee = 2
    }
    enum AbsentStatus
    {
        Absent = 2,
        LateIn = 3,
        EarlyOut = 4
    }
    public class itextEvents : IPdfPageEvent
    {
        public static bool isLandscape;
        public itextEvents()
        {
            isLandscape = true;
        }
        public itextEvents(bool IsLandscape)
        {
            isLandscape = IsLandscape;
        }
        //Create object of PdfContentByte
        PdfContentByte pdfContent;

        public void OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title)
        {
            throw new NotImplementedException();
        }

        public void OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnCloseDocument(PdfWriter writer, Document document)
        {
            //  throw new NotImplementedException();  


        }

        public void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            //School Info
            iTextSharp.text.Font Heading = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
            iTextSharp.text.Font Heading1 = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
            iTextSharp.text.Font DateFont = new iTextSharp.text.Font(FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0)));
            var userContext = (HRMS.Entities.ViewModel.UserContextViewModel)HttpContext.Current.Session["userContext"];
            List<string> columnList = new List<string>();
            if (SchoolInfo.isHeaderRepeated)
            {
                SchoolInformation sc = SchoolInfo.schoolInfo;
                string Schoolname = sc.SchoolName_1;
                if (!string.IsNullOrEmpty(sc.CampusName_1))
                {
                    Schoolname += " " + "-" + " " + sc.CampusName_1;
                }
                string ImageFilePath = System.Web.HttpContext.Current.Server.MapPath(sc.Logo);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ImageFilePath);

                PdfPTable head = CommonHelper.GetPdfHeader(SchoolInfo.ds.Tables[0], SchoolInfo.page.Width, true, jpg, Heading, Heading1, Schoolname, 150);
                head.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  0,      // left offset
                          // ** bottom** yPos of the table
                  SchoolInfo.page.Height - SchoolInfo.cellHeight + head.TotalHeight,
                  writer.DirectContent
                );

                PdfContentByte cb = writer.DirectContent;
                cb.SetLineWidth(1.5f);
                cb.SetColorStroke(new CMYKColor(0, 0, 0, 100f));
                cb.MoveTo(20, 500);
                cb.LineTo(530, 500);
                cb.Stroke();

                cb.SetLineWidth(1.5f);
                cb.MoveTo(750, 500);
                cb.LineTo(822, 500);
                cb.Stroke();

                PdfPCell WPSReportName = new PdfPCell(new Phrase(CommonHelper.tempMessage, Heading));
                WPSReportName.Border = PdfPCell.NO_BORDER;
                WPSReportName.HorizontalAlignment = Element.ALIGN_CENTER;
                WPSReportName.VerticalAlignment = Element.ALIGN_MIDDLE;
                PdfPTable table = new PdfPTable(1);
                table.TotalWidth = 220f;
                table.AddCell(WPSReportName);
                table.WriteSelectedRows(0, -1, 520, 510, cb);

                //dt.Columns.Remove("active");
                //dt.Columns.Remove("TopHeader");
                PdfPTable tableHeader = new PdfPTable(SchoolInfo.ds.Tables[0].Columns.Count - 2);
                tableHeader.TotalWidth = SchoolInfo.page.Width - 42;
                PdfPCell tableCell;
                columnList = SchoolInfo.ds.Tables[0].Columns.Cast<DataColumn>().Where(x => x.ColumnName != "active" && x.ColumnName != "TopHeader").Select(x => x.ColumnName).ToList();

                foreach (string iColumn in columnList)
                {
                    tableCell = new PdfPCell(new Phrase(iColumn, Heading1));
                    tableCell.SetLeading(0.0f, 1f);
                    tableCell.MinimumHeight = 27;
                    tableCell.FixedHeight = 27;
                    tableCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    tableCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    tableCell.PaddingBottom = 5f;
                    tableCell.PaddingTop = 4f;
                    tableHeader.AddCell(tableCell);
                }
                var cellHeight = tableHeader.Rows.FirstOrDefault().MaxHeights;
                tableHeader.WriteSelectedRows(
                  0, -1,  // first/last row; -1 flags all write all rows
                  21,      // left offset
                           // ** bottom** yPos of the table
                  472,
                  writer.DirectContent
                );
            }

            PdfPTable footer = new PdfPTable(3);
            footer.HeaderRows = 1;
            footer.TotalWidth = SchoolInfo.page.Width;
            //float footerColumnWidth = SchoolInfo.page.Width /3;
            footer.LockedWidth = true;

            PdfPCell footerCell1 = new PdfPCell(new Phrase(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"), DateFont));
            footerCell1.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            footerCell1.FixedHeight = SchoolInfo.cellHeight;
            footerCell1.Border = PdfPCell.NO_BORDER;
            footerCell1.PaddingLeft = 0f;
            footerCell1.PaddingTop = 0f;
            footerCell1.PaddingBottom = 0f;
            footer.AddCell(footerCell1);

            PdfPCell footerCell2 = new PdfPCell(new Phrase("Printed By: " + userContext.UserRolls.FirstOrDefault(), DateFont));
            if (!isLandscape)
            {
                footerCell2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            }
            else
            {
                footerCell2.HorizontalAlignment = PdfPCell.ALIGN_MIDDLE;
                footerCell2.PaddingLeft = 100f;
            }
            footerCell2.FixedHeight = SchoolInfo.cellHeight;
            footerCell2.Border = PdfPCell.NO_BORDER;
            footerCell2.PaddingTop = 0f;
            footerCell2.PaddingBottom = 0f;
            footer.AddCell(footerCell2);

            PdfPCell footerCell3 = new PdfPCell(new Phrase("Page No. " + (++SchoolInfo.pageCount).ToString(), DateFont));
            var id = footerCell3.ID;
            footerCell3.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
            footerCell3.FixedHeight = SchoolInfo.cellHeight;
            footerCell3.Border = PdfPCell.NO_BORDER;
            footerCell3.PaddingLeft = 0f;
            footerCell3.PaddingTop = 0f;
            footerCell3.PaddingBottom = 0f;
            footerCell3.PaddingRight = 40f;
            footer.AddCell(footerCell3);
            footer.WriteSelectedRows(0, -1, 20, document.Bottom, writer.DirectContent);

            //set pdfContent value
            pdfContent = writer.DirectContent;
            //Move the pointer and draw line to separate header section from rest of page
            pdfContent.MoveTo(30, document.PageSize.Height - 35);
            //  pdfContent.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 35);
            pdfContent.Stroke();
        }

        public void OnGenericTag(PdfWriter writer, Document document, Rectangle rect, string text)
        {
            throw new NotImplementedException();
        }

        public void OnOpenDocument(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

        public void OnParagraph(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title)
        {
            throw new NotImplementedException();
        }

        public void OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            throw new NotImplementedException();
        }

        public void OnStartPage(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }
    }
    public static class SchoolInfo
    {
        public static DataSet ds;
        public static float cellHeight;
        public static Rectangle page;
        public static SchoolInformation schoolInfo;
        public static bool isHeaderRepeated;
        public static int pageCount;
        public static float difference;
    }

    public static class GlobalSetting
    {
        public static bool SessionExprire;
    }
    public class SettingsHelper
    {
        public static string ClientId
        {
            get { return ConfigurationManager.AppSettings["Azure:ClientID"]; }
        }

        public static string ClientSecret
        {
            get { return ConfigurationManager.AppSettings["Azure:ClientSecret"]; }
        }
        public static string AppUrl
        {
            get { return ConfigurationManager.AppSettings["AppUrl"]; }
        }
        public static string AzureLoginUrl
        {
            get { return "https://login.microsoftonline.com/common"; }
        }

        public static string O365UnifiedResource
        {
            get { return "https://graph.microsoft.com/"; }
        }
        public static string AzureLogoutUrl
        {
            get { return "https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=" + ConfigurationManager.AppSettings["AppUrl"]; }
        }
        public static string HRMSDateFormat
        {
            get { return ConfigurationManager.AppSettings["HRMSDateFormat"]; }
        }
    }

}