﻿using HRMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMS.Web.CommonHelper
{
    public class SessionManager
    {
        private static object Get(string sessionKey)
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session[sessionKey] != null)
                return HttpContext.Current.Session[sessionKey];
            return null;
        }

        private static void Set(string sessionKey, object value)
        {
            HttpContext.Current.Session[sessionKey] = value;
        }
        /// <summary>
        /// Event search session field
        /// </summary>
        public static DevelopmentGoalSessionModel DevelopmentGoalModel
        {
            get
            {
                if (HttpContext.Current.Session["DevelopmentGoalModel"] != null)
                    return HttpContext.Current.Session["DevelopmentGoalModel"] as DevelopmentGoalSessionModel;
                return new DevelopmentGoalSessionModel();
            }
            set
            {
                System.Web.HttpContext.Current.Session["DevelopmentGoalModel"] = value;
            }
        }


    }
}