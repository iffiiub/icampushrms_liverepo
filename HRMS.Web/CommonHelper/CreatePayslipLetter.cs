﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Collections;
using HRMS.DataAccess;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Web;
using HRMS.Web.Controllers;

namespace HRMS.Web
{
    public class CreatePayslipLetter :BaseController
    {
        DataSet _PayslipData, _teacherList, _Salary, _Addition, _Deduction, _Summary, _PayslipSummary;

        string letter = "";

//        public void Create(string CycleID, int TransMode, string WhereID, string _Filepath)
//        {

//            string destinationFile = "";
//            PayrollDB _Payrollbusiness = new PayrollDB();
//            _PayslipData = _Payrollbusiness.GetPaySlipData_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
//            _teacherList = _Payrollbusiness.GetTeachers_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
//            _PayslipSummary = _Payrollbusiness.GetPaySlipSummary_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
//            string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";

//            for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
//            {
//                _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";

//                _Salary = new DataSet();
//                _Addition = new DataSet();
//                _Deduction = new DataSet();
//                _Summary = new DataSet();

//                _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
//                _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
//                _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
//                _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));

//                _employeeid = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
//                _name = _Salary.Tables[0].Rows[0]["name"].ToString();
//                _cyclename = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
//                _paydate = _Summary.Tables[0].Rows[0]["paydate"].ToString();
//                _datefrom = _Summary.Tables[0].Rows[0]["datefrom"].ToString();
//                _dateto = _Summary.Tables[0].Rows[0]["dateto"].ToString();
//                _department = _Salary.Tables[0].Rows[0]["department"].ToString();
//                _accountnumber = _Salary.Tables[0].Rows[0]["accountnumber"].ToString();
//                _bank = _Salary.Tables[0].Rows[0]["bank"].ToString();


//                string fileName = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/PaySlip-" + _employeeid + "-" + _name + "-" + _cyclename + ".docx");

//                string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/Pay-slip-template.docx");
//              //   destinationFile = _Filepath; //System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/testdoc.docx");
//                destinationFile = fileName;
//                // Create a copy of the template file and open the copy 
//               System.IO.File.Copy(sourceFile, destinationFile, true);

//                // create key value pair, key represents words to be replace and 
//                //values represent values in document in place of keys.
//                Dictionary<string, string> keyValues = new Dictionary<string, string>();
//                keyValues.Add("paymentdate", _paydate);
//                keyValues.Add("payfrom", _datefrom);
//                keyValues.Add("payto", _dateto);
//                keyValues.Add("employeeid", _employeeid);
//                keyValues.Add("department", _department);
//                keyValues.Add("bankname", _bank);
//                keyValues.Add("inservicesince", "");
//                keyValues.Add("employeename", _name);
//                keyValues.Add("position", "");
//                keyValues.Add("accountnumber", _accountnumber);
//                keyValues.Add("remainingoffdays", "");

//                //Remuneration
//                try
//                {
//                    if (_Salary.Tables[0].Rows.Count > 0)
//                    {
//                        for (int ii = 1; ii <= _Salary.Tables[0].Rows.Count; ii++)
//                        {
//                            keyValues.Add("rem" + ii + "name", _Salary.Tables[0].Rows[ii]["itemDescr"].ToString());
//                            keyValues.Add("rem" + ii + "no", _Salary.Tables[0].Rows[ii]["amount"].ToString());
//                        }
//                    }
//                }
//                catch { }

//                //Addition
//                try
//                {
//                    if (_Addition.Tables[0].Rows.Count > 0)
//                    {
//                        for (int iii = 0; iii < _Addition.Tables[0].Rows.Count; iii++)
//                        {
//                            keyValues.Add("add" + iii + "name", _Addition.Tables[0].Rows[iii]["itemDescr"].ToString());
//                            keyValues.Add("add" + iii + "no", _Addition.Tables[0].Rows[iii]["amount"].ToString());
//                        }
//                    }
//                }
//                catch { }

//                //Deduction
//                try
//                {
//                    if (_Deduction.Tables[0].Rows.Count > 0)
//                    {
//                        for (int iiii = 0; iiii < _Deduction.Tables[0].Rows.Count; iiii++)
//                        {
//                            keyValues.Add("ded" + iiii + "name", _Deduction.Tables[0].Rows[iiii]["itemDescr"].ToString());
//                            keyValues.Add("ded" + iiii + "no", _Deduction.Tables[0].Rows[iiii]["amount"].ToString());
//                        }
//                    }
//                }
//                catch { }

//                //Totals
//                try
//                {
//                    if (_Summary.Tables[0].Rows.Count > 0)
//                    {
//                        keyValues.Add("remtotal", _Summary.Tables[0].Rows[0]["salaryTotal"].ToString());
//                        keyValues.Add("addtotal", _Summary.Tables[0].Rows[0]["additionTotal"].ToString());
//                        keyValues.Add("dedtotal", _Summary.Tables[0].Rows[0]["deductionTotal"].ToString());
//                        keyValues.Add("netsalary", _Summary.Tables[0].Rows[0]["net"].ToString());

//                    }
//                }
//                catch { }

//                SearchAndReplace(destinationFile, keyValues);

//                _Salary.Dispose();
//                _Addition.Dispose();
//                _Deduction.Dispose();
//                _Summary.Dispose();

//                HttpResponse response = HttpContext.Current.Response;
//                response.Clear();
//                response.ClearContent();
//                response.ClearHeaders();
//                response.Buffer = true;
//                response.ContentType = "application/msword";
//                response.AddHeader("content-disposition", "attachment;filename='" + destinationFile + "'");
//                response.Cache.SetCacheability(HttpCacheability.NoCache);

//                response.Write(destinationFile);
//                response.End();
               
//            }

//          // Process.Start(destinationFile);

//            //HttpResponse.
                
//            //    ( destinationFile);
//            //Response.ContentType = "application/msword";
//            //Response.AddHeader("content-disposition", "attachment;filename='PaySlip-" + _employeeid + "-" + _name + "-" + _cyclename + "'");
//            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
//            //Response.Write(destinationFile);
//            //Response.End();


          
           
         
////            Response.ContentType = "text/plain";
////Response.OutputStream.Write(buffer, 0, buffer.Length);
////Response.AddHeader("Content-Disposition", "attachment;filename=yourfile.txt");
////This of course works if you want to write a text file. In case you want to write a .doc for example you change the ContentType to "application/msword" etc...
        
//        }



        public void Create1(string CycleID, int TransMode, string WhereID, string _Filepath)
        {
            TextWriter _tw = new StreamWriter(_Filepath);
            _tw.Write("");

            PayrollDB _Payrollbusiness = new PayrollDB();
            _PayslipData = _Payrollbusiness.GetPaySlipData_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
            _teacherList = _Payrollbusiness.GetTeachers_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
            _PayslipSummary = _Payrollbusiness.GetPaySlipSummary_DataSet(CycleID.ToString(), TransMode, WhereID.ToString());
            string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";

            for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
            {
                _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";

                _Salary = new DataSet();
                _Addition = new DataSet();
                _Deduction = new DataSet();
                _Summary = new DataSet();

                _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
                _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
                _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
                _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));

                _employeeid = _Salary.Tables[0].Rows[0]["employeeid"].ToString();
                _name = _Salary.Tables[0].Rows[0]["name"].ToString();
                _cyclename = _Summary.Tables[0].Rows[0]["cyclename"].ToString();
                _paydate = _Summary.Tables[0].Rows[0]["paydate"].ToString();
                _datefrom = _Summary.Tables[0].Rows[0]["datefrom"].ToString();
                _dateto = _Summary.Tables[0].Rows[0]["dateto"].ToString();
                _department = _Salary.Tables[0].Rows[0]["department"].ToString();
                _accountnumber = _Salary.Tables[0].Rows[0]["accountnumber"].ToString();
                _bank = _Salary.Tables[0].Rows[0]["bank"].ToString();

                //
                //letter += "<div><div style='float:left;'><img src='../Content/DocMawakebLogo.png' style='width:100px;height:100px;' /></div><div style='float:right;'>Al Mawakeb School - Al Barsha</div></div>";

                string logoFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Content/images/DocMawakebLogo.png");

                //letter += "<table border='0' cellpadding='0' cellspacing='0' style='width: 950px; border:none;'><tr><td>";//main table

                ////header
                //letter += "<table border='0' cellpadding='2' cellspacing='2' style='font-size: 9px; vertical-align: middle;"
                //      + "width: 100%; font-family: Arial; text-align: center; border:none;'>";
                //letter += "<tr><td rowspan='3' style='width:150px;vertical-align:top;'><img src='" + logoFilePath + "' style='width:100px;height:100px;' /></td><td style='width:500px;'><span style='font-weight:bold; font-size:18px;'>Al Mawakeb School - Al Barsha</span><br>Al Barsha Dubai UAE P.O.Box: 35001<br><div style='text-align:right;'>Phone : +971 4 3478288<br>Fax : +971 4 3478077</div></td></tr></table>";

                //letter += "<table border='0' cellpadding='2' cellspacing='2' style='font-size: 9px; vertical-align: middle;"
                //      + "width: 100%; font-family: Arial; text-align: center; border:none;'>";
                //letter += "<tr><td>E-mail: info@almawakeb.sch.ae<br>Website: www.almawakeb.sch.ae<br><span style='font-weight:bold; font-size:18px;'>September 2015 PAY SLIP</span><div style='text-align:right;'><span style='font-weight:bold; font-size:18px;'>Date of payment:<br>Pay period:</span></div></tr>";
                //letter += "</table>";

                //letter += "</td></tr></table>";//main table

                //-------------------------------------------------------------------------------------
               
                letter += "   <br /> "
                    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 950px'>"
                    + "<tr>"
                    + "<td style='background-color: whitesmoke'>"
                  + "<table border='0' cellpadding='0' cellspacing='5' style='font-size: 9pt; vertical-align: middle;"
                      + "width: 100%; font-family: Arial; text-align: center'>"
                      + "<tr>"
                          + "<td>"
                              + "Number</td>"
                          + "<td>"
                              + "Name</td>"
                          + "<td>"
                              + "PayCycle No.</td>"
                          + "<td>"
                              + "Pay date</td>"
                          + "<td>"
                              + "From</td>"
                          + "<td>"
                              + "To</td>"
                          + "<td>"
                              + "Slip No.</td>"
                          + "<td>"
                              + "Department</td>"
                          + "<td rowspan='2' style='border-right: black 1px solid; border-top: black 1px solid;"
                              + "border-left: black 1px solid; border-bottom: black 1px solid; background-color: white; text-align: left;'>"
                              + " " + _Summary.Tables[0].Rows[0]["schoolname"].ToString() + " </td>"
                      + "</tr>"
                      + "<tr>"
                          + "<td style='background-color: white'>"
                              + "" + _employeeid + "</td>"
                          + "<td style='background-color: white'>"
                              + "" + _name + "</td>"
                          + "<td style='background-color: white'>"
                              + " " + _cyclename + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _paydate + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _datefrom + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _dateto + " </td>"
                          + "<td style='background-color: white'>"
                              + "</td>"
                          + "<td style='background-color: white'>"
                              + "" + _department + "</td>"
                      + "</tr>"
                  + "</table>"
                    + "</td>"
                    + "</tr>"
                    + "<tr style='font-size: 12pt'>"
                    + "<td>"
                  + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; border-right: black 2px solid; border-top: black 2px solid; border-left: black 2px solid; border-bottom: black 2px solid;'>"
                      + "<tr>"
                          + "<td style='vertical-align: top; height: 150px; border-top-width: 1px; border-left-width: 1px; border-left-color: black; border-bottom-width: 1px; border-bottom-color: black; border-top-color: black; border-right-width: 1px; border-right-color: black;'>"
                              + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
                                  + "<tr>"
                                      + "<td style='height: 20px; background-color: lightgrey'>"
                                          + "<div align='center' style='text-align: center'>"
                                              + "Remuneration</div>"
                                      + "</td>"
                                  + "</tr>"
                                  + "<tr>"
                                      + "<td>"
                                          + " ";

                try
                {
                    if (_Salary.Tables[0].Rows.Count > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int ii = 0; ii < _Salary.Tables[0].Rows.Count; ii++)
                        {
                            letter += "<tr><td>" + _Salary.Tables[0].Rows[ii]["itemDescr"].ToString() + "<td/><td>" + _Salary.Tables[0].Rows[ii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }

                letter += "</td>"
         + "</tr>"
     + "</table>"
 + "</td>"
 + "<td style='width: 1px; background-color: Gray'>"
     + "&nbsp;</td>"
 + "<td style='vertical-align: top; height: 150px'>"
     + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
         + "<tr>"
             + "<td style='height: 20px; background-color: lightgrey'>"
                 + "<div align='center'>"
                     + "Addition</div>"
             + "</td>"
         + "</tr>"
         + "<tr>"
             + "<td>";
                try
                {
                    if (_Addition.Tables[0].Rows.Count > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int iii = 0; iii < _Addition.Tables[0].Rows.Count; iii++)
                        {
                            letter += "<tr><td>" + _Addition.Tables[0].Rows[iii]["itemDescr"].ToString() + "<td/><td>" + _Addition.Tables[0].Rows[iii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }



                letter += "</td>"
        + "</tr>"
    + "</table>"
+ "</td>"
+ "<td style='width: 1px; background-color: Gray'>"
    + "&nbsp;</td>"
+ "<td style='vertical-align: top; height: 150px'>"
    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
        + "<tr>"
            + "<td style='background-color: lightgrey'>"
                + "<div align='center'>"
                    + "Deduction</div>"
            + "</td>"
        + "</tr>"
        + "<tr>"
            + "<td>";
                try
                {
                    if (_Deduction.Tables[0].Rows.Count > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int iiii = 0; iiii < _Deduction.Tables[0].Rows.Count; iiii++)
                        {
                            letter += "<tr><td>" + _Deduction.Tables[0].Rows[iiii]["itemDescr"].ToString() + "<td/><td>" + _Deduction.Tables[0].Rows[iiii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }


                letter += "</td>"
        + "</tr>"
    + "</table>"
+ "</td>"
+ "<td style='width: 1px; background-color: Gray'>"
    + "&nbsp;</td>"
+ "<td style='vertical-align: top; height: 150px'>"
    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
        + "<tr>"
            + "<td style='background-color: lightgrey'>"
                + "<div align='center'>"
                    + "Summary</div>"
            + "</td>"
        + "</tr>"
        + "<tr>"
            + "<td>";
                try
                {
                    if (_Summary.Tables[0].Rows.Count > 0)
                    {

                        letter += "<table>"
                         + "<tr><td>Total Paid<td><td>" + _Summary.Tables[0].Rows[0]["salaryTotal"].ToString() + "</td></tr>"
                           + "<tr><td>Total Additions<td><td>" + _Summary.Tables[0].Rows[0]["additionTotal"].ToString() + "</td></tr>"
                             + "<tr><td>Total Deductions<td><td>" + _Summary.Tables[0].Rows[0]["deductionTotal"].ToString() + "</td></tr>"
                               + "<tr><td>Net<td><td>" + _Summary.Tables[0].Rows[0]["net"].ToString() + "</td></tr>"
                         + "</table>";
                    }
                }
                catch { }



                letter += "</td>"
         + "</tr>"
     + "</table>"
 + "</td>"
+ "</tr>"
+ "</table>"
+ "</td>"
+ "</tr>"
+ "<tr style='font-size: 12pt'>"
+ "<td style='border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid' >"
+ "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; font-size: 9pt; font-family: arial;'>"
+ "<tr>"
 + "<td>"
     + "&nbsp;*Notice* " + _name + "</td>"
+ "</tr>"
+ "<tr>"
 + "<td>"
     + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; font-size: 9pt; font-family: arial;'>"
         + "<tr>"
             + "<td style='height: 15px'>"
                 + "&nbsp;&nbsp; Deposit &nbsp;";




                try
                {
                    if (_Summary.Tables[0].Rows.Count > 0)
                    {
                        letter += _Summary.Tables[0].Rows[0]["net"].ToString();
                    }
                }
                catch { }






                letter += "</td>"
             + "<td style='height: 15px'>"
                 + "Account No.: " + _accountnumber + "</td>"
             + "<td style='height: 15px'>"
                 + "Bank : " + _bank + "</td>"
            + "</tr>"
        + "</table>"
    + "</td>"
+ "</tr>"
+ "</table>"
+ "</td>"
+ "</tr>"
+ "<tr style='font-size: 12pt'>"
+ "<td style='border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid;"
+ "border-bottom: black 1px solid; font-size: 9pt; font-family: arial;'>"
+ "&nbsp;*Notes*&nbsp;";


                letter += "</td>"
        + "</tr>"
        + "<tr style='font-size: 12pt'>"
            + "<td>"
            + "</td>"
        + "</tr>"
    + "</table>"
    + "<br /><br /><br /><br />";

                 
                _tw.Write(letter);
                _tw.Flush();
                letter = "";


                _Salary.Dispose();
                _Addition.Dispose();
                _Deduction.Dispose();
                _Summary.Dispose();



            }
            _tw.Close();
            Process.Start(_Filepath);
        }


        /*
        public void Create(string CycleID, int TransMode, string WhereID, string _Filepath)
        {
            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/Pay-slip-template.docx");
            string destinationFile = _Filepath; //System.Web.HttpContext.Current.Server.MapPath("~/CommonHelper/testdoc.docx");

            // Create a copy of the template file and open the copy 
            File.Copy(sourceFile, destinationFile, true);

            // create key value pair, key represents words to be replace and 
            //values represent values in document in place of keys.
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues.Add("employeeid", "123");
            SearchAndReplace(destinationFile, keyValues);

            //Process.Start(destinationFile);
        }
        */

        // To search and replace content in a document part.
        public  void SearchAndReplace(string document, Dictionary<string, string> dict)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }

                foreach (KeyValuePair<string, string> item in dict)
                {
                    Regex regexText = new Regex(item.Key);
                    docText = regexText.Replace(docText, item.Value);
                }

                using (StreamWriter sw = new StreamWriter(
                          wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }

                //new page
                //wordDoc.MainDocumentPart.Document = new DocumentFormat.OpenXml.Wordprocessing.Document(docText.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", ""));
            }
        }


        /*
        public void SendEmails(int CycleID, int TransMode, int WhereID)
        {
            PayrollBusiness _Payrollbusiness = new PayrollBusiness();
            _PayslipData = _Payrollbusiness.GetPayslipData(CycleID, TransMode, WhereID);
            _teacherList = _Payrollbusiness.GetTeachers(CycleID, TransMode, WhereID);
            _PayslipSummary = _Payrollbusiness.GetPayslipSummary(CycleID, TransMode, WhereID);
            string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";


            for (int i = 0; i < _teacherList.Tables[0].Rows.Count; i++)
            {
                #region Send Email To Current Employee
                _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";


                _Salary = new DataSet();
                _Addition = new DataSet();
                _Deduction = new DataSet();
                _Summary = new DataSet();


                DataRow[] salaryRow = _PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' ");
                DataRow[] additionRow = _PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' ");
                DataRow[] deductionRow = _PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' ");
                DataRow[] summaryRow = _PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]);

                _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Salary' "));
                _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Addition' "));
                _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"] + " and type = 'Deduction' "));
                _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + _teacherList.Tables[0].Rows[i]["employeeid"]));


                _employeeid = salaryRow[0]["employeeid"].ToString();
                _name = salaryRow[0]["name"].ToString();
                _cyclename = summaryRow[0]["cyclename"].ToString();
                _paydate = Convert.ToDateTime(summaryRow[0]["paydate"]).ToString("dd/MM/yyyy");
                _datefrom = Convert.ToDateTime(summaryRow[0]["datefrom"]).ToString("dd/MM/yyyy");
                _dateto = Convert.ToDateTime(summaryRow[0]["dateto"]).ToString("dd/MM/yyyy");
                _department = salaryRow[0]["department"].ToString();
                _accountnumber = salaryRow[0]["accountnumber"].ToString();
                _bank = salaryRow[0]["bank"].ToString();

                //_employeeid = _Salary.Tables[0].Rows[i]["employeeid"].ToString();
                //_name = _Salary.Tables[0].Rows[i]["name"].ToString();
                //_cyclename = summaryRow[i]["cyclename"].ToString();
                //_paydate = Convert.ToDateTime(summaryRow[i]["paydate"]).ToString("dd/MM/yyyy");
                //_datefrom = Convert.ToDateTime(summaryRow[i]["datefrom"]).ToString("dd/MM/yyyy");
                //_dateto = Convert.ToDateTime(summaryRow[i]["dateto"]).ToString("dd/MM/yyyy"); 
                //_department = _Salary.Tables[0].Rows[i]["department"].ToString();
                //_accountnumber = _Salary.Tables[0].Rows[i]["accountnumber"].ToString();
                //_bank = _Salary.Tables[0].Rows[i]["bank"].ToString();



                //-------------------------------------------------------------------------------------


                letter += "   <br /> "
                    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 950px'>"
                    + "<tr>"
                    + "<td style='background-color: whitesmoke'>"
                  + "<table border='0' cellpadding='0' cellspacing='5' style='font-size: 9pt; vertical-align: middle;"
                      + "width: 100%; font-family: Arial; text-align: center'>"
                      + "<tr>"
                          + "<td>"
                              + "Number</td>"
                          + "<td>"
                              + "Name</td>"
                          + "<td>"
                              + "PayCycle No.</td>"
                          + "<td>"
                              + "Pay date</td>"
                          + "<td>"
                              + "From</td>"
                          + "<td>"
                              + "To</td>"
                          + "<td>"
                              + "Slip No.</td>"
                          + "<td>"
                              + "Department</td>"
                          + "<td rowspan='2' style='border-right: black 1px solid; border-top: black 1px solid;"
                              + "border-left: black 1px solid; border-bottom: black 1px solid; background-color: white; text-align: left;'>"
                              + " " + summaryRow[0]["schoolname"].ToString() + " </td>"
                      + "</tr>"
                      + "<tr>"
                          + "<td style='background-color: white'>"
                              + "" + _employeeid + "</td>"
                          + "<td style='background-color: white'>"
                              + "" + _name + "</td>"
                          + "<td style='background-color: white'>"
                              + " " + _cyclename + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _paydate + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _datefrom + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _dateto + " </td>"
                          + "<td style='background-color: white'>"
                              + "</td>"
                          + "<td style='background-color: white'>"
                              + "" + _department + "</td>"
                      + "</tr>"
                  + "</table>"
                    + "</td>"
                    + "</tr>"
                    + "<tr style='font-size: 12pt'>"
                    + "<td>"
                  + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; border-right: black 2px solid; border-top: black 2px solid; border-left: black 2px solid; border-bottom: black 2px solid;'>"
                      + "<tr>"
                          + "<td style='vertical-align: top; height: 150px; border-top-width: 1px; border-left-width: 1px; border-left-color: black; border-bottom-width: 1px; border-bottom-color: black; border-top-color: black; border-right-width: 1px; border-right-color: black;'>"
                              + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
                                  + "<tr>"
                                      + "<td style='height: 20px; background-color: lightgrey'>"
                                          + "<div align='center' style='text-align: center'>"
                                              + "Remuneration</div>"
                                      + "</td>"
                                  + "</tr>"
                                  + "<tr>"
                                      + "<td>"
                                          + " ";







                try
                {
                    if (salaryRow.GetLength(0) > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int ii = 0; ii < salaryRow.GetLength(0); ii++)
                        {
                            letter += "<tr><td>" + salaryRow[ii]["itemDescr"].ToString() + "<td/><td>" + salaryRow[ii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }

                letter += "</td>"
         + "</tr>"
     + "</table>"
 + "</td>"
 + "<td style='width: 1px; background-color: Gray'>"
     + "&nbsp;</td>"
 + "<td style='vertical-align: top; height: 150px'>"
     + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
         + "<tr>"
             + "<td style='height: 20px; background-color: lightgrey'>"
                 + "<div align='center'>"
                     + "Addition</div>"
             + "</td>"
         + "</tr>"
         + "<tr>"
             + "<td>";
                try
                {
                    if (additionRow.GetLength(0) > 0)
                    {
                        letter += "<table style='width=100%'>";
                        for (int iii = 0; iii < additionRow.GetLength(0); iii++)
                        {
                            letter += "<tr><td>" + additionRow[iii]["itemDescr"].ToString() + "<td/><td>" + additionRow[iii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }



                letter += "</td>"
        + "</tr>"
    + "</table>"
+ "</td>"
+ "<td style='width: 1px; background-color: Gray'>"
    + "&nbsp;</td>"
+ "<td style='vertical-align: top; height: 150px'>"
    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
        + "<tr>"
            + "<td style='background-color: lightgrey'>"
                + "<div align='center'>"
                    + "Deduction</div>"
            + "</td>"
        + "</tr>"
        + "<tr>"
            + "<td>";
                try
                {
                    if (deductionRow.GetLength(0) > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int iiii = 0; iiii < deductionRow.GetLength(0); iiii++)
                        {
                            letter += "<tr><td>" + deductionRow[iiii]["itemDescr"].ToString() + "<td/><td>" + deductionRow[iiii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }


                letter += "</td>"
        + "</tr>"
    + "</table>"
+ "</td>"
+ "<td style='width: 1px; background-color: Gray'>"
    + "&nbsp;</td>"
+ "<td style='vertical-align: top; height: 150px'>"
    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
        + "<tr>"
            + "<td style='background-color: lightgrey'>"
                + "<div align='center'>"
                    + "Summary</div>"
            + "</td>"
        + "</tr>"
        + "<tr>"
            + "<td>";
                try
                {
                    if (summaryRow.GetLength(0) > 0)
                    {

                        letter += "<table>"
                         + "<tr><td>Total Paid<td><td>" + summaryRow[0]["salaryTotal"].ToString() + "</td></tr>"
                           + "<tr><td>Total Additions<td><td>" + summaryRow[0]["additionTotal"].ToString() + "</td></tr>"
                             + "<tr><td>Total Deductions<td><td>" + summaryRow[0]["deductionTotal"].ToString() + "</td></tr>"
                               + "<tr><td>Net<td><td>" + summaryRow[0]["net"].ToString() + "</td></tr>"
                         + "</table>";
                    }
                }
                catch { }



                letter += "</td>"
         + "</tr>"
     + "</table>"
 + "</td>"
+ "</tr>"
+ "</table>"
+ "</td>"
+ "</tr>"
+ "<tr style='font-size: 12pt'>"
+ "<td style='border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid' >"
+ "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; font-size: 9pt; font-family: arial;'>"
+ "<tr>"
 + "<td>"
     + "&nbsp;*Notice* " + _name + "</td>"
+ "</tr>"
+ "<tr>"
 + "<td>"
     + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; font-size: 9pt; font-family: arial;'>"
         + "<tr>"
             + "<td style='height: 15px'>"
                 + "&nbsp;&nbsp; Deposit &nbsp;";




                try
                {
                    if (summaryRow.GetLength(0) > 0)
                    {
                        letter += summaryRow[i]["net"].ToString();
                    }
                }
                catch { }






                letter += "</td>"
             + "<td style='height: 15px'>"
                 + "Account No.: " + _accountnumber + "</td>"
             + "<td style='height: 15px'>"
                 + "Bank : " + _bank + "</td>"
            + "</tr>"
        + "</table>"
    + "</td>"
+ "</tr>"
+ "</table>"
+ "</td>"
+ "</tr>"
+ "<tr style='font-size: 12pt'>"
+ "<td style='border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid;"
+ "border-bottom: black 1px solid; font-size: 9pt; font-family: arial;'>"
+ "&nbsp;*Notes*&nbsp;";


                letter += "</td>"
        + "</tr>"
        + "<tr style='font-size: 12pt'>"
            + "<td>"
            + "</td>"
        + "</tr>"
    + "</table>"
    + "<br /><br /><br /><br />";

                // Send Email
                SendEmail sendEmail = null;
                DataHelper dataHelper = null;
                SchoolServerInformationBusiness serverInfo = null;
                TeacherContactBusiness teacherContact = null;
                try
                {
                    string query = "Select SchoolID FROM SA_SchoolInformation WHERE IsNull(IsActive,0) = 1";
                    dataHelper = new DataHelper();
                    object result = dataHelper.GetScalarValue(query);
                    if (result != null)
                    {
                        short schoolID = Convert.ToInt16(result);
                        serverInfo = new SchoolServerInformationBusiness();
                        serverInfo = serverInfo.GetSchoolServerInformationByID(schoolID);

                        if (!string.IsNullOrEmpty(serverInfo.TeacherMailDomain) &&
                            !string.IsNullOrEmpty(serverInfo.EmailFrom) &&
                            !string.IsNullOrEmpty(serverInfo.EmailUserName) &&
                            !string.IsNullOrEmpty(serverInfo.EmailPassword) && serverInfo.SmtpPort > 0)
                        {
                            sendEmail = new SendEmail();
                            teacherContact = new TeacherContactBusiness();
                            teacherContact = teacherContact.GetTeacherContactByID(short.Parse(_employeeid));

                            if (!string.IsNullOrEmpty(teacherContact.WorkEmail))
                            {
                                sendEmail.SmtpHost = serverInfo.TeacherMailDomain;
                                sendEmail.SmtpPort = (int)serverInfo.SmtpPort;
                                sendEmail.EnableSSL = bool.Parse(serverInfo.EnableSsl.ToString()); //true;
                                sendEmail.UserName = serverInfo.EmailUserName;
                                sendEmail.Password = serverInfo.EmailPassword;
                                sendEmail.FromAddress = serverInfo.EmailFrom;
                                sendEmail.To = teacherContact.WorkEmail;
                                //sendEmail.CC = "ameen.arya@gmail.com"; //test@testmail.com";
                                sendEmail.Subject = "Pay Slip";
                                sendEmail.IsBodyHtml = true;
                                sendEmail.Message = letter;
                                sendEmail.Send();
                                new PayEmailLogBusiness().AddPayEmailLogRecord(short.Parse(_employeeid), CycleID, DateTime.Now, true);
                                //sendEmail.SendDatabaseMail();
                            }
                            else
                            {
                                new PayEmailLogBusiness().AddPayEmailLogRecord(short.Parse(_employeeid), CycleID, DateTime.Now, false);
                            }
                        }
                    }
                }
                catch
                {
                    new PayEmailLogBusiness().AddPayEmailLogRecord(short.Parse(_employeeid), CycleID, DateTime.Now, false);
                    throw;
                }
                finally
                {
                    if (serverInfo != null)
                    {
                        serverInfo.Dispose();
                    }
                    if (teacherContact != null)
                    {
                        teacherContact.Dispose();
                    }
                    serverInfo.Dispose();
                    dataHelper.Dispose();
                }

                letter = "";

                _Salary.Dispose();
                _Addition.Dispose();
                _Deduction.Dispose();
                _Summary.Dispose();
                #endregion
            }
        }

        public void SendEmails(int CycleID, int TransMode, int WhereID, ArrayList EmpList)
        {
            PayrollBusiness _Payrollbusiness = new PayrollBusiness();
            _PayslipData = _Payrollbusiness.GetPayslipData(CycleID, TransMode, WhereID);
            //_teacherList = _Payrollbusiness.GetTeachers(CycleID, TransMode, WhereID);
            _PayslipSummary = _Payrollbusiness.GetPayslipSummary(CycleID, TransMode, WhereID);
            string _employeeid = "", _name = "", _cyclename = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";


            for (int i = 0; i < EmpList.Count; i++)
            {
                #region Send Email To Current Employee
                _employeeid = ""; _name = ""; _cyclename = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";


                _Salary = new DataSet();
                _Addition = new DataSet();
                _Deduction = new DataSet();
                _Summary = new DataSet();


                DataRow[] salaryRow = _PayslipData.Tables[0].Select("employeeid = " + EmpList[i].ToString() + " and type = 'Salary' ");
                DataRow[] additionRow = _PayslipData.Tables[0].Select("employeeid = " + EmpList[i].ToString() + " and type = 'Addition' ");
                DataRow[] deductionRow = _PayslipData.Tables[0].Select("employeeid = " + EmpList[i].ToString() + " and type = 'Deduction' ");
                DataRow[] summaryRow = _PayslipSummary.Tables[0].Select("employeeid = " + EmpList[i].ToString());

                _Salary.Merge(_PayslipData.Tables[0].Select("employeeid = " + EmpList[i].ToString() + " and type = 'Salary' "));
                _Addition.Merge(_PayslipData.Tables[0].Select("employeeid = " + EmpList[i].ToString() + " and type = 'Addition' "));
                _Deduction.Merge(_PayslipData.Tables[0].Select("employeeid = " + EmpList[i].ToString() + " and type = 'Deduction' "));
                _Summary.Merge(_PayslipSummary.Tables[0].Select("employeeid = " + EmpList[i].ToString()));


                _employeeid = salaryRow[0]["employeeid"].ToString();
                _name = salaryRow[0]["name"].ToString();
                _cyclename = summaryRow[0]["cyclename"].ToString();
                _paydate = Convert.ToDateTime(summaryRow[0]["paydate"]).ToString("dd/MM/yyyy");
                _datefrom = Convert.ToDateTime(summaryRow[0]["datefrom"]).ToString("dd/MM/yyyy");
                _dateto = Convert.ToDateTime(summaryRow[0]["dateto"]).ToString("dd/MM/yyyy");
                _department = salaryRow[0]["department"].ToString();
                _accountnumber = salaryRow[0]["accountnumber"].ToString();
                _bank = salaryRow[0]["bank"].ToString();

                //_employeeid = _Salary.Tables[0].Rows[i]["employeeid"].ToString();
                //_name = _Salary.Tables[0].Rows[i]["name"].ToString();
                //_cyclename = summaryRow[i]["cyclename"].ToString();
                //_paydate = Convert.ToDateTime(summaryRow[i]["paydate"]).ToString("dd/MM/yyyy");
                //_datefrom = Convert.ToDateTime(summaryRow[i]["datefrom"]).ToString("dd/MM/yyyy");
                //_dateto = Convert.ToDateTime(summaryRow[i]["dateto"]).ToString("dd/MM/yyyy"); 
                //_department = _Salary.Tables[0].Rows[i]["department"].ToString();
                //_accountnumber = _Salary.Tables[0].Rows[i]["accountnumber"].ToString();
                //_bank = _Salary.Tables[0].Rows[i]["bank"].ToString();



                //-------------------------------------------------------------------------------------


                letter += "   <br /> "
                    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 950px'>"
                    + "<tr>"
                    + "<td style='background-color: whitesmoke'>"
                  + "<table border='0' cellpadding='0' cellspacing='5' style='font-size: 9pt; vertical-align: middle;"
                      + "width: 100%; font-family: Arial; text-align: center'>"
                      + "<tr>"
                          + "<td>"
                              + "Number</td>"
                          + "<td>"
                              + "Name</td>"
                          + "<td>"
                              + "PayCycle No.</td>"
                          + "<td>"
                              + "Pay date</td>"
                          + "<td>"
                              + "From</td>"
                          + "<td>"
                              + "To</td>"
                          + "<td>"
                              + "Slip No.</td>"
                          + "<td>"
                              + "Department</td>"
                          + "<td rowspan='2' style='border-right: black 1px solid; border-top: black 1px solid;"
                              + "border-left: black 1px solid; border-bottom: black 1px solid; background-color: white; text-align: left;'>"
                              + " " + summaryRow[0]["schoolname"].ToString() + " </td>"
                      + "</tr>"
                      + "<tr>"
                          + "<td style='background-color: white'>"
                              + "" + _employeeid + "</td>"
                          + "<td style='background-color: white'>"
                              + "" + _name + "</td>"
                          + "<td style='background-color: white'>"
                              + " " + _cyclename + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _paydate + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _datefrom + " </td>"
                          + "<td style='background-color: white'>"
                              + " " + _dateto + " </td>"
                          + "<td style='background-color: white'>"
                              + "</td>"
                          + "<td style='background-color: white'>"
                              + "" + _department + "</td>"
                      + "</tr>"
                  + "</table>"
                    + "</td>"
                    + "</tr>"
                    + "<tr style='font-size: 12pt'>"
                    + "<td>"
                  + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; border-right: black 2px solid; border-top: black 2px solid; border-left: black 2px solid; border-bottom: black 2px solid;'>"
                      + "<tr>"
                          + "<td style='vertical-align: top; height: 150px; border-top-width: 1px; border-left-width: 1px; border-left-color: black; border-bottom-width: 1px; border-bottom-color: black; border-top-color: black; border-right-width: 1px; border-right-color: black;'>"
                              + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
                                  + "<tr>"
                                      + "<td style='height: 20px; background-color: lightgrey'>"
                                          + "<div align='center' style='text-align: center'>"
                                              + "Remuneration</div>"
                                      + "</td>"
                                  + "</tr>"
                                  + "<tr>"
                                      + "<td>"
                                          + " ";







                try
                {
                    if (salaryRow.GetLength(0) > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int ii = 0; ii < salaryRow.GetLength(0); ii++)
                        {
                            letter += "<tr><td>" + salaryRow[ii]["itemDescr"].ToString() + "<td/><td>" + salaryRow[ii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }

                letter += "</td>"
         + "</tr>"
     + "</table>"
 + "</td>"
 + "<td style='width: 1px; background-color: Gray'>"
     + "&nbsp;</td>"
 + "<td style='vertical-align: top; height: 150px'>"
     + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
         + "<tr>"
             + "<td style='height: 20px; background-color: lightgrey'>"
                 + "<div align='center'>"
                     + "Addition</div>"
             + "</td>"
         + "</tr>"
         + "<tr>"
             + "<td>";
                try
                {
                    if (additionRow.GetLength(0) > 0)
                    {
                        letter += "<table style='width=100%'>";
                        for (int iii = 0; iii < additionRow.GetLength(0); iii++)
                        {
                            letter += "<tr><td>" + additionRow[iii]["itemDescr"].ToString() + "<td/><td>" + additionRow[iii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }



                letter += "</td>"
        + "</tr>"
    + "</table>"
+ "</td>"
+ "<td style='width: 1px; background-color: Gray'>"
    + "&nbsp;</td>"
+ "<td style='vertical-align: top; height: 150px'>"
    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
        + "<tr>"
            + "<td style='background-color: lightgrey'>"
                + "<div align='center'>"
                    + "Deduction</div>"
            + "</td>"
        + "</tr>"
        + "<tr>"
            + "<td>";
                try
                {
                    if (deductionRow.GetLength(0) > 0)
                    {

                        letter += "<table style='width=100%'>";
                        for (int iiii = 0; iiii < deductionRow.GetLength(0); iiii++)
                        {
                            letter += "<tr><td>" + deductionRow[iiii]["itemDescr"].ToString() + "<td/><td>" + deductionRow[iiii]["amount"].ToString() + "</td><tr>";
                        }
                        letter += "</table>";
                    }
                }
                catch { }


                letter += "</td>"
        + "</tr>"
    + "</table>"
+ "</td>"
+ "<td style='width: 1px; background-color: Gray'>"
    + "&nbsp;</td>"
+ "<td style='vertical-align: top; height: 150px'>"
    + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%'>"
        + "<tr>"
            + "<td style='background-color: lightgrey'>"
                + "<div align='center'>"
                    + "Summary</div>"
            + "</td>"
        + "</tr>"
        + "<tr>"
            + "<td>";
                try
                {
                    if (summaryRow.GetLength(0) > 0)
                    {

                        letter += "<table>"
                         + "<tr><td>Total Paid<td><td>" + summaryRow[0]["salaryTotal"].ToString() + "</td></tr>"
                           + "<tr><td>Total Additions<td><td>" + summaryRow[0]["additionTotal"].ToString() + "</td></tr>"
                             + "<tr><td>Total Deductions<td><td>" + summaryRow[0]["deductionTotal"].ToString() + "</td></tr>"
                               + "<tr><td>Net<td><td>" + summaryRow[0]["net"].ToString() + "</td></tr>"
                         + "</table>";
                    }
                }
                catch { }



                letter += "</td>"
         + "</tr>"
     + "</table>"
 + "</td>"
+ "</tr>"
+ "</table>"
+ "</td>"
+ "</tr>"
+ "<tr style='font-size: 12pt'>"
+ "<td style='border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid' >"
+ "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; font-size: 9pt; font-family: arial;'>"
+ "<tr>"
 + "<td>"
     + "&nbsp;*Notice* " + _name + "</td>"
+ "</tr>"
+ "<tr>"
 + "<td>"
     + "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; font-size: 9pt; font-family: arial;'>"
         + "<tr>"
             + "<td style='height: 15px'>"
                 + "&nbsp;&nbsp; Deposit &nbsp;";




                try
                {
                    if (summaryRow.GetLength(0) > 0)
                    {
                        letter += summaryRow[i]["net"].ToString();
                    }
                }
                catch { }






                letter += "</td>"
             + "<td style='height: 15px'>"
                 + "Account No.: " + _accountnumber + "</td>"
             + "<td style='height: 15px'>"
                 + "Bank : " + _bank + "</td>"
            + "</tr>"
        + "</table>"
    + "</td>"
+ "</tr>"
+ "</table>"
+ "</td>"
+ "</tr>"
+ "<tr style='font-size: 12pt'>"
+ "<td style='border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid;"
+ "border-bottom: black 1px solid; font-size: 9pt; font-family: arial;'>"
+ "&nbsp;*Notes*&nbsp;";


                letter += "</td>"
        + "</tr>"
        + "<tr style='font-size: 12pt'>"
            + "<td>"
            + "</td>"
        + "</tr>"
    + "</table>"
    + "<br /><br /><br /><br />";

                // Send Email
                SendEmail sendEmail = null;
                DataHelper dataHelper = null;
                SchoolServerInformationBusiness serverInfo = null;
                TeacherContactBusiness teacherContact = null;
                try
                {
                    string query = "Select SchoolID FROM SA_SchoolInformation WHERE IsNull(IsActive,0) = 1";
                    dataHelper = new DataHelper();
                    object result = dataHelper.GetScalarValue(query);
                    if (result != null)
                    {
                        short schoolID = Convert.ToInt16(result);
                        serverInfo = new SchoolServerInformationBusiness();
                        serverInfo = serverInfo.GetSchoolServerInformationByID(schoolID);

                        if (!string.IsNullOrEmpty(serverInfo.TeacherMailDomain) &&
                            !string.IsNullOrEmpty(serverInfo.EmailFrom) &&
                            !string.IsNullOrEmpty(serverInfo.EmailUserName) &&
                            !string.IsNullOrEmpty(serverInfo.EmailPassword) && serverInfo.SmtpPort > 0)
                        {
                            sendEmail = new SendEmail();
                            teacherContact = new TeacherContactBusiness();
                            teacherContact = teacherContact.GetTeacherContactByID(short.Parse(_employeeid));

                            if (!string.IsNullOrEmpty(teacherContact.WorkEmail))
                            {
                                sendEmail.SmtpHost = serverInfo.TeacherMailDomain;
                                sendEmail.SmtpPort = (int)serverInfo.SmtpPort;
                                sendEmail.EnableSSL = bool.Parse(serverInfo.EnableSsl.ToString()); //true;
                                sendEmail.UserName = serverInfo.EmailUserName;
                                sendEmail.Password = serverInfo.EmailPassword;
                                sendEmail.FromAddress = serverInfo.EmailFrom;
                                sendEmail.To = teacherContact.WorkEmail;
                                //sendEmail.CC = "ameen.arya@gmail.com"; //test@testmail.com";
                                sendEmail.Subject = "Pay Slip";
                                sendEmail.IsBodyHtml = true;
                                sendEmail.Message = letter;
                                sendEmail.Send();
                                new PayEmailLogBusiness().AddPayEmailLogRecord(short.Parse(_employeeid), CycleID, DateTime.Now, true);
                                //sendEmail.SendDatabaseMail();
                            }
                            else
                            {
                                new PayEmailLogBusiness().AddPayEmailLogRecord(short.Parse(_employeeid), CycleID, DateTime.Now, false);
                            }
                        }
                    }
                }
                catch
                {
                    new PayEmailLogBusiness().AddPayEmailLogRecord(short.Parse(_employeeid), CycleID, DateTime.Now, false);
                    throw;
                }
                finally
                {
                    if (serverInfo != null)
                    {
                        serverInfo.Dispose();
                    }
                    if (teacherContact != null)
                    {
                        teacherContact.Dispose();
                    }
                    serverInfo.Dispose();
                    dataHelper.Dispose();
                }

                letter = "";

                _Salary.Dispose();
                _Addition.Dispose();
                _Deduction.Dispose();
                _Summary.Dispose();
                #endregion
            }
        }

        public void SendSundryEmails(int BatchID, int SundryID)
        {
            //PayrollBusiness _Payrollbusiness = new PayrollBusiness();
            //_PayslipData = _Payrollbusiness.GetPayslipData(CycleID, TransMode, WhereID);
            //_teacherList = _Payrollbusiness.GetTeachers(CycleID, TransMode, WhereID);
            //_PayslipSummary = _Payrollbusiness.GetPayslipSummary(CycleID, TransMode, WhereID);
            string _employeeid = "", _name = "", _BatchName = "", _paydate = "", _datefrom = "", _dateto = "", _department = "", _accountnumber = "", _bank = "";
            double _Amount = 0;
            DataSet _dsRpt = null;
            String MessageBody = "";
            PaySundriesBusiness _paySundriesBusiness = new PaySundriesBusiness();
            _dsRpt = _paySundriesBusiness.GetPaySundriesListOfBatchForReport(BatchID, SundryID);

            for (int i = 0; i < _dsRpt.Tables[0].Rows.Count; i++)
            {
                #region Send Email To Current Employee
                _employeeid = ""; _name = ""; _BatchName = ""; _paydate = ""; _datefrom = ""; _dateto = ""; _department = ""; _accountnumber = ""; _bank = "";

                _employeeid = _dsRpt.Tables[0].Rows[i]["employeeid"].ToString();
                _name = _dsRpt.Tables[0].Rows[i]["EmpName"].ToString();
                _BatchName = _dsRpt.Tables[0].Rows[i]["PayBatchName"].ToString();
                _Amount = double.Parse(_dsRpt.Tables[0].Rows[i]["Amount"].ToString());
                _accountnumber = _dsRpt.Tables[0].Rows[i]["AccountNumber"].ToString();
                _bank = _dsRpt.Tables[0].Rows[i]["BankName"].ToString();



                //-------------------------------------------------------------------------------------

                letter = "Dear " + _name;
                letter += '\n';
                letter += '\n';
                letter += '\n';

                letter += " We are pleased to inform you that we have transferred the amount of ";
                letter += " AED " + _Amount.ToString() + "/= to your account No." + _accountnumber;
                letter += " in " + _bank + " as " + _BatchName;
                letter += '\n';
                letter += '\n';
                letter += "We thank you and urge you on to continuous and better achievements. ";
                letter += '\n';
                letter += '\n';
                letter += "Regards";
                letter += '\n';
                letter += '\n';
                letter += "The Administration .";

                // Send Email
                SendEmail sendEmail = null;
                DataHelper dataHelper = null;
                SchoolServerInformationBusiness serverInfo = null;
                TeacherContactBusiness teacherContact = null;
                try
                {
                    string query = "Select SchoolID FROM SA_SchoolInformation WHERE IsNull(IsActive,0) = 1";
                    dataHelper = new DataHelper();
                    object result = dataHelper.GetScalarValue(query);
                    if (result != null)
                    {
                        short schoolID = Convert.ToInt16(result);
                        serverInfo = new SchoolServerInformationBusiness();
                        serverInfo = serverInfo.GetSchoolServerInformationByID(schoolID);

                        if (!string.IsNullOrEmpty(serverInfo.TeacherMailDomain) &&
                            !string.IsNullOrEmpty(serverInfo.EmailFrom) &&
                            !string.IsNullOrEmpty(serverInfo.EmailUserName) &&
                            !string.IsNullOrEmpty(serverInfo.EmailPassword) && serverInfo.SmtpPort > 0)
                        {
                            sendEmail = new SendEmail();
                            teacherContact = new TeacherContactBusiness();
                            teacherContact = teacherContact.GetTeacherContactByID(short.Parse(_employeeid));

                            if (!string.IsNullOrEmpty(teacherContact.WorkEmail))
                            {
                                sendEmail.SmtpHost = serverInfo.TeacherMailDomain;
                                sendEmail.SmtpPort = (int)serverInfo.SmtpPort;
                                sendEmail.EnableSSL = bool.Parse(serverInfo.EnableSsl.ToString());  // true;
                                sendEmail.UserName = serverInfo.EmailUserName;
                                sendEmail.Password = serverInfo.EmailPassword;
                                sendEmail.FromAddress = serverInfo.EmailFrom;
                                // sendEmail.To = teacherContact.WorkEmail;
                                sendEmail.To = "faizan.haroon@iconnect.ae";
                                //sendEmail.CC = "althukp@gmail.com"; //test@testmail.com";
                                sendEmail.Subject = _BatchName;
                                sendEmail.IsBodyHtml = false;
                                sendEmail.Message = letter;
                                sendEmail.Send();
                                //sendEmail.SendDatabaseMail();
                            }
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (serverInfo != null)
                    {
                        serverInfo.Dispose();
                    }
                    if (teacherContact != null)
                    {
                        teacherContact.Dispose();
                    }
                    serverInfo.Dispose();
                    dataHelper.Dispose();
                }

                letter = "";

                _dsRpt.Dispose();
                #endregion
            }
        }

        */
    }
}
