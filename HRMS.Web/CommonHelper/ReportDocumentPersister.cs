﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HRMS.Web.CommonHelper
{
    public class ReportDocumentPersister
    {
        #region Public Properties
        public string ReportPhysicalPath
        {
            get;
            set;
        }
        public List<CustomParameterField> ParameterFieldList
        {
            get;
            set;
        }
        public CustomSubReports SubReports
        {
            get;
            set;
        }
        public DataTable DataSource
        {
            get;
            set;
        }

        public DataSet TableCollections
        {
            get;
            set;
        }

        public bool IsMultipleTables
        {
            get;
            set;
        }

        public bool ExportData
        {
            get;
            set;
        }
        public DiskFileDestinationOptions ExportFileDestinationOptions
        {
            get;
            set;
        }
        #endregion

        #region Public Constructors
        public ReportDocumentPersister()
        {
            SubReports = new CustomSubReports();
        }

        public ReportDocumentPersister(string reportPhysicalPath,
                                        List<CustomParameterField> parameterFieldList, DataTable dataSource,bool IsMultipleTables)
        {
            SubReports = new CustomSubReports();

            this.ReportPhysicalPath = reportPhysicalPath;
            this.ParameterFieldList = parameterFieldList;
            this.DataSource = dataSource;
            this.IsMultipleTables = IsMultipleTables;
        }

        public ReportDocumentPersister(string reportPhysicalPath,
                                List<CustomParameterField> parameterFieldList, DataSet TableCollections, bool IsMultipleTables)
        {
            SubReports = new CustomSubReports();

            this.ReportPhysicalPath = reportPhysicalPath;
            this.ParameterFieldList = parameterFieldList;
            this.TableCollections = TableCollections;
            this.IsMultipleTables = IsMultipleTables;
        }


        #endregion
    }

    public class CustomParameterField
    {
        #region Public Properties
        public string ParameterName
        {
            get;
            set;
        }
        public object ParameterValue
        {
            get;
            set;
        }
        #endregion

        #region Public Constructors
        public CustomParameterField()
        {
        }

        public CustomParameterField(string MyParameterName, object MyParameterValue)
        {
            this.ParameterName = MyParameterName;
            this.ParameterValue = MyParameterValue;
        }
        #endregion
    }

    public class CustomSubReports
    {
        #region Class Level Variables
        private List<CustomSubReport> _subReportList;
        #endregion

        #region Public Properties
        public CustomSubReport this[int index]
        {
            get
            {
                return _subReportList[index] as CustomSubReport;
            }
            set
            {
                _subReportList[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _subReportList == null ? 0 : _subReportList.Count;
            }
        }
        #endregion


        #region Constructor
        public CustomSubReports()
        {
        }
        #endregion

        #region Public Methods
        public void Add(string SubReportName, DataTable DataSource)
        {
            if (_subReportList == null)
            {
                _subReportList = new List<CustomSubReport>();
            }
            _subReportList.Add(new CustomSubReport(SubReportName, DataSource));
        }
        #endregion
    }

    public class CustomSubReport
    {
        #region Public Properties
        public string SubReportName
        {
            get;
            set;
        }
        public DataTable DataSource
        {
            get;
            set;
        }
        #endregion

        #region Public Constructors
        public CustomSubReport()
        {
        }

        public CustomSubReport(string SubReportName, DataTable DataSource)
        {
            this.SubReportName = SubReportName;
            this.DataSource = DataSource;
        }
        #endregion
    }
}